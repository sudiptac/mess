/* main.c - main line routines */

/* SimpleScalar(TM) Tool Suite
 * Copyright (C) 1994-2003 by Todd M. Austin, Ph.D. and SimpleScalar, LLC.
 * All Rights Reserved. 
 * 
 * THIS IS A LEGAL DOCUMENT, BY USING SIMPLESCALAR,
 * YOU ARE AGREEING TO THESE TERMS AND CONDITIONS.
 * 
 * No portion of this work may be used by any commercial entity, or for any
 * commercial purpose, without the prior, written permission of SimpleScalar,
 * LLC (info@simplescalar.com). Nonprofit and noncommercial use is permitted
 * as described below.
 * 
 * 1. SimpleScalar is provided AS IS, with no warranty of any kind, express
 * or implied. The user of the program accepts full responsibility for the
 * application of the program and the use of any results.
 * 
 * 2. Nonprofit and noncommercial use is encouraged. SimpleScalar may be
 * downloaded, compiled, executed, copied, and modified solely for nonprofit,
 * educational, noncommercial research, and noncommercial scholarship
 * purposes provided that this notice in its entirety accompanies all copies.
 * Copies of the modified software can be delivered to persons who use it
 * solely for nonprofit, educational, noncommercial research, and
 * noncommercial scholarship purposes provided that this notice in its
 * entirety accompanies all copies.
 * 
 * 3. ALL COMMERCIAL USE, AND ALL USE BY FOR PROFIT ENTITIES, IS EXPRESSLY
 * PROHIBITED WITHOUT A LICENSE FROM SIMPLESCALAR, LLC (info@simplescalar.com).
 * 
 * 4. No nonprofit user may place any restrictions on the use of this software,
 * including as modified by the user, by any other authorized user.
 * 
 * 5. Noncommercial and nonprofit users may distribute copies of SimpleScalar
 * in compiled or executable form as set forth in Section 2, provided that
 * either: (A) it is accompanied by the corresponding machine-readable source
 * code, or (B) it is accompanied by a written offer, with no time limit, to
 * give anyone a machine-readable copy of the corresponding source code in
 * return for reimbursement of the cost of distribution. This written offer
 * must permit verbatim duplication by anyone, or (C) it is distributed by
 * someone who received only the executable form, and is accompanied by a
 * copy of the written offer of source code.
 * 
 * 6. SimpleScalar was developed by Todd M. Austin, Ph.D. The tool suite is
 * currently maintained by SimpleScalar LLC (info@simplescalar.com). US Mail:
 * 2395 Timbercrest Court, Ann Arbor, MI 48105.
 * 
 * Copyright (C) 1994-2003 by Todd M. Austin, Ph.D. and SimpleScalar, LLC.
 */
/*
 **************************************************************************
 *     Authors :- Sandeep Baldawa<sob051000@utdallas.edu> & Rama Sangireddy
 *	This file has been modified to handle multiple cores.The file has 
 *      undergone major modifications related to the same.If you fix any bugs
 *      do not forget to add tags and then give the explanation 
 *	of those tags in the below format.
 * 
 *   TAGS               BUGS                		RESOLVER                DATE
 *  MAX_CMP_BUG_FIX     THE MAX_CMP doesnot match 
 *                      the number of arguments     SANDEEP			       02-27-2006  
 *    
 *   
 ***************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <setjmp.h>
#include <signal.h>
#include <sys/types.h>
#ifndef _MSC_VER
#include <unistd.h>
#include <sys/time.h>
#endif
#ifdef BFD_LOADER
#include <bfd.h>
#endif /* BFD_LOADER */
#include "sim.h"
#include "host.h"
#include "misc.h"
#include "machine.h"
#include "endian.h"
#include "version.h"
#include "dlite.h"
#include "options.h"
#include "stats.h"
#include "loader.h"
#include "sim_cmp.h"
/* sudiptac :::: For shared bus simulation */
#include "sharedbus.h"
/* default length for temporary strings */
#define MAXLEN 256

/* default simulator scheduling priority */
#define NICE_DEFAULT_VALUE		0


/********************************ADD GLOBAL VARIABLES HERE****************************/
/* Context related data */
context* contexts;
/* sudiptac: added for single preemption */
context* backup_contexts;
/*   Current Running context */
int current_context=0;
/* Useless have to remove 
	it sometime helps in compilation */
counter_t sim_num_insn;

/* sudiptac :::: Moved from sim-outorder.c to make it global */
int mem_lat[2] = 
{ /* lat to first chunk */8, /* lat between remaining chunks */1 };

/* multi-core specific and shared bus related option */
struct opt_odb_t* shared_sim_odb;	/* option database shared by all cores */
int max_cores_sharing_il2;				/* maximum cores sharing L2 icache */
int max_cores_sharing_dl2;				/* maximum cores sharing L2 dcache */
int max_cores;								/* maximum number of cores */
int fx_slot_length;						/* bus slot length assigned to each core */
SHARED_BUS_TYPE g_shared_bus_type;	/* shared bus type */

/*************************************************************************************/


#if 0 /* not portable... :-( */
/* total simulator (data) memory usage */
unsigned int sim_mem_usage = 0;
#endif

/*************************ADD MAIN FUNCTION GLOBAL VARIABLES**************************/
/* byte/word swapping required to execute target executable on this host */
int sim_swap_bytes;
int sim_swap_words;

/* execution start/end times */
time_t sim_start_time;
time_t sim_end_time;
int sim_elapsed_time;

/* exit when this becomes non-zero */
int sim_exit_now = FALSE;

/* longjmp here when simulation is completed */
jmp_buf sim_exit_buf;

/* set to non-zero when simulator should dump statistics */
int sim_dump_stats = FALSE;

/* stats database */

/* EIO interfaces */
char *sim_eio_fname = NULL;
char *sim_chkpt_fname = NULL;
FILE *sim_eio_fd = NULL;

/* redirected program/simulator output file names */
static char *sim_simout = NULL;
static char *sim_progout = NULL;
FILE *sim_progfd = NULL;

/*************************************************************************************/

/*********************ADD MAIN FUNCTION STATIC VARIABLES HERE ************************/
/* track first argument orphan, this is the program to execute */
static int exec_index = -1;

/* dump help information */
static int help_me;

/* random number generator seed */
static int rand_seed;

/* initialize and quit immediately */
static int init_quit;

#ifndef _MSC_VER
/* simulator scheduling priority */
static int nice_priority;
#endif

/*************************************************************************************/

/* stats signal handler */
static void
signal_sim_stats(int sigtype){
		  sim_dump_stats = TRUE;
}

/* exit signal handler */
static void
signal_exit_now(int sigtype){
		  sim_exit_now = TRUE;
}


		  static int
orphan_fn(int i, int argc, char **argv)
{
		  exec_index = i;
		  return /* done */FALSE;
}


		  static void
banner(FILE *fd, int argc, char **argv)
{
		  char *s;

		  fprintf(fd,
								"%s: SimpleScalar/%s Tool Set version %d.%d of %s.\n"
								"Copyright (c) 1994-2000 by Todd M. Austin.  All Rights Reserved.\n"
								"This version of SimpleScalar is licensed for academic non-commercial use only.\n"
								"\n",
								((s = strrchr(argv[0], '/')) ? s+1 : argv[0]),
								VER_TARGET, VER_MAJOR, VER_MINOR, VER_UPDATE);
}

static void
usage(FILE *fd, int argc, char **argv,int context_id){
		  fprintf(fd, "Usage: %s {-options} executable {arguments}\n", argv[0]);
		  if(context_id != -1){
					 opt_print_help(contexts[context_id].sim_odb, fd);
		  }	  
}

static int running = FALSE;

/* print all simulator stats */
void
sim_print_stats(FILE *fd,int context_id){

		  if (!running)
					 return;
		  /* get stats time */
		  sim_end_time = time((time_t *)NULL);
		  sim_elapsed_time = MAX(sim_end_time - sim_start_time, 1);

		  /* print simulation stats */
		  fprintf(fd, "\nsim: ** simulation statistics of %s **\n", contexts[context_id].mem->ld_prog_fname);
		  stat_print_stats(contexts[context_id].sim_sdb, fd);
		  sim_aux_stats(fd);
		  fprintf(fd, "\n");
}

/* un-initialize the simulator */
void
sim_uninit(int context_id){

		  if ((contexts[context_id].ptrace_nelt) > 0){
					 ptrace_close();
		  }	


}

/* print stats, uninitialize simulator components, and exit w/ exitcode */
void
exit_now(int exit_code){
		  int i;
		  sqword_t cycle;
		  FILE* tmpFile;
		  char banner_str[128];
		  char addr_str[64];
		  char bpred_count_str[64];
		  char cycles_str[64];
		  char bpred_miss_str[64];
		  char icache_str_L1[64];
		  char icache_str_L2[64];
		  char bus_str[64];
		  char task_str[256];

		  for(i=0;i<MAX_CMP;i++){
					 /* print simulation stats */
					 opt_print_options(contexts[i].sim_odb, contexts[i].outfile, /* short */TRUE, /* notes */TRUE);
					 sim_print_stats(contexts[i].outfile,i);
					 sim_print_stats(stderr,i);

					 /* un-initialize the simulator */
					 sim_uninit(i);
		  }	
  
		  /* by Chronos: to dump out effective execution cycles, branch misses, cache
		  * misses etc.
		  */

		  for (i = 0; i < MAX_CMP; i++)
		  {
					 sprintf(banner_str, "+----------------------------------------------------------------------------------+");
					 sprintf(task_str, "%-30s%s", "task name:", contexts[i].mem->ld_prog_fname);
					 sprintf(addr_str, "%-30s[%x, %x]", "effective code range:", contexts[i].mem->start_addr, \
								contexts[i].mem->end_addr - sizeof(md_inst_t));
					 //sprintf(bpred_count_str, "%-30s%d", "effective bpred count:", effect_bpred_count);
					 sprintf(cycles_str, "%-30s%Lu", "effective sim cycles:", contexts[i].effect_cycles);
					 sprintf(bpred_miss_str, "%-30s%Lu", "effective bpred misses:", contexts[i].effect_bpred_miss);
					 sprintf(icache_str_L1, "%-30s%Lu", "effective L1 icache misses:", contexts[i].effect_icache_l1_miss);
					 sprintf(icache_str_L2, "%-30s%Lu", "effective L2 icache misses:", contexts[i].effect_icache_l2_miss);
					 sprintf(bus_str, "%-30s%Lu", "effective bus delay:", contexts[i].effect_bus_delay);

					 printf("%s\n", banner_str);
					 printf("| %-80s |\n", task_str);
					 printf("| %-80s |\n", addr_str);
					 //printf("| %-60s |\n", bpred_count_str);
					 printf("| %-80s |\n", cycles_str);
					 printf("| %-80s |\n", bpred_miss_str);
					 printf("| %-80s |\n", icache_str_L1);
					 printf("| %-80s |\n", icache_str_L2);
					 printf("| %-80s |\n", bus_str);
					 printf("%s\n\n", banner_str);
		  }

  #if 0

		  fprintf(stdout, "***********************************************************************\n");
		  tmpFile = fopen("busdelay.out", "a");
		  if(!tmpFile) {
					 fprintf(stderr, "bad output file\n");
					 exit(-1);
		  }
		  /* sudiptac :::: Print effective cycle information */
		  for(i = 0; i < MAX_CMP; i++) {
					 cycle = contexts[i].effect_cycles + (mem_lat[0] + contexts[i].cache_il2_lat)
								* contexts[i].effect_icache_l2_miss + contexts[i].cache_il2_lat * 
								(contexts[i].effect_icache_l1_miss - contexts[i].effect_icache_l2_miss);
					 contexts[i].effect_cycles = cycle;				 
					 fprintf(stdout, "==================================================================\n");
					 fprintf(stdout, "Sim statistics for program %s\n", contexts[i].mem->ld_prog_fname);
					 fprintf(stdout, "==================================================================\n");
					 fprintf(stdout, "Effective No. of instructions = %u\n", contexts[i].effect_cycles);
					 fprintf(stdout, "Effective L1 cache miss = %u\n", contexts[i].effect_icache_l1_miss);
					 fprintf(stdout, "Effective L2 cache miss = %u\n", contexts[i].effect_icache_l2_miss);
					 fprintf(stdout, "Effective cycle = %u\n", cycle);
					 fprintf(stdout, "Effective bus delay = %u\n", contexts[i].effect_bus_delay);
					 /* DELETEME: delete this line once done */
					 fprintf(tmpFile, "%u\t", contexts[i].effect_bus_delay);
					 fprintf(stdout, "Effective cycle with shared bus = %u\n", cycle + 
										  contexts[i].effect_bus_delay);
		  }		  
		  /* DELETEME: delete this line once done */
		  fprintf(tmpFile, "\n");
		  fclose(tmpFile);
		  fprintf(stdout, "***********************************************************************\n");
#endif

		  /* all done! */
		  if(!exit_code)
					 exit(exit_code);
}

static void opt_reg(struct opt_odb_t *sim_odb, int context_id){
		  opt_reg_flag(contexts[context_id].sim_odb, "-h", "print help message",
								&help_me, /* default */FALSE, /* !print */FALSE, NULL);
		  opt_reg_flag(contexts[context_id].sim_odb, "-v", "verbose operation",
								&verbose, /* default */FALSE, /* !print */FALSE, NULL);
#ifdef DEBUG
		  opt_reg_flag(contexts[context_id].sim_odb, "-d", "enable debug message",
								&debugging, /* default */FALSE, /* !print */FALSE, NULL);
#endif /* DEBUG */
		  opt_reg_flag(contexts[context_id].sim_odb, "-i", "start in Dlite debugger",
								&dlite_active, /* default */FALSE, /* !print */FALSE, NULL);
		  opt_reg_int(contexts[context_id].sim_odb, "-seed",
								"random number generator seed (0 for timer seed)",
								&rand_seed, /* default */1, /* print */TRUE, NULL);
		  opt_reg_flag(contexts[context_id].sim_odb, "-q", "initialize and terminate immediately",
								&init_quit, /* default */FALSE, /* !print */FALSE, NULL);
		  opt_reg_string(contexts[context_id].sim_odb, "-chkpt", "restore EIO trace execution from <fname>",
								&sim_chkpt_fname, /* default */NULL, /* !print */FALSE, NULL);

		  /* stdio redirection options */
		  opt_reg_string(contexts[context_id].sim_odb, "-redir:sim",
								"redirect simulator output to file (non-interactive only)",
								&sim_simout,
								/* default */NULL, /* !print */FALSE, NULL);
		  opt_reg_string(contexts[context_id].sim_odb, "-redir:prog",
								"redirect simulated program output to file",
								&sim_progout, /* default */NULL, /* !print */FALSE, NULL);

#ifndef _MSC_VER
		  /* scheduling priority option */
		  opt_reg_int(contexts[context_id].sim_odb, "-nice",
								"simulator scheduling priority", &nice_priority,
								/* default */NICE_DEFAULT_VALUE, /* print */TRUE, NULL);
#endif

		  /* FIXME: add stats intervals and max insts... */

		  /* register all simulator-specific options */
		  sim_reg_options(contexts[context_id].sim_odb, shared_sim_odb, context_id);

		  /* parse simulator options */
		  exec_index = -1;
}

/* allocate memory and initialize all core data structures */
static void
init_all_cores(int max_cmp) {
		  contexts = (context *) calloc(max_cmp, sizeof(context));

		  if (!contexts)
					 fatal("out of virtual memory");

		  /* sudiptac: allocate backup contexts to handle single preemptions */
		  backup_contexts = (context *) calloc(max_cmp, sizeof(context));

		  if (!backup_contexts)
					 fatal("out of virtual memory");
}


static void
init_core(char *env[], char *filename, int context_id){
		  FILE *argfile;
		  int retcode,i;
		  int locargc = 0;
		  char *locargv[256] = {0};
		  char file[256];
		  context *current;
		  char *temp[100]={0};
		  //char temp[100];
#if 0
		  int j,opt_count=0;
		  FILE *argopt;
		  char opt_array[30][50];
		  char firststring[256];
#endif

		  /* Open new options database for each context  and register for the same */
		  contexts[current_context].sim_odb = opt_new(orphan_fn);
		  opt_reg((contexts[current_context].sim_odb),current_context);

		  /* create a context to load the core into */
		  current = (context *) calloc(1,sizeof(context));

		  /* open the file containing the command line arguments for this core*/
		  current->argfile = argfile = fopen(filename, "r");
		  printf("\n filename is %s  \n",filename);
		  if(argfile == NULL){
					 fprintf(contexts[current_context].outfile,"ERROR: cannot open argument file: %s\n", filename);
					 exit(-1);
		  }

		  locargv[0] = (char *) malloc(240);
		  temp[0] = (char *) malloc(30);
		  temp[1] = (char *) malloc(30);
		  temp[2] = (char *) malloc(30);
		  temp[3] = (char *) malloc(30);

		  while (1) {
					 if (locargv[locargc] == NULL  ) {
								locargv[locargc] = (char *) malloc(240);
					 }

					 /*
					  ** Read until EOF
					  */
					 retcode = fscanf(argfile, "%s", locargv[locargc]);
#if 0
					 printf("\n retcode value is:%d  \n",retcode);
					 printf("\n INIT_THREAD : locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
#endif
					 if (retcode == EOF) {
								//printf("\n I get EOF here \n");
								//printf("\n*****LOCARGV  is :%s\n",locargv[0]);
								//printf("\n******LOCARGC is :%d\n",locargc);
								fprintf(contexts[current_context].outfile,"args: ");
								for (i=0;i<locargc;i++){
										  fprintf(contexts[current_context].outfile,"%d:%s ",i,locargv[i]);
								}
								fprintf(contexts[current_context].outfile,"\n");

								/* actually load the program into memory */
								//printf("\n *******before call to sim_load_prog argv is :%s \n",locargv[0]);
								//printf("\n *******before call to sim_load_prog argc is :%d \n",locargc);

								sim_load_prog(locargv[0], locargc, locargv, env,context_id);
								//printf("\n After sim_load_prog \n");
								for (i=0;i<locargc;i++){
										  free(locargv[i]);
								}	
								return;
					 }

					 /* handle input redirection (of stdin) */
					 else if (locargv[locargc][0] == '<') {
								fscanf(argfile, "%s", file);
								contexts[current_context].infile = fopen(file,"r");

								// printf("\n INIT_THREAD INPUT FILE is %s \n",file);
								//printf("\n current context value is : %d \n",current_context);
								//printf("\n INIT_THREAD INPUT FILE: %d \n",fileno(contexts[current_context].infile));
								if (contexts[current_context].infile == NULL) {
										  printf("couldn't open %s for reading\n",file);
										  exit(1);
								}
								// contexts[context_id].infile = infile[current_context];
					 }

					 /* handle output redirection (of stdout) */
					 else if (locargv[locargc][0] == '>') {
								fscanf(argfile, "%s", file);
								contexts[current_context].outfile = fopen(file,"w");
								//printf("\n INIT_THREAD OUTPUT FILE is %s \n",file);
								if (contexts[current_context].outfile == NULL) {
										  printf("couldn't open %s for writing\n",file);
										  exit(1);
								}
								//printf("the context id is %d \n",context_id);
								//printf("\n The infile is %d \n", fileno(file[current_context]));
					 }

					 else if (locargv[locargc][0] == '-') {
#if 0
								printf("\n I get option \n");
								printf("\n locargv\n");
								printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
#endif
								if(!strcmp(locargv[locargc],"-fastfwd")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].fastfwd_count = atoi(locargv[locargc]);
								}
								else if(!strcmp(locargv[locargc],"-max:inst")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].max_insts = atoi(locargv[locargc]);
								} 

								else if(!strcmp(locargv[locargc],"-fetch:ifqsize")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  //printf("ascii to int value is %d \n",atoi(locargv[locargc]));
										  contexts[context_id].ruu_ifq_size = atoi(locargv[locargc]);
								} 
								else if(!strcmp(locargv[locargc],"-decode:width")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].ruu_decode_width = atoi(locargv[locargc]);
								}
								else if(!strcmp(locargv[locargc],"-issue:width")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].ruu_issue_width = atoi(locargv[locargc]);
								} 

								else if(!strcmp(locargv[locargc],"-commit:width")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].ruu_commit_width = atoi(locargv[locargc]);
								} 
								else if(!strcmp(locargv[locargc],"-cache:il1")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  strcpy(temp[0],locargv[locargc]);
										  opt_reg_string(contexts[context_id].sim_odb, "-cache:il1",
																"i2 data cache config, i.e., {<config>|none}",
																&(contexts[context_id].cache_il1_opt), temp[0],
																/* print */TRUE, NULL);
								} 
								else if(!strcmp(locargv[locargc],"-cache:il2")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  strcpy(temp[1],locargv[locargc]);
										  opt_reg_string(contexts[context_id].sim_odb, "-cache:il2",
																"l2 data cache config, i.e., {<config>|none}",
																&(contexts[context_id].cache_il2_opt), temp[1],
																/* print */TRUE, NULL);

								} 
								else if(!strcmp(locargv[locargc],"-cache:dl1")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  ///printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  strcpy(temp[2],locargv[locargc]);
										  opt_reg_string(contexts[context_id].sim_odb, "-cache:dl1",
																"dl1 data cache config, i.e., {<config>|none}",
																&(contexts[context_id].cache_dl1_opt), temp[2],
																/* print */TRUE, NULL);

										  //printf("\n cache_dl1_opt is %s \n", contexts[context_id].cache_dl1_opt);
										  //printf("context_id is %d \n",context_id);
								}
								else if(!strcmp(locargv[locargc],"-cache:dl2")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  strcpy(temp[3],locargv[locargc]);
										  opt_reg_string(contexts[context_id].sim_odb, "-cache:dl2",
																"dl2 data cache config, i.e., {<config>|none}",
																&(contexts[context_id].cache_dl2_opt), temp[3],
																/* print */TRUE, NULL);

								}
								else if(!strcmp(locargv[locargc],"-fetch:mplat")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].ruu_branch_penalty = atoi(locargv[locargc]);
								}
								else if(!strcmp(locargv[locargc],"-ruu:size")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].RUU_size = atoi(locargv[locargc]);
								}
								else if(!strcmp(locargv[locargc],"-lsq:size")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].LSQ_size = atoi(locargv[locargc]);
								}
								else if(!strcmp(locargv[locargc],"-res:ialu")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].res_ialu = atoi(locargv[locargc]);
								}
								else if(!strcmp(locargv[locargc],"-res:imult")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].res_imult= atoi(locargv[locargc]);
								} 
								else if(!strcmp(locargv[locargc],"-res:fpalu")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].res_fpalu= atoi(locargv[locargc]);
								}
								else if(!strcmp(locargv[locargc],"-res:fpmult")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].res_fpmult= atoi(locargv[locargc]);
								} 
								else if(!strcmp(locargv[locargc],"-mem:lat")){
										  char inter_chunk[4];

										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  contexts[context_id].mem_nelt= atoi(locargv[locargc]);
										  /* sudiptac ::: set the memory latency <first chunk> <inter chunk>*/
										  mem_lat[0] = atoi(locargv[locargc]);		 
										  fscanf(argfile, "%s", inter_chunk);
										  mem_lat[1] = atoi(inter_chunk);		 
								}
								/* sudiptac: adding memory bus width option */
								else if(!strcmp(locargv[locargc],"-mem:width")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  /* sudiptac ::: set the memory bus width */
										  contexts[context_id].mem_bus_width = atoi(locargv[locargc]);		 
								}
								/* sudiptac :::: added "-issue:inorder" option */
								else if(!strcmp(locargv[locargc],"-issue:inorder")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  if(!strcmp(locargv[locargc], "true"))			
													 contexts[context_id].ruu_inorder_issue = 1;
										  else	  
													 contexts[context_id].ruu_inorder_issue = 0;
								}
								/* sudiptac :::: added "-issue:wrongpath" option */
								else if(!strcmp(locargv[locargc],"-issue:wrongpath")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  if(!strcmp(locargv[locargc], "true"))			
													 contexts[context_id].ruu_include_spec = 1;
										  else	  
													 contexts[context_id].ruu_include_spec = 0;
								}
								/* sudiptac :::: adding branch prediction */
								else if(!strcmp(locargv[locargc],"-bpred")){
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  //printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
										  /* Set the branch predictor type */			
										  contexts[context_id].pred_type = (char *)malloc(20);
										  strcpy(contexts[context_id].pred_type, locargv[locargc]);
								}
								/* sudiptac :::: adding preempting task (HIGH PRIORITY) option 
								 * (no nested or multiple preemption is supported currently) */
								else if(!strcmp(locargv[locargc], "-task:preempting")) {
												retcode = fscanf(argfile, "%s", locargv[locargc]);
												//printf("\n  locargc is : %d locarv[locargc] value is : %s \n",locargc,locargv[locargc]);
												/* set the preempting task related options */
												contexts[context_id].preempting_task_name = (char *)malloc(1024);
												strcpy(contexts[context_id].preempting_task_name, locargv[locargc]);
												/*****IMP***** set this flag */
												contexts[context_id].preempting_task = 1;
								}
								/* sudiptac :::: adding preemption point for a low priority task */
								else if(!strcmp(locargv[locargc], "-preempt:point")) {
										  retcode = fscanf(argfile, "%s", locargv[locargc]);
										  /* set the preemption point for the low priority task */
										  contexts[context_id].preemption_point = atoi(locargv[locargc]);		 
								}
					 }
					 else{
								locargc++;
					 }
		  }
}

int
main(int argc, char **argv, char **envp){
		  char *s;
		  int i, exit_code, g_exec_index = -1;

		  /************MAX_CMP_BUG_FIX STARTS*************/
		  if(argc < 2){
					 //printf("\n Error : The value of the MAX_CMP doesnot match the command options entered\n"); 
					 //fprintf(stderr, "Usage: %s {-options} executable {arguments eg:- bzip.arg}\n", argv[0]);

					 //return 0;
					 usage(stderr, argc, argv, -1);
					 exit(1);
		  }
		  /************MAX_CMP_BUG_FIX ENDS*************/

#ifndef _MSC_VER
		  /* catch SIGUSR1 and dump intermediate stats */
		  signal(SIGUSR1, signal_sim_stats);


		  /* catch SIGUSR2 and dump final stats and exit */
		  signal(SIGUSR2, signal_exit_now);
#endif /* _MSC_VER */

		  /* register an error handler */
		  fatal_hook(sim_print_stats);

		  /* set up a non-local exit point */
		  /* sudiptac :::: Moved the code to sim_main as we dont want to exit
			* from simulation as soon as any of the context is expired */

		  /* if ((exit_code = setjmp(sim_exit_buf)) != 0){ */
		  /* special handling as longjmp cannot pass 0 */
		  /* exit_now(exit_code); */
		  /*}*/

#ifdef BFD_LOADER
		  /* initialize the bfd library */
		  bfd_init();
#endif /* BFD_LOADER */

		  /* initialize the instruction decoder */
		  md_init_decoder();
 
		  /* register global options */
		  shared_sim_odb = opt_new(orphan_fn);
		  opt_reg_flag(shared_sim_odb, "-h", "print help message", 
					 &help_me, /* default */FALSE, /* !print */FALSE, NULL);
		  opt_reg_flag(shared_sim_odb, "-v", "verbose operation",
					 &verbose, /* default */FALSE, /* !print */FALSE, NULL);
#ifdef DEBUG
		  opt_reg_flag(shared_sim_odb, "-d", "enable debug message",
					 &debugging, /* default */FALSE, /* !print */FALSE, NULL);
#endif /* DEBUG */
		  opt_reg_flag(shared_sim_odb, "-i", "start in Dlite debugger",
					 &dlite_active, /* default */FALSE, /* !print */FALSE, NULL);
		  opt_reg_int(shared_sim_odb, "-seed",
					 "random number generator seed (0 for timer seed)",
					 &rand_seed, /* default */1, /* print */TRUE, NULL);
		  opt_reg_flag(shared_sim_odb, "-q", "initialize and terminate immediately",
					 &init_quit, /* default */FALSE, /* !print */FALSE, NULL);
		  opt_reg_string(shared_sim_odb, "-chkpt", "restore EIO trace execution from <fname>",
					 &sim_chkpt_fname, /* default */NULL, /* !print */FALSE, NULL);

		  /* stdio redirection options */
		  opt_reg_string(shared_sim_odb, "-redir:sim",
					 "redirect simulator output to file (non-interactive only)",
					 &sim_simout,
					 /* default */NULL, /* !print */FALSE, NULL);
		  opt_reg_string(shared_sim_odb, "-redir:prog",
					 "redirect simulated program output to file",
					 &sim_progout, /* default */NULL, /* !print */FALSE, NULL);

#ifndef _MSC_VER
		  /* scheduling priority option */
		  opt_reg_int(shared_sim_odb, "-nice",
					 "simulator scheduling priority", &nice_priority,
					 /* default */NICE_DEFAULT_VALUE, /* print */TRUE, NULL);
#endif
		  /* sudiptac :::: register shared resource related options */ 
		  shared_sim_reg_options(shared_sim_odb);
		  
		  /* sudiptac :::: read the processor options that are shared by all available cores */
		  exec_index = -1;
		  opt_process_options(shared_sim_odb, argc, argv);
		  g_exec_index = exec_index;

		  /* sudiptac :::: initialize all context (core) data structure */
		  init_all_cores(MAX_CMP);
		 
		  /* sudiptac ::: sanity check, number of executables must be equal 
			* to the MAX_CMP */
		  if (g_exec_index + MAX_CMP != argc || g_exec_index < 0) 
					 fatal("\n Error : The value of the MAX_CMP doesnot match the command options entered\n"); 

		  /*******************************START OF  CMP SPECIFIC CHANGES*******************************/
		  /* Support added to load multiple binaries */
		  while(current_context < MAX_CMP){
					 /* initalize the next context */
					 /* sudiptac :::: added the "-sconfig" option */ 
					 /* g_exec_index tracks the first position of the binary to execute */
					 init_core(envp, argv[g_exec_index + current_context], current_context);

					 sim_check_options(contexts[current_context].sim_odb, argc, argv, current_context);
					 contexts[current_context].sim_sdb = stat_new();
					 sim_reg_stats(contexts[current_context].sim_sdb, current_context);

					 /* sudiptac: adding the preemption support */
					 if (contexts[current_context].preempting_task) {
								/* copy the context in a backup context, this will be required to switch the context 
								 * back and forth during preemption */
								memcpy(&backup_contexts[current_context], &contexts[current_context], sizeof(context));
					 }

					 /* redirect I/O? */
					 if (sim_simout != NULL){
								/* send simulator non-interactive output (STDERR) to file SIM_SIMOUT */
								fflush(stderr);
								if (!freopen(sim_simout, "w", stderr)){
										  fatal("unable to redirect simulator output to file `%s'", sim_simout);
								}	
					 }

					 if (sim_progout != NULL){
								/* redirect simulated program output to file SIM_PROGOUT */
								sim_progfd = fopen(sim_progout, "w");
								if (!sim_progfd){
										  fatal("unable to redirect program output to file `%s'", sim_progout);
								}
					 }	

					 /* need at least two argv values to run */
					 if (argc < 2){
								banner(contexts[current_context].outfile, argc, argv);
								usage(contexts[current_context].outfile, argc, argv,i);
								exit(1);
					 }

					 /* opening banner */
					 banner(contexts[current_context].outfile, argc, argv);

					 if (help_me){
								/* print help message and exit */
								usage(contexts[current_context].outfile, argc, argv,i);
								exit(1);
					 }

					 /* seed the random number generator */
					 if (rand_seed == 0){
								/* seed with the timer value, true random */
								mysrand(time((time_t *)NULL));
					 }

					 else{
								/* seed with default or user-specified random number generator seed */
								mysrand(rand_seed);
					 }

					 current_context++;
		  }

#if 0
		  /* Set the file name that describes the shared bus arbitration policy */
		  g_shared_bus_filename = argv[current_context + 1];
		  assert(g_shared_bus_filename);
#endif
		  
		  /*******************************END OF  CMP SPECIFIC CHANGES*******************************/

		  /* record start of execution time, used in rate stats */
		  sim_start_time = time((time_t *)NULL);

		  /* emit the command line for later reuse */
		  fprintf(stderr, "sim: command line: ");
		  for (i=0; i < argc; i++){
					 fprintf(stderr, "%s ", argv[i]);
		  }	
		  fprintf(stderr, "\n");

		  /* output simulation conditions */
		  s = ctime(&sim_start_time);

		  if (s[strlen(s)-1] == '\n'){
					 s[strlen(s)-1] = '\0';
		  }	

		  fprintf(stderr, "\nsim: simulation started @ %s, options follow:\n", s);
		  sim_aux_config(stderr);
		  fprintf(stderr, "\n");

		  /* omit option dump time from rate stats */
		  sim_start_time = time((time_t *)NULL);

		  if (init_quit){
					 exit_now(0);
		  }	

		  running = TRUE;

		  sim_main();

		  /* simulation finished early */
		  exit_now(0);

		  return 0;
}
