/* sim-outorder.c - sample out-of-order issue perf simulator implementation
 **************************************************************************
 *     Author :- Rama Sangireddy & Sandeep Baldawa<sob051000@utdallas.edu>
 *	This file has been modified to handle multiple cores. The file has 
 *      undergone major modifications related to the same. If you fix any bugs
 *      do not forget to add tags and then give the explanation 
 *	of those tags in the below format.
 * 
 *   BUGS                		RESOLVER                DATE
 *   Redirection problem for
 *   multiple files                     SANDEEP			02-26-2006  
 *    
 *   
 ***************************************************************************
 *  This file is a part of the SimpleScalar tool suite written by
 * Todd M. Austin as a part of the Multiscalar Research Project.
 *  
 * The tool suite is currently maintained by Doug Burger and Todd M. Austin.
 * 
 * Copyright (C) 1994, 1995, 1996, 1997, 1998 by Todd M. Austin
 *
 * This source file is distributed "as is" in the hope that it will be
 * useful.  The tool set comes with no warranty, and no author or
 * distributor accepts any responsibility for the consequences of its
 * use. 
 * 
 * Everyone is granted permission to copy, modify and redistribute
 * this tool set under the following conditions:
 * 
 *    This source code is distributed for non-commercial use only. 
 *    Please contact the maintainer for restrictions applying to 
 *    commercial use.
 *
 *    Permission is granted to anyone to make or distribute copies
 *    of this source code, either as received or modified, in any
 *    medium, provided that all copyright notices, permission and
 *    nonwarranty notices are preserved, and that the distributor
 *    grants the recipient permission for further redistribution as
 *    permitted by this document.
 *
 *    Permission is granted to distribute this file in compiled
 *    or executable form under the same conditions that apply for
 *    source code, provided that either:
 *
 *    A. it is accompanied by the corresponding machine-readable
 *       source code,
 *    B. it is accompanied by a written offer, with no time limit,
 *       to give anyone a machine-readable copy of the corresponding
 *       source code in return for reimbursement of the cost of
 *       distribution.  This written offer must permit verbatim
 *       duplication by anyone, or
 *    C. it is distributed by someone who received only the
 *       executable form, and is accompanied by a copy of the
 *       written offer of source code that they received concurrently.
 *
 * In other words, you are welcome to use, share and improve this
 * source file.  You are forbidden to forbid anyone else to use, share
 * and improve what you give them.
 *
 * INTERNET: dburger@cs.wisc.edu
 * US Mail:  1210 W. Dayton Street, Madison, WI 53706
 *
 * $Id: sim-outorder.c,v 1.1.1.1 2000/05/26 15:18:58 taustin Exp $
 *
 * $Log: sim-outorder.c,v $
 * Revision 1.1.1.1  2000/05/26 15:18:58  taustin
 * SimpleScalar Tool Set
 *
 *
 * Revision 1.7  1999/12/31 18:50:38  taustin
 * quad_t naming conflicts removed
				* added retirement tracing to sim-outorder (enable with -v)
* speculative execution should now be deterministic (uninit bugs fixed...)
				* sim-outorder now stops after sim_num_insn
				*
				* Revision 1.6  1999/12/13 18:46:40  taustin
				* cross endian execution support added
				*
				* Revision 1.5  1998/08/27 16:27:48  taustin
				* implemented host interface description in host.h
				* added target interface support
				* added support for register and memory contexts
				* instruction predecoding moved to loader module
				* Alpha target support added
				* added support for qword's
				* added fault support
				* added option ("-max:inst") to limit number of instructions analyzed
				* explicit BTB sizing option added to branch predictors, use
				*       "-btb" option to configure BTB
				* added queue statistics for IFQ, RUU, and LSQ; all terms of Little's
				*       law are measured and reports; also, measures fraction of cycles
				*       in which queue is full
				* added fast forward option ("-fastfwd") that skips a specified number
				*       of instructions (using functional simulation) before starting timing
				*       simulation
				* sim-outorder speculative loads no longer allocate memory pages,
				*       this significantly reduces memory requirements for programs with
*       lots of mispeculation (e.g., cc1)
				* branch predictor updates can now optionally occur in ID, WB,
				*       or CT
				* added target-dependent myprintf() support
* fixed speculative qword store bug (missing most significant word)
				* sim-outorder now computes correct result when non-speculative register
				*       operand is first defined speculative within the same inst
				* speculative fault handling simplified
				* dead variable "no_ea_dep" removed
				*
				* Revision 1.4  1997/04/16  22:10:23  taustin
* added -commit:width support (from kskadron)
				* fixed "bad l2 D-cache parms" fatal string
				*
				* Revision 1.3  1997/03/11  17:17:06  taustin
				* updated copyright
				* `-pcstat' option support added
				* long/int tweaks made for ALPHA target support
				* better defaults defined for caches/TLBs
				* "mstate" command supported added for DLite!
				* supported added for non-GNU C compilers
				* buglet fixed in speculative trace generation
				* multi-level cache hierarchy now supported
				* two-level predictor supported added
				* I/D-TLB supported added
				* many comments added
				* options package supported added
				* stats package support added
				* resource configuration options extended
				* pipetrace support added
				* DLite! support added
				* writeback throttling now supported
				* decode and issue B/W now decoupled
				* new and improved (and more precise) memory scheduler added
				* cruft for TLB paper removed
				*
				* Revision 1.1  1996/12/05  18:52:32  taustin
				* Initial revision
				*
				*
				*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <signal.h>

#include "host.h"
#include "misc.h"
#include "machine.h"
#include "regs.h"
#include "memory.h"
#include "cache.h"
#include "loader.h"
#include "syscall.h"
#include "bpred.h"
#include "resource.h"
#include "bitmap.h"
#include "options.h"
#include "eval.h"
#include "stats.h"
#include "ptrace.h"
#include "dlite.h"
#include "sim.h"
#include "sim_cmp.h"
				/* sudiptac :::: For shared bus modeling */
#include "sharedbus.h"

				/*
				 * This file implements a very detailed out-of-order issue superscalar
				 * processor with a two-level memory system and speculative execution support.
				 * This simulator is a performance simulator, tracking the latency of all
				 * pipeline operations.
				 */


				/******ADD ALL EXTERN VARIABLES HERE*********************************/
				/***Variable to share the value of current context with other files*/
				extern context* contexts;
				/* sudiptac: added to handle single preemption */
				extern context* backup_contexts;
				extern int current_context;
				/* number of cores, default = 1 */
				extern int max_cores;
				/* number of cores sharing L2 icache and L2 dcache */
				extern int max_cores_sharing_il2;
				extern int max_cores_sharing_dl2;
				/* bus slot length, default = 0 */
				extern int fx_slot_length;
				/* shared bus type, default = - 1 */
				extern SHARED_BUS_TYPE g_shared_bus_type;
				/*********************************************************************/



				/***This indicates the number of contexts expired as of now****/
				static int no_context_expired;
				/********This value indicates the number of processors that are accessing the memory****/
				static int no_process_entered_shared_memory;

				/* sudiptac :::: effective commit of an instruction */
				int effect_commit = 0;
				int effect_l2_miss = 0;
				int effect_miss = 0;

				/* chronos related changes */
				static int my_last_inst_missed_in_L1 = FALSE;
				static int my_last_inst_missed_in_l2 = FALSE;

				/* sudiptac :::: made it extern global... actual declaration moved to main.c */ 
				extern int mem_lat[2];

				/* text-based stat profiles */
#define MAX_PCSTAT_VARS 8

				/* convert 64-bit inst text addresses to 32-bit inst equivalents */
#ifdef TARGET_PISA
#define IACOMPRESS(A,ADDRS,ENV)							\
								((ADDRS) ? ((((A) - (contexts[context_id].mem)->ld_text_base) >> 1) + (contexts[context_id].mem)->ld_text_base) : (A))
#define ISCOMPRESS(SZ,ADDRS)							\
								((ADDRS) ? ((SZ) >> 1) : (SZ))
#else /* !TARGET_PISA */
#define IACOMPRESS(A,ADDRS,ENV)		(A)
#define ISCOMPRESS(SZ,ADDRS)		(SZ)
#endif /* TARGET_PISA */

				/* operate in backward-compatible bugs mode (for testing only) */
				static int bugcompat_mode;

				/* cycle counter */
				static tick_t sim_cycle = 0;

				/* wedge all stat values into a counter_t */
#define STATVAL(STAT)							\
								((STAT)->sc == sc_int							\
								 ? (counter_t)*((STAT)->variant.for_int.var)			\
								 : ((STAT)->sc == sc_uint						\
												 ? (counter_t)*((STAT)->variant.for_uint.var)		\
												 : ((STAT)->sc == sc_counter					\
																 ? *((STAT)->variant.for_counter.var)				\
																 : (panic("bad stat class"), 0))))

static void init_context(context *c, int c_id )
{
				char str[20];

				/* allocate and initialize register file */
				regs_init(&c->regs);
				regs_init(&c->spec_regs);
				c->context_expired=0;	
				c->regs.context_id = c_id;
				c->spec_mode = FALSE;
				c->ptrace_seq = 0;
				c->mem_nelt=2;	
				c->ptrace_nelt = 0;
				c->bimod_nelt = 1;
				c->bimod_config[0] = 2048;
				c->twolev_nelt = 4;
				c->twolev_config[0] = 1; /* l1size */
				c->twolev_config[1] = 1024; /* l2size */
				c->twolev_config[2] = 8; /* hist */;
				c->twolev_config[3] = FALSE; /* xor */
				c->comb_nelt = 1;
				c->comb_config[0] = 1024; /* meta_table_size */
				c->ras_size = 8;
				c->btb_nelt = 2;
				c->btb_config[0] = 512;/* nsets */
				c->btb_config[1] = 4;/* assoc */
				c->ruu_include_spec = TRUE;
				c->RUU_size = 8;
				c->LSQ_size = 4;
				c->pcstat_nelt = 0;

				/*************************CAUTION************************/
				/* sudiptac: following functional units and their relevant 
				 * parameters are set and made hard-coded to maintain 
				 * consistency with equivalent parameters of our analyzer 
				 * (Chronos)
				 */ 
				c->fu_config[0].name          = "integer-ALU";
				c->fu_config[0].quantity      = 1;
				c->fu_config[0].busy          = 0;
				c->fu_config[0].x[0].class    = IntALU;
				c->fu_config[0].x[0].oplat    = 1;
				c->fu_config[0].x[0].issuelat = 1;

				c->fu_config[1].name             = "integer-MULT/DIV";
				c->fu_config[1].quantity         = 1;
				c->fu_config[1].busy             = 0;
				c->fu_config[1].x[0].class		  = IntMULT;
				//c->fu_config[1].x[0].oplat		  = 3;
				c->fu_config[1].x[0].oplat		  = 4;
				c->fu_config[1].x[0].issuelat	  = 4;
				c->fu_config[1].x[1].class		  = IntDIV;
				//c->fu_config[1].x[1].oplat		  = 20;
				c->fu_config[1].x[1].oplat		  = 4;
				//c->fu_config[1].x[1].issuelat	  = 19;
				c->fu_config[1].x[1].issuelat	  = 4;

				c->fu_config[2].name             = "memory-port";
				c->fu_config[2].quantity         = 2;
				c->fu_config[2].busy             = 0;
				c->fu_config[2].x[0].class		  = RdPort;
				c->fu_config[2].x[0].oplat		  = 1;
				c->fu_config[2].x[0].issuelat	  = 1;
				c->fu_config[2].x[1].class		  = WrPort;
				c->fu_config[2].x[1].oplat		  = 1;
				c->fu_config[2].x[1].issuelat	  = 1;

				c->fu_config[3].name             = "FP-adder";
				c->fu_config[3].quantity         = 1;
				c->fu_config[3].busy             = 0;
				c->fu_config[3].x[0].class		  = FloatADD;
				//c->fu_config[3].x[0].oplat		  = 2;
				c->fu_config[3].x[0].oplat		  = 2;
				c->fu_config[3].x[0].issuelat	  = 2;
				c->fu_config[3].x[1].class		  = FloatCMP;
				//c->fu_config[3].x[1].oplat		  = 2;
				c->fu_config[3].x[1].oplat		  = 2;
				c->fu_config[3].x[1].issuelat	  = 2;
				c->fu_config[3].x[2].class		  = FloatCVT;
				//c->fu_config[3].x[2].oplat		  = 2;
				c->fu_config[3].x[2].oplat		  = 2;
				c->fu_config[3].x[2].issuelat	  = 2;

				c->fu_config[4].name             = "FP-MULT/DIV";
				c->fu_config[4].quantity         = 1;
				c->fu_config[4].busy             = 0;
				c->fu_config[4].x[0].class		  = FloatMULT;
				//c->fu_config[4].x[0].oplat		  = 4;
				c->fu_config[4].x[0].oplat		  = 12;
				c->fu_config[4].x[0].issuelat	  = 12;
				c->fu_config[4].x[1].class		  = FloatDIV;
				//c->fu_config[4].x[1].oplat		  = 12;
				c->fu_config[4].x[1].oplat		  = 12;
				//c->fu_config[4].x[1].issuelat	  = 12;
				c->fu_config[4].x[1].issuelat	  = 12;
				c->fu_config[4].x[2].class		  = FloatSQRT;
				//c->fu_config[4].x[2].oplat		  = 24;
				c->fu_config[4].x[2].oplat		  = 12;
				//c->fu_config[4].x[2].issuelat	  = 24;
				c->fu_config[4].x[2].issuelat	  = 12;

#if 0
				c->fu_config[0].name          = "integer-ALU";
				c->fu_config[0].quantity      = 4;
				c->fu_config[0].busy          = 0;
				c->fu_config[0].x[0].class    = IntALU;
				c->fu_config[0].x[0].oplat    = 1;
				c->fu_config[0].x[0].issuelat = 1;

				c->fu_config[1].name             = "integer-MULT/DIV";
				c->fu_config[1].quantity         = 1;
				c->fu_config[1].busy             = 0;
				c->fu_config[1].x[0].class		  = IntMULT;
				//c->fu_config[1].x[0].oplat		  = 3;
				c->fu_config[1].x[0].oplat		  = 1;
				c->fu_config[1].x[0].issuelat	  = 1;
				c->fu_config[1].x[1].class		  = IntDIV;
				//c->fu_config[1].x[1].oplat		  = 20;
				c->fu_config[1].x[1].oplat		  = 1;
				//c->fu_config[1].x[1].issuelat	  = 19;
				c->fu_config[1].x[1].issuelat	  = 1;

				c->fu_config[2].name             = "memory-port";
				c->fu_config[2].quantity         = 2;
				c->fu_config[2].busy             = 0;
				c->fu_config[2].x[0].class		  = RdPort;
				c->fu_config[2].x[0].oplat		  = 1;
				c->fu_config[2].x[0].issuelat	  = 1;
				c->fu_config[2].x[1].class		  = WrPort;
				c->fu_config[2].x[1].oplat		  = 1;
				c->fu_config[2].x[1].issuelat	  = 1;

				c->fu_config[3].name             = "FP-adder";
				c->fu_config[3].quantity         = 4;
				c->fu_config[3].busy             = 0;
				c->fu_config[3].x[0].class		  = FloatADD;
				//c->fu_config[3].x[0].oplat		  = 2;
				c->fu_config[3].x[0].oplat		  = 1;
				c->fu_config[3].x[0].issuelat	  = 1;
				c->fu_config[3].x[1].class		  = FloatCMP;
				//c->fu_config[3].x[1].oplat		  = 2;
				c->fu_config[3].x[1].oplat		  = 1;
				c->fu_config[3].x[1].issuelat	  = 1;
				c->fu_config[3].x[2].class		  = FloatCVT;
				//c->fu_config[3].x[2].oplat		  = 2;
				c->fu_config[3].x[2].oplat		  = 1;
				c->fu_config[3].x[2].issuelat	  = 1;

				c->fu_config[4].name             = "FP-MULT/DIV";
				c->fu_config[4].quantity         = 1;
				c->fu_config[4].busy             = 0;
				c->fu_config[4].x[0].class		  = FloatMULT;
				//c->fu_config[4].x[0].oplat		  = 4;
				c->fu_config[4].x[0].oplat		  = 1;
				c->fu_config[4].x[0].issuelat	  = 1;
				c->fu_config[4].x[1].class		  = FloatDIV;
				//c->fu_config[4].x[1].oplat		  = 12;
				c->fu_config[4].x[1].oplat		  = 1;
				//c->fu_config[4].x[1].issuelat	  = 12;
				c->fu_config[4].x[1].issuelat	  = 1;
				c->fu_config[4].x[2].class		  = FloatSQRT;
				//c->fu_config[4].x[2].oplat		  = 24;
				c->fu_config[4].x[2].oplat		  = 1;
				//c->fu_config[4].x[2].issuelat	  = 24;
				c->fu_config[4].x[2].issuelat	  = 1;
#endif
				c->sim_slip = 0;
				c->sim_total_insn = 0;
				c->sim_num_refs = 0;
				c->sim_total_refs = 0;
				c->sim_num_loads = 0;
				c->sim_total_loads = 0;
				c->sim_num_branches = 0;
				c->sim_total_branches = 0;
				c->inst_seq = 0;
				c->ptrace_seq = 0;
				c->spec_mode = FALSE;
				c->ruu_fetch_issue_delay = 0;
				c->pred_perfect = FALSE;
				c->fu_pool = NULL;
				c->bucket_free_list = NULL;
				c->last_op.next  = NULL;
				c->last_op.rs    = NULL;
				c->last_op.tag   = 0;
				c->last_inst_missed = FALSE;
				c->last_inst_tmissed = FALSE;
				/* allocate and initialize memory space */
				sprintf(str,"mem_%d",c->id);
				c->mem = mem_create(str);
				mem_init(c->mem);
				c->mem->ld_text_base = 0;
				c->mem->ld_text_size = 0;
				c->mem->ld_data_base = 0;
				c->mem->ld_brk_point = 0;
				c->mem->ld_data_size = 0;
				c->mem->ld_stack_base = 0;
				c->mem->ld_stack_size = 0;
				c->mem->ld_stack_min = -1;
				c->mem->ld_prog_fname = NULL;
				c->mem->ld_prog_entry = 0;
				c->mem->ld_environ_base = 0;
				c->mem->ld_target_big_endian = 0;
				c->mem->context_id = c_id;
				c->id = c_id;
				c->regs.context_id = c_id;

				c->fetch_issue_delay = 0;
				c->icount = 0;
				c->sim_num_insn = 0;

}

/* sudiptac: build a virtual to physical core mapping. A virtual core is 
 * assigned to each task, however, a physical core can be assigned to 
 * multiple tasks (multi-tasking). This map is introduced to simulate the 
 * timing behavior of multi-tasking system (e.g. cache related preemption 
 * delay) */
static void update_virtual_to_physical_core_mapping() 
{
				int i, j;

				/* initialize the virtual to physical core map ====
				 * by default the map is an identical map i.e. 
				 * map[i] = i; */
				virtual_to_physical_core_map = (char *) malloc(MAX_CMP * sizeof(char));
				for (i = 0; i < MAX_CMP; i++)
								virtual_to_physical_core_map[i] = i;

				for (i = 0; i < MAX_CMP; i++) {
								/* there is no task which can preempt task "i", so skip 
								 * this part */
								if (contexts[i].preempting_task == 0)
												continue;
								/* sanity check */
								assert(contexts[i].preempting_task_name && "preempting task name not found");
								for (j = 0; j < MAX_CMP; j++) {
												if (strcmp(contexts[j].mem->ld_prog_fname, contexts[i].preempting_task_name) == 0) {
																/* actually task "j" is running in core "i". so set appropriate 
																 * physical core */
																printf("I got a high priority task %s which may preempt task %s\n", \
																				contexts[j].mem->ld_prog_fname, contexts[i].mem->ld_prog_fname);
																virtual_to_physical_core_map[j] = i;
																contexts[i].preempting_context = j;
												}
								}
				}
}

/* save a context "c" in a back up context "b". Used when preemptionpreempting task name not found 
 * is possible by a high priority task */
static void save_context(context* b, context* c)
{
				memcpy(b, c, sizeof(context));
}

/* load a context "c" from a back up context "b". Used when preemption 
 * is possible by a high priority task */
static void load_context(context* c, context* b)
{
				memcpy(c, b, sizeof(context));
}

/* memory access latency, assumed to not cross a page boundary */
				static unsigned int			/* total latency of access */
mem_access_latency(int blk_sz,int context_id)		/* block size accessed */
{
				int lat=0,i=0;
				int chunks = (blk_sz + (contexts[context_id].mem_bus_width - 1)) / contexts[context_id].mem_bus_width;

				assert(chunks > 0);


				lat += (mem_lat[0] + /* remainder chunk latency */mem_lat[1] * (chunks - 1));

				/* FIXME: Following code is meant for locking the shared cache. Seems not 
				 * required if the shared cache has multiple ports. Currently disabling the
				 * piece of code. Have to confirm with Eric/Abhik/Tulika */
				/*for(i=0; i<MAX_CMP ;i++){
					if( i!= current_context){
					lat += contexts[i].shared_mem_latency ;
					}
					} 

					contexts[current_context].shared_mem_latency=lat; */

				if(no_process_entered_shared_memory < MAX_CMP){
								no_process_entered_shared_memory++;
				};

				return (lat);
}


/*
 * cache miss handlers
 */

/* l1 data cache l1 block miss handler function */
				static unsigned int			/* latency of block access */
dl1_access_fn(enum mem_cmd cmd,		/* access cmd, Read or Write */
								md_addr_t baddr,		/* block address to access */
								int bsize,		/* size of block to access */
								struct cache_blk_t *blk,	/* ptr to block in upper level */
								tick_t now,		/* time of access */
								int context_id)		/* icontext_id for the access */
{
				unsigned int lat;

				if (contexts[context_id].cache_dl2)
				{
								/* access next level of data cache hierarchy */
								lat = cache_access(contexts[context_id].cache_dl2, cmd, baddr,context_id, NULL, bsize,
																/* now */now, /* pudata */NULL, /* repl addr */NULL);
								if (cmd == Read)
												return lat;
								else
								{
												/* FIXME: unlimited write buffers */
												return 0;
								}
				}
				else
				{
								/* access main memory */
								if (cmd == Read)
												return mem_access_latency(bsize,context_id);
								else
								{
												/* FIXME: unlimited write buffers */
												return 0;
								}
				}
}

/* l2 data cache block miss handler function */
				static unsigned int			/* latency of block access */
dl2_access_fn(enum mem_cmd cmd,		/* access cmd, Read or Write */
								md_addr_t baddr,		/* block address to access */
								int bsize,		/* size of block to access */
								struct cache_blk_t *blk,	/* ptr to block in upper level */
								tick_t now,		/* time of access */
								int context_id)		/* context id of the access*/
{
				/* this is a miss to the lowest level, so access main memory */
				if (cmd == Read)
								return mem_access_latency(bsize,context_id);
				else
				{
								/* FIXME: unlimited write buffers */
								return 0;
				}
}

/* l1 inst cache l1 block miss handler function */
				static unsigned int						/* latency of block access */
il1_access_fn(enum mem_cmd cmd,		/* access cmd, Read or Write */
								md_addr_t baddr,				/* block address to access */
								int bsize,						/* size of block to access */
								struct cache_blk_t *blk,	/* ptr to block in upper level */
								tick_t now,						/* time of access */
								int context_id)				/* context id of the access*/
{
				unsigned int lat;
				int bdelay = 0;
				int effect_bus_delay = 0;
				int offset = 0;
				int fx_interval = fx_slot_length * max_cores_sharing_il2;


				/* compute the offset for bus access */
				/* shared bus has been disabled || isolated access */
				if (fx_interval == 0 || max_cores_sharing_il2 == 1)
								offset = 0;
				else
								offset = now % fx_interval - (context_id % max_cores_sharing_il2) * fx_slot_length;

#if 0
				/* Handle effective i-cache misses */	 
				if(baddr >= contexts[context_id].mem->start_addr && 
												baddr < contexts[context_id].mem->end_addr)	  
								contexts[context_id].effect_icache_l1_miss++;	  
#endif

				if (contexts[context_id].cache_il2)
				{
								/* for multi-tasking, make sure you are not accessing the virtual cache */
								int phy_context_id; 
								
								if(baddr >= contexts[context_id].mem->start_addr && baddr < contexts[context_id].mem->end_addr)	  
												phy_context_id = virtual_to_physical_core_map[context_id];
								else
												phy_context_id = context_id;

								/* access next level of inst cache hierarchy */
								lat = cache_access(contexts[phy_context_id].cache_il2, cmd, baddr, context_id, NULL, bsize,
																/* now */now, /* pudata */NULL, /* repl addr */NULL);

								/* Compute bus delay ---- no split transaction allowed */
								bdelay = compute_bus_delay(offset, lat);

								if(baddr >= contexts[context_id].mem->start_addr && 
																baddr < contexts[context_id].mem->end_addr)
								{
#if 0
												if (lat > contexts[context_id].cache_il2_lat)
																contexts[context_id].effect_icache_l2_miss++;	  
#endif
												effect_bus_delay = bdelay;
								}
								else
												effect_bus_delay = 0;

								/* accumulate the total bus delay suffered (for debug) */
								contexts[context_id].bdelay += bdelay;

								/* sudiptac :::: account for the bus delay in case of an 
								 * effective L1 cache miss */
								contexts[context_id].effect_bus_delay += effect_bus_delay;

								if(!context_id && bdelay > 0)
												printf("context = %d, baddr = 0x%x, lat = %d, L2 bdelay = %d (%d)\n", context_id, baddr, lat, \
																				effect_bus_delay, contexts[context_id].effect_bus_delay);

								if (cmd == Read)
												return (effect_bus_delay+lat);
								else
												panic("writes to instruction memory not supported");
				}
				else
				{
								/* access main memory */
								if (cmd == Read) {
												lat = mem_access_latency(bsize,context_id);

												/* Compute bus delay ---- no split transaction allowed */
												bdelay = compute_bus_delay(offset, lat);

												if(baddr >= contexts[context_id].mem->start_addr && 
																				baddr < contexts[context_id].mem->end_addr)	  
																effect_bus_delay = bdelay;
												else
																effect_bus_delay = 0;

												/* accumulate the total bus delay suffered (for debug) */
												contexts[context_id].bdelay += bdelay;

												/* sudiptac :::: account for the bus delay in case of an 
												 * effective L1 cache miss */
												contexts[context_id].effect_bus_delay += effect_bus_delay;

												return (bdelay+lat);
								}  
								else
												panic("writes to instruction memory not supported");
				}
}

/* l2 inst cache block miss handler function */
				static unsigned int			/* latency of block access */
il2_access_fn(enum mem_cmd cmd,		/* access cmd, Read or Write */
								md_addr_t baddr,		/* block address to access */
								int bsize,		/* size of block to access */
								struct cache_blk_t *blk,	/* ptr to block in upper level */
								tick_t now,		/* time of access */
								int context_id)		/* context id of the access*/
{
				/* this is a miss to the lowest level, so access main memory */
				if (cmd == Read)
				{
								return mem_access_latency(bsize,context_id);
				} 	 
				else
								panic("writes to instruction memory not supported");
}


/*
 * TLB miss handlers
 */

/* inst cache block miss handler function */
				static unsigned int			/* latency of block access */
itlb_access_fn(enum mem_cmd cmd,	/* access cmd, Read or Write */
								md_addr_t baddr,		/* block address to access */
								int bsize,		/* size of block to access */
								struct cache_blk_t *blk,	/* ptr to block in upper level */
								tick_t now,		/* time of access */
								int context_id)		/* context id of the access*/
{
				md_addr_t *phy_page_ptr = (md_addr_t *)blk->user_data;

				/* no real memory access, however, should have user data space attached */
				assert(phy_page_ptr);

				/* fake translation, for now... */
				*phy_page_ptr = 0;

				/* return tlb miss latency */
				return contexts[context_id].tlb_miss_lat;
}

/* data cache block miss handler function */
				static unsigned int			/* latency of block access */
dtlb_access_fn(enum mem_cmd cmd,	/* access cmd, Read or Write */
								md_addr_t baddr,	/* block address to access */
								int bsize,		/* size of block to access */
								struct cache_blk_t *blk,	/* ptr to block in upper level */
								tick_t now,		/* time of access */
								int context_id)
{
				md_addr_t *phy_page_ptr = (md_addr_t *)blk->user_data;

				/* no real memory access, however, should have user data space attached */
				assert(phy_page_ptr);

				/* fake translation, for now... */
				*phy_page_ptr = 0;

				/* return tlb miss latency */
				return contexts[context_id].tlb_miss_lat;
}

/* register simulator-specific options related to shared resources */
				void
shared_sim_reg_options(struct opt_odb_t* shared_sim_odb)
{
				/* sudiptac :::: multi-core specific option */
				/* register the options in database shared by all the available cores */

				/* number of cores */
				opt_reg_int(shared_sim_odb, "-core:ncores", "number of cores in processor",
												&max_cores, /* default */1,
												/* print */TRUE, /* format */NULL);


				/* register cache sharing option */
				opt_reg_int(shared_sim_odb, "-il2:share", "number of cores sharing L2 icache",
												&max_cores_sharing_il2, /* default */1,
												/* print */TRUE, /* format */NULL);

				/* this option is currently not used */
				/* more precisely, L2 icache and L2 dcache currently cannot have differet 
				 * sharing option */
				opt_reg_int(shared_sim_odb, "-dl2:share", "number of cores sharing L2 data cache",
												&max_cores_sharing_dl2, /* default */1,
												/* print */TRUE, /* format */NULL);

				/* register shared bus option */		  		   					  

				/* bus slot length assigned to each core */
				opt_reg_int(shared_sim_odb, "-bus:slotlength", "shared bus slot length",
												&fx_slot_length, /* default */0,
												/* print */TRUE, /* format */NULL);

				/* shared bus type ::: round robin or flexray static */
				opt_reg_int(shared_sim_odb, "-bus:bustype", "shared bus type (RR  = 0 | FLEXRAY STATIC = 1)",
												&g_shared_bus_type, /* default */-1,
												/* print */TRUE, /* format */NULL);
}

/* register simulator-specific options */
				void
sim_reg_options(struct opt_odb_t *odb, struct opt_odb_t* shared_sim_odb, int context_id)
{


				init_context(&contexts[context_id],context_id);

				opt_reg_header(odb, 
												"sim-outorder: This simulator implements a very detailed out-of-order issue\n"
												"superscalar processor with a two-level memory system and speculative\n"
												"execution support.  This simulator is a performance simulator, tracking the\n"
												"latency of all pipeline operations.\n"
											);

				/* instruction limit */

				opt_reg_uint(odb, "-max:inst", "maximum number of inst's to execute",
												&(contexts[context_id].max_insts), /* default */1000000000,
												/* print */TRUE, /* format */NULL);

				/* trace options */

				opt_reg_int(odb, "-fastfwd", "number of insts skipped before timing starts",
												&(contexts[context_id].fastfwd_count), /* default */1000000,
												/* print */TRUE, /* format */NULL);
#if 0
				opt_reg_string_list(odb, "-ptrace",
												"generate pipetrace, i.e., <fname|stdout|stderr> <range>",
												contexts[context_id].ptrace_opts, /* arr_sz */2, &contexts[context_id].ptrace_nelt, /* default */NULL,
												/* !print */FALSE, /* format */NULL, /* !accrue */FALSE);
#endif
				opt_reg_note(odb,
												"  Pipetrace range arguments are formatted as follows:\n"
												"\n"
												"    {{@|#}<start>}:{{@|#|+}<end>}\n"
												"\n"
												"  Both ends of the range are optional, if neither are specified, the entire\n"
												"  execution is traced.  Ranges that start with a `@' designate an address\n"
												"  range to be traced, those that start with an `#' designate a cycle count\n"
												"  range.  All other range values represent an instruction count range.  The\n"
												"  second argument, if specified with a `+', indicates a value relative\n"
												"  to the first argument, e.g., 1000:+100 == 1000:1100.  Program symbols may\n"
												"  be used in all contexts.\n"
												"\n"
												"    Examples:   -ptrace FOO.trc #0:#1000\n"
												"                -ptrace BAR.trc @2000:\n"
												"                -ptrace BLAH.trc :1500\n"
												"                -ptrace UXXE.trc :\n"
												"                -ptrace FOOBAR.trc @main:+278\n"
										);

				/* ifetch options */

				opt_reg_int(odb, "-fetch:ifqsize", "instruction fetch queue size (in insts)",
												&(contexts[context_id].ruu_ifq_size), /* default */4,
												/* print */TRUE, /* format */NULL);

				opt_reg_int(odb, "-fetch:mplat", "extra branch mis-prediction latency",
												&(contexts[context_id].ruu_branch_penalty), /* default */3,
												/* print */TRUE, /* format */NULL);

				opt_reg_int(odb, "-fetch:speed",
												"speed of front-end of machine relative to execution core",
												&(contexts[context_id].fetch_speed), /* default */1,
												/* print */TRUE, /* format */NULL);

				/* branch predictor options */

				opt_reg_note(odb,
												"  Branch predictor configuration examples for 2-level predictor:\n"
												"    Configurations:   N, M, W, X\n"
												"      N   # entries in first level (# of shift register(s))\n"
												"      W   width of shift register(s)\n"
												"      M   # entries in 2nd level (# of counters, or other FSM)\n"
												"      X   (yes-1/no-0) xor history and address for 2nd level index\n"
												"    Sample predictors:\n"
												"      GAg     : 1, W, 2^W, 0\n"
												"      GAp     : 1, W, M (M > 2^W), 0\n"
												"      PAg     : N, W, 2^W, 0\n"
												"      PAp     : N, W, M (M == 2^(N+W)), 0\n"
												"      gshare  : 1, W, 2^W, 1\n"
												"  Predictor `comb' combines a bimodal and a 2-level predictor.\n"
										);

				opt_reg_string(odb, "-bpred",
												"branch predictor type {nottaken|taken|perfect|bimod|2lev|comb}",
												&(contexts[context_id].pred_type), /* default */"bimod",
												/* print */TRUE, /* format */NULL);

				opt_reg_int_list(odb, "-bpred:bimod",
												"bimodal predictor config (<table size>)",
												contexts[context_id].bimod_config, contexts[context_id].bimod_nelt, &(contexts[context_id].bimod_nelt),
												/* default */contexts[context_id].bimod_config,
												/* print */TRUE, /* format */NULL, /* !accrue */FALSE);

				opt_reg_int_list(odb, "-bpred:2lev",
												"2-level predictor config "
												"(<l1size> <l2size> <hist_size> <xor>)",
												contexts[context_id].twolev_config, contexts[context_id].twolev_nelt, &(contexts[context_id].twolev_nelt),
												/* default */contexts[context_id].twolev_config,
												/* print */TRUE, /* format */NULL, /* !accrue */FALSE);

				opt_reg_int_list(odb, "-bpred:comb",
												"combining predictor config (<meta_table_size>)",
												contexts[context_id].comb_config, contexts[context_id].comb_nelt, &(contexts[context_id].comb_nelt),
												/* default */contexts[context_id].comb_config,
												/* print */TRUE, /* format */NULL, /* !accrue */FALSE);

				opt_reg_int(odb, "-bpred:ras",
												"return address stack size (0 for no return stack)",
												&(contexts[context_id].ras_size), /* default */contexts[context_id].ras_size,
												/* print */TRUE, /* format */NULL);

				opt_reg_int_list(odb, "-bpred:btb",
												"BTB config (<num_sets> <associativity>)",
												contexts[context_id].btb_config, contexts[context_id].btb_nelt, &(contexts[context_id].btb_nelt),
												/* default */contexts[context_id].btb_config,
												/* print */TRUE, /* format */NULL, /* !accrue */FALSE);

				opt_reg_string(odb, "-bpred:spec_update",
												"speculative predictors update in {ID|WB} (default non-spec)",
												&(contexts[context_id].bpred_spec_opt), /* default */NULL,
												/* print */TRUE, /* format */NULL);

				/* decode options */

				opt_reg_int(odb, "-decode:width",
												"instruction decode B/W (insts/cycle)",
												&(contexts[context_id].ruu_decode_width), /* default */4,
												/* print */TRUE, /* format */NULL);

				/* issue options */

				opt_reg_int(odb, "-issue:width",
												"instruction issue B/W (insts/cycle)",
												&(contexts[context_id].ruu_issue_width), /* default */4,
												/* print */TRUE, /* format */NULL);

				opt_reg_flag(odb, "-issue:inorder", "run pipeline with in-order issue",
												&(contexts[context_id].ruu_inorder_issue), /* default */FALSE,
												/* print */TRUE, /* format */NULL);

				opt_reg_flag(odb, "-issue:wrongpath",
												"issue instructions down wrong execution paths",
												&(contexts[context_id].ruu_include_spec), /* default */TRUE,
												/* print */TRUE, /* format */NULL);

				/* commit options */

				opt_reg_int(odb, "-commit:width",
												"instruction commit B/W (insts/cycle)",
												&(contexts[context_id].ruu_commit_width), /* default */4,
												/* print */TRUE, /* format */NULL);

				/* register scheduler options */

				opt_reg_int(odb, "-ruu:size",
												"register update unit (RUU) size",
												&(contexts[context_id].RUU_size), /* default */16,
												/* print */TRUE, /* format */NULL);

				/* memory scheduler options  */

				opt_reg_int(odb, "-lsq:size",
												"load/store queue (LSQ) size",
												&(contexts[context_id].LSQ_size), /* default */8,
												/* print */TRUE, /* format */NULL);

				/* cache options */

#if 0
				opt_reg_string(odb, "-cache:dl1",
												"l1 data cache config, i.e., {<config>|none}",
												&(contexts[context_id].cache_dl1_opt), "dl1:128:32:4:l",
												/* print */TRUE, NULL);

#endif
				opt_reg_note(odb,
												"  The cache config parameter <config> has the following format:\n"
												"\n"
												"    <name>:<nsets>:<bsize>:<assoc>:<repl>\n"
												"\n"
												"    <name>   - name of the cache being defined\n"
												"    <nsets>  - number of sets in the cache\n"
												"    <bsize>  - block size of the cache\n"
												"    <assoc>  - associativity of the cache\n"
												"    <repl>   - block replacement strategy, 'l'-LRU, 'f'-FIFO, 'r'-random\n"
												"\n"
												"    Examples:   -cache:dl1 dl1:4096:32:1:l\n"
												"                -dtlb dtlb:128:4096:32:r\n"
										);

				opt_reg_int(odb, "-cache:dl1lat",
												"l1 data cache hit latency (in cycles)",
												&(contexts[context_id].cache_dl1_lat), /* default */1,
												/* print */TRUE, /* format */NULL);

#if 0
				opt_reg_string(odb, "-cache:dl2",
												"l2 data cache config, i.e., {<config>|none}",
												&(contexts[context_id].cache_dl2_opt), "ul2:1024:64:4:l",
												/* print */TRUE, NULL);
#endif
				opt_reg_int(odb, "-cache:dl2lat",
												"l2 data cache hit latency (in cycles)",
												&(contexts[context_id].cache_dl2_lat), /* default */6,
												/* print */TRUE, /* format */NULL);

#if 0
				opt_reg_string(odb, "-cache:il1",
												"l1 inst cache config, i.e., {<config>|dl1|dl2|none}",
												&(contexts[context_id].cache_il1_opt), "il1:512:32:1:l",
												/* print */TRUE, NULL);
#endif
				opt_reg_note(odb,
												"  Cache levels can be unified by pointing a level of the instruction cache\n"
												"  hierarchy at the data cache hiearchy using the \"dl1\" and \"dl2\" cache\n"
												"  configuration arguments.  Most sensible combinations are supported, e.g.,\n"
												"\n"
												"    A unified l2 cache (il2 is pointed at dl2):\n"
												"      -cache:il1 il1:128:64:1:l -cache:il2 dl2\n"
												"      -cache:dl1 dl1:256:32:1:l -cache:dl2 ul2:1024:64:2:l\n"
												"\n"
												"    Or, a fully unified cache hierarchy (il1 pointed at dl1):\n"
												"      -cache:il1 dl1\n"
												"      -cache:dl1 ul1:256:32:1:l -cache:dl2 ul2:1024:64:2:l\n"
										);

				opt_reg_int(odb, "-cache:il1lat",
												"l1 instruction cache hit latency (in cycles)",
												&(contexts[context_id].cache_il1_lat), /* default */1,
												/* print */TRUE, /* format */NULL);

#if 0
				opt_reg_string(odb, "-cache:il2",
												"l2 instruction cache config, i.e., {<config>|dl2|none}",
												&(contexts[context_id].cache_il2_opt), "dl2",
												/* print */TRUE, NULL);
#endif
				opt_reg_int(odb, "-cache:il2lat",
												"l2 instruction cache hit latency (in cycles)",
												&(contexts[context_id].cache_il2_lat), /* default */6,
												/* print */TRUE, /* format */NULL);

				opt_reg_flag(odb, "-cache:flush", "flush caches on system calls",
												&(contexts[context_id].flush_on_syscalls), /* default */FALSE, /* print */TRUE, NULL);

				opt_reg_flag(odb, "-cache:icompress",
												"convert 64-bit inst addresses to 32-bit inst equivalents",
												&(contexts[context_id].compress_icache_addrs), /* default */FALSE,
												/* print */TRUE, NULL);

				/* mem options */
				opt_reg_int_list(odb, "-mem:lat",
												"memory access latency (<first_chunk> <inter_chunk>)",
												mem_lat, contexts[context_id].mem_nelt, &contexts[context_id].mem_nelt, mem_lat,
												/* print */TRUE, /* format */NULL, /* !accrue */FALSE);

				opt_reg_int(odb, "-mem:width", "memory access bus width (in bytes)",
												&contexts[context_id].mem_bus_width, /* default */8,
												/* print */TRUE, /* format */NULL);

				/* TLB options */

				opt_reg_string(odb, "-tlb:itlb",
												"instruction TLB config, i.e., {<config>|none}",
												&(contexts[context_id].itlb_opt), "none", /* print */TRUE, NULL);

				opt_reg_string(odb, "-tlb:dtlb",
												"data TLB config, i.e., {<config>|none}",
												&(contexts[context_id].dtlb_opt), "none", /* print */TRUE, NULL);

				opt_reg_int(odb, "-tlb:lat",
												"inst/data TLB miss latency (in cycles)",
												&(contexts[context_id].tlb_miss_lat), /* default */30,
												/* print */TRUE, /* format */NULL);

				/* resource configuration */

				opt_reg_int(odb, "-res:ialu",
												"total number of integer ALU's available",
												&(contexts[context_id].res_ialu), /* default */contexts[context_id].fu_config[FU_IALU_INDEX].quantity,
												/* print */TRUE, /* format */NULL);

				opt_reg_int(odb, "-res:imult",
												"total number of integer multiplier/dividers available",
												&(contexts[context_id].res_imult), /* default */contexts[context_id].fu_config[FU_IMULT_INDEX].quantity,
												/* print */TRUE, /* format */NULL);

				opt_reg_int(odb, "-res:memport",
												"total number of memory system ports available (to CPU)",
												&(contexts[context_id].res_memport), /* default */contexts[context_id].fu_config[FU_MEMPORT_INDEX].quantity,
												/* print */TRUE, /* format */NULL);

				opt_reg_int(odb, "-res:fpalu",
												"total number of floating point ALU's available",
												&(contexts[context_id].res_fpalu), /* default */contexts[context_id].fu_config[FU_FPALU_INDEX].quantity,
												/* print */TRUE, /* format */NULL);

				opt_reg_int(odb, "-res:fpmult",
												"total number of floating point multiplier/dividers available",
												&(contexts[context_id].res_fpmult), /* default */contexts[context_id].fu_config[FU_FPMULT_INDEX].quantity,
												/* print */TRUE, /* format */NULL);

				opt_reg_string_list(odb, "-pcstat",
												"profile stat(s) against text addr's (mult uses ok)",
												contexts[context_id].pcstat_vars, MAX_PCSTAT_VARS, &(contexts[context_id].pcstat_nelt), NULL,
												/* !print */FALSE, /* format */NULL, /* accrue */TRUE);

				opt_reg_flag(odb, "-bugcompat",
												"operate in backward-compatible bugs mode (for testing only)",
												&bugcompat_mode, /* default */FALSE, /* print */TRUE, NULL);

				/* sudiptac ::: do not read them here, read them in shared_sim_reg_options */
#if 0

				/* sudiptac :::: multi-core specific option */
				/* register the options in database shared by all the available cores */

				/* number of cores */
				opt_reg_int(odb, "-core:ncores", "number of cores in processor",
												&max_cores, /* default */1,
												/* print */TRUE, /* format */NULL);


				/* register shared bus option */		  		   					  

				/* bus slot length assigned to each core */
				opt_reg_int(odb, "-bus:slotlength", "shared bus slot length",
												&fx_slot_length, /* default */0,
												/* print */TRUE, /* format */NULL);

				/* shared bus type ::: round robin or flexray static */
				opt_reg_int(odb, "-bus:bustype", "shared bus type (RR  = 0 | FLEXRAY STATIC = 1)",
												&g_shared_bus_type, /* default */-1,
												/* print */TRUE, /* format */NULL);
#endif
}

/* check simulator-specific option values */
				void
sim_check_options(struct opt_odb_t *odb,        /* options database */
								int argc, char **argv,int context_id)        /* command line arguments */
{
				char name[128], c;
				int nsets, bsize, assoc;

				if ((contexts[context_id].fastfwd_count) < 0 || contexts[context_id].fastfwd_count >= 2147483647)
								fatal("bad fast forward count: %d", contexts[context_id].fastfwd_count);

				if (contexts[context_id].ruu_ifq_size < 1 || (contexts[context_id].ruu_ifq_size & (contexts[context_id].ruu_ifq_size - 1)) != 0)
								fatal("inst fetch queue size must be positive > 0 and a power of two");

				if (contexts[context_id].ruu_branch_penalty < 1)
								fatal("mis-prediction penalty must be at least 1 cycle");

				if (contexts[context_id].fetch_speed < 1)
								fatal("front-end speed must be positive and non-zero");

				if (!mystricmp(contexts[context_id].pred_type, "perfect"))
				{
								/* perfect predictor */
								contexts[context_id].pred = NULL;
								contexts[context_id].pred_perfect = TRUE;
				}
				else if (!mystricmp(contexts[context_id].pred_type, "taken"))
				{
								/* static predictor, not taken */
								contexts[context_id].pred = bpred_create(BPredTaken, 0, 0, 0, 0, 0, 0, 0, 0, 0);
				}
				else if (!mystricmp(contexts[context_id].pred_type, "nottaken"))
				{
								/* static predictor, taken */
								contexts[context_id].pred = bpred_create(BPredNotTaken, 0, 0, 0, 0, 0, 0, 0, 0, 0);
				}
				else if (!mystricmp(contexts[context_id].pred_type, "bimod"))
				{
								/* bimodal predictor, bpred_create() checks BTB_SIZE */
								if (contexts[context_id].bimod_nelt != 1)
												fatal("bad bimod predictor config (<table_size>)");
								if (contexts[context_id].btb_nelt != 2)
												fatal("bad btb config (<num_sets> <associativity>)");

								/* bimodal predictor, bpred_create() checks BTB_SIZE */
								contexts[context_id].pred = bpred_create(BPred2bit,
																/* bimod table size */contexts[context_id].bimod_config[0],
																/* 2lev l1 size */0,
																/* 2lev l2 size */0,
																/* meta table size */0,
																/* history reg size */0,
																/* history xor address */0,
																/* btb sets */contexts[context_id].btb_config[0],
																/* btb assoc */contexts[context_id].btb_config[1],
																/* ret-addr stack size */contexts[context_id].ras_size);
				}
				else if (!mystricmp(contexts[context_id].pred_type, "2lev"))
				{
								/* 2-level adaptive predictor, bpred_create() checks args */
								if (contexts[context_id].twolev_nelt != 4)
												fatal("bad 2-level pred config (<l1size> <l2size> <hist_size> <xor>)");
								if (contexts[context_id].btb_nelt != 2)
												fatal("bad btb config (<num_sets> <associativity>)");

								contexts[context_id].pred = bpred_create(BPred2Level,
																/* bimod table size */0,
																/* 2lev l1 size */contexts[context_id].twolev_config[0],
																/* 2lev l2 size */contexts[context_id].twolev_config[1],
																/* meta table size */0,
																/* history reg size */contexts[context_id].twolev_config[2],
																/* history xor address */contexts[context_id].twolev_config[3],
																/* btb sets */contexts[context_id].btb_config[0],
																/* btb assoc */contexts[context_id].btb_config[1],
																/* ret-addr stack size */contexts[context_id].ras_size);
				}
				else if (!mystricmp(contexts[context_id].pred_type, "comb"))
				{
								/* combining predictor, bpred_create() checks args */
								if (contexts[context_id].twolev_nelt != 4)
												fatal("bad 2-level pred config (<l1size> <l2size> <hist_size> <xor>)");
								if (contexts[context_id].bimod_nelt != 1)
												fatal("bad bimod predictor config (<table_size>)");
								if (contexts[context_id].comb_nelt != 1)
												fatal("bad combining predictor config (<meta_table_size>)");
								if (contexts[context_id].btb_nelt != 2)
												fatal("bad btb config (<num_sets> <associativity>)");

								contexts[context_id].pred = bpred_create(BPredComb,
																/* bimod table size */contexts[context_id].bimod_config[0],
																/* l1 size */contexts[context_id].twolev_config[0],
																/* l2 size */contexts[context_id].twolev_config[1],
																/* meta table size */contexts[context_id].comb_config[0],
																/* history reg size */contexts[context_id].twolev_config[2],
																/* history xor address */contexts[context_id].twolev_config[3],
																/* btb sets */contexts[context_id].btb_config[0],
																/* btb assoc */contexts[context_id].btb_config[1],
																/* ret-addr stack size */contexts[context_id].ras_size);
				}
				else
								fatal("cannot parse predictor type `%s'", contexts[context_id].pred_type);

				if (!contexts[context_id].bpred_spec_opt)
								contexts[context_id].bpred_spec_update = spec_CT;
				else if (!mystricmp(contexts[context_id].bpred_spec_opt, "ID"))
								contexts[context_id].bpred_spec_update = spec_ID;
				else if (!mystricmp(contexts[context_id].bpred_spec_opt, "WB"))
								contexts[context_id].bpred_spec_update = spec_WB;
				else
								fatal("bad speculative update stage specifier, use {ID|WB}");

				if (contexts[context_id].ruu_decode_width < 1 || (contexts[context_id].ruu_decode_width & (contexts[context_id].ruu_decode_width-1)) != 0)
								fatal("issue width must be positive non-zero and a power of two");

				if (contexts[context_id].ruu_issue_width < 1 || (contexts[context_id].ruu_issue_width & (contexts[context_id].ruu_issue_width-1)) != 0)
								fatal("issue width must be positive non-zero and a power of two");

				if (contexts[context_id].ruu_commit_width < 1)
								fatal("commit width must be positive non-zero");

				if (contexts[context_id].RUU_size < 2 || (contexts[context_id].RUU_size & (contexts[context_id].RUU_size-1)) != 0)
								fatal("RUU size must be a positive number > 1 and a power of two");

				if (contexts[context_id].LSQ_size < 2 || (contexts[context_id].LSQ_size & (contexts[context_id].LSQ_size-1)) != 0)
								fatal("contexts[context_id].LSQ size must be a positive number > 1 and a power of two");

				/* use a level 1 D-cache? */
				if (!mystricmp(contexts[context_id].cache_dl1_opt, "none"))
				{
								contexts[context_id].cache_dl1 = NULL;

								/* the level 2 D-cache cannot be defined */
								if (strcmp(contexts[context_id].cache_dl2_opt, "none"))
												fatal("the l1 data cache must defined if the l2 cache is defined");
								contexts[context_id].cache_dl2 = NULL;
				}
				else /* dl1 is defined */
				{


								if (sscanf(contexts[context_id].cache_dl1_opt, "%[^:]:%d:%d:%d:%c",
																				name, &nsets, &bsize, &assoc, &c) != 5)
												fatal("bad l1 D-cache parms: <name>:<nsets>:<bsize>:<assoc>:<repl>");
								contexts[context_id].cache_dl1 = cache_create(name, nsets, bsize, /* balloc */FALSE,
																/* usize */0, assoc, cache_char2policy(c),
																dl1_access_fn, /* hit lat */contexts[context_id].cache_dl1_lat);

								/* is the level 2 D-cache defined? */
								if (!mystricmp(contexts[context_id].cache_dl2_opt, "none"))
												contexts[context_id].cache_dl2 = NULL;
								else
								{
												if (sscanf(contexts[context_id].cache_dl2_opt, "%[^:]:%d:%d:%d:%c",
																								name, &nsets, &bsize, &assoc, &c) != 5)
																fatal("bad l2 D-cache parms: "
																								"<name>:<nsets>:<bsize>:<assoc>:<repl>");
												printf("\n Value of context_id %% NO_CORE_SHARE_L2 in cache_dl2 is %d \n",(context_id % NO_CORE_SHARE_L2));
												if((context_id % NO_CORE_SHARE_L2) == 0){ 
																printf("\n Hence I enter create cache_dl2 \n");
																contexts[context_id].cache_dl2 = cache_create(name, nsets, bsize, /* balloc */FALSE,
																								/* usize */0, assoc, cache_char2policy(c),
																								dl2_access_fn, /* hit lat */contexts[context_id].cache_dl2_lat);
												}
												else{
																/* L2 shared logic */
																contexts[context_id].cache_dl2 =contexts[context_id - 1].cache_dl2;  
												}
								}
				}

				/* use a level 1 I-cache? */
				if (!mystricmp(contexts[context_id].cache_il1_opt, "none"))
				{
								contexts[context_id].cache_il1 = NULL;

								/* the level 2 I-cache cannot be defined */
								if (strcmp(contexts[context_id].cache_il2_opt, "none"))
												fatal("the l1 inst cache must defined if the l2 cache is defined");
								contexts[context_id].cache_il2 = NULL;
				}
				else if (!mystricmp(contexts[context_id].cache_il1_opt, "dl1"))
				{
								if (!contexts[context_id].cache_dl1)
												fatal("I-cache l1 cannot access D-cache l1 as it's undefined");
								contexts[context_id].cache_il1 = contexts[context_id].cache_dl1;

								/* the level 2 I-cache cannot be defined */
								if (strcmp(contexts[context_id].cache_il2_opt, "none"))
												fatal("the l1 inst cache must defined if the l2 cache is defined");
								contexts[context_id].cache_il2 = NULL;
				}
				else if (!mystricmp(contexts[context_id].cache_il1_opt, "dl2"))
				{
								if (!contexts[context_id].cache_dl2)
												fatal("I-cache l1 cannot access D-cache l2 as it's undefined");
								contexts[context_id].cache_il1 = contexts[context_id].cache_dl2;

								/* the level 2 I-cache cannot be defined */
								if (strcmp(contexts[context_id].cache_il2_opt, "none"))
												fatal("the l1 inst cache must defined if the l2 cache is defined");
								contexts[context_id].cache_il2 = NULL;
				}
				else /* il1 is defined */
				{
								if (sscanf(contexts[context_id].cache_il1_opt, "%[^:]:%d:%d:%d:%c",
																				name, &nsets, &bsize, &assoc, &c) != 5)
												fatal("bad l1 I-cache parms: <name>:<nsets>:<bsize>:<assoc>:<repl>");
								contexts[context_id].cache_il1 = cache_create(name, nsets, bsize, /* balloc */FALSE,
																/* usize */0, assoc, cache_char2policy(c),
																il1_access_fn, /* hit lat */contexts[context_id].cache_il1_lat);

								/* is the level 2 D-cache defined? */
								if (!mystricmp(contexts[context_id].cache_il2_opt, "none"))
												contexts[context_id].cache_il2 = NULL;
								else if (!mystricmp(contexts[context_id].cache_il2_opt, "dl2"))
								{
												if (!contexts[context_id].cache_dl2)
																fatal("I-cache l2 cannot access D-cache l2 as it's undefined");
												contexts[context_id].cache_il2 = contexts[context_id].cache_dl2;
								}
								else
								{
												if (sscanf(contexts[context_id].cache_il2_opt, "%[^:]:%d:%d:%d:%c",
																								name, &nsets, &bsize, &assoc, &c) != 5)
																fatal("bad l2 I-cache parms: "
																								"<name>:<nsets>:<bsize>:<assoc>:<repl>");

												if((context_id % NO_CORE_SHARE_L2) == 0){
																contexts[context_id].cache_il2 = cache_create(name, nsets, bsize, /* balloc */FALSE,
																								/* usize */0, assoc, cache_char2policy(c),
																								il2_access_fn, /* hit lat */contexts[context_id].cache_il2_lat);
												}
												else{
																contexts[context_id].cache_il2=contexts[context_id -1].cache_il2;
												}
								}
				}

				/* use an I-TLB? */
				if (!mystricmp(contexts[context_id].itlb_opt, "none"))
								contexts[context_id].itlb = NULL;
				else
				{
								if (sscanf(contexts[context_id].itlb_opt, "%[^:]:%d:%d:%d:%c",
																				name, &nsets, &bsize, &assoc, &c) != 5)
												fatal("bad TLB parms: <name>:<nsets>:<page_size>:<assoc>:<repl>");
								contexts[context_id].itlb = cache_create(name, nsets, bsize, /* balloc */FALSE,
																/* usize */sizeof(md_addr_t), assoc,
																cache_char2policy(c), itlb_access_fn,
																/* hit latency */1);
				}

				/* use a D-TLB? */
				if (!mystricmp(contexts[context_id].dtlb_opt, "none"))
								contexts[context_id].dtlb = NULL;
				else
				{
								if (sscanf(contexts[context_id].dtlb_opt, "%[^:]:%d:%d:%d:%c",
																				name, &nsets, &bsize, &assoc, &c) != 5)
												fatal("bad TLB parms: <name>:<nsets>:<page_size>:<assoc>:<repl>");
								contexts[context_id].dtlb = cache_create(name, nsets, bsize, /* balloc */FALSE,
																/* usize */sizeof(md_addr_t), assoc,
																cache_char2policy(c), dtlb_access_fn,
																/* hit latency */1);
				}

				if (contexts[context_id].cache_dl1_lat < 1)
								fatal("l1 data cache latency must be greater than zero");

				if (contexts[context_id].cache_dl2_lat < 1)
								fatal("l2 data cache latency must be greater than zero");

				if (contexts[context_id].cache_il1_lat < 1)
								fatal("l1 instruction cache latency must be greater than zero");

				if (contexts[context_id].cache_il2_lat < 1)
								fatal("l2 instruction cache latency must be greater than zero");

				/* FIXME: What is this doing here ? */		  
				/* if (contexts[context_id].mem_nelt != 2)
					 fatal("bad memory access latency (<first_chunk> <inter_chunk>)"); */

				if (mem_lat[0] < 1 || mem_lat[1] < 1)
								fatal("all memory access latencies must be greater than zero");

				if (contexts[context_id].mem_bus_width < 1 || (contexts[context_id].mem_bus_width & (contexts[context_id].mem_bus_width-1)) != 0)
								fatal("memory bus width must be positive non-zero and a power of two");

				if (contexts[context_id].tlb_miss_lat < 1)
								fatal("TLB miss latency must be greater than zero");

				if (contexts[context_id].res_ialu < 1)
								fatal("number of integer ALU's must be greater than zero");
				if (contexts[context_id].res_ialu > MAX_INSTS_PER_CLASS)
								fatal("number of integer ALU's must be <= MAX_INSTS_PER_CLASS");
				contexts[context_id].fu_config[FU_IALU_INDEX].quantity = contexts[context_id].res_ialu;

				if (contexts[context_id].res_imult < 1)
								fatal("number of integer multiplier/dividers must be greater than zero");
				if (contexts[context_id].res_imult > MAX_INSTS_PER_CLASS)
								fatal("number of integer mult/div's must be <= MAX_INSTS_PER_CLASS");
				contexts[context_id].fu_config[FU_IMULT_INDEX].quantity = contexts[context_id].res_imult;

				if (contexts[context_id].res_memport < 1)
								fatal("number of memory system ports must be greater than zero");
				if (contexts[context_id].res_memport > MAX_INSTS_PER_CLASS)
								fatal("number of memory system ports must be <= MAX_INSTS_PER_CLASS");
				contexts[context_id].fu_config[FU_MEMPORT_INDEX].quantity = contexts[context_id].res_memport;

				if (contexts[context_id].res_fpalu < 1)
								fatal("number of floating point ALU's must be greater than zero");
				if (contexts[context_id].res_fpalu > MAX_INSTS_PER_CLASS)
								fatal("number of floating point ALU's must be <= MAX_INSTS_PER_CLASS");
				contexts[context_id].fu_config[FU_FPALU_INDEX].quantity = contexts[context_id].res_fpalu;

				if (contexts[context_id].res_fpmult < 1)
								fatal("number of floating point multiplier/dividers must be > zero");
				if (contexts[context_id].res_fpmult > MAX_INSTS_PER_CLASS)
								fatal("number of FP mult/div's must be <= MAX_INSTS_PER_CLASS");
				contexts[context_id].fu_config[FU_FPMULT_INDEX].quantity = contexts[context_id].res_fpmult;
}

/* print simulator-specific configuration information */
				void
sim_aux_config(FILE *stream)            /* output stream */
{
				/* nada */
}

/* register simulator-specific statistics */
				void
sim_reg_stats(struct stat_sdb_t *sdb,int context_id)   /* stats database */
{
				int i;
				stat_reg_counter(contexts[current_context].sim_sdb, "sim_num_insn",
												"total number of instructions committed",
												&contexts[context_id].sim_num_insn, contexts[context_id].sim_num_insn, NULL);
				stat_reg_counter(contexts[current_context].sim_sdb, "sim_num_refs",
												"total number of loads and stores committed",
												&contexts[context_id].sim_num_refs, 0, NULL);
				stat_reg_counter(contexts[current_context].sim_sdb, "sim_num_loads",
												"total number of loads committed",
												&contexts[context_id].sim_num_loads, 0, NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "sim_num_stores",
												"total number of stores committed",
												"sim_num_refs - sim_num_loads", NULL);
				stat_reg_counter(contexts[current_context].sim_sdb, "sim_num_branches",
												"total number of branches committed",
												&contexts[context_id].sim_num_branches, /* initial value */0, /* format */NULL);
				stat_reg_int(contexts[current_context].sim_sdb, "sim_elapsed_time",
												"total simulation time in seconds",
												&sim_elapsed_time, 0, NULL);
				stat_reg_formula(contexts[context_id].sim_sdb, "sim_inst_rate",
												"simulation speed (in insts/sec)",
												"sim_num_insn / sim_elapsed_time", NULL);

				stat_reg_counter(contexts[current_context].sim_sdb, "sim_total_insn",
												"total number of instructions executed",
												&contexts[context_id].sim_total_insn, 0, NULL);
				stat_reg_counter(contexts[current_context].sim_sdb, "sim_total_refs",
												"total number of loads and stores executed",
												&contexts[context_id].sim_total_refs, 0, NULL);
				stat_reg_counter(contexts[current_context].sim_sdb, "sim_total_loads",
												"total number of loads executed",
												&contexts[context_id].sim_total_loads, 0, NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "sim_total_stores",
												"total number of stores executed",
												"sim_total_refs - sim_total_loads", NULL);
				stat_reg_counter(contexts[current_context].sim_sdb, "sim_total_branches",
												"total number of branches executed",
												&contexts[context_id].sim_total_branches, /* initial value */0, /* format */NULL);

				/* register performance stats */
				stat_reg_counter(contexts[current_context].sim_sdb, "sim_cycle",
												"total simulation time in cycles",
												&(contexts[current_context].sim_cycles), /* initial value */0, /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "sim_IPC",
												"instructions per cycle",
												"sim_num_insn / sim_cycle", /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "sim_CPI",
												"cycles per instruction",
												"sim_cycle / sim_num_insn", /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "sim_exec_BW",
												"total instructions (mis-spec + committed) per cycle",
												"sim_total_insn / sim_cycle", /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "sim_IPB",
												"instruction per branch",
												"sim_num_insn / sim_num_branches", /* format */NULL);

				/* occupancy stats */
				stat_reg_counter(contexts[current_context].sim_sdb, "IFQ_count", "cumulative IFQ occupancy",
												&contexts[context_id].IFQ_count, /* initial value */0, /* format */NULL);
				stat_reg_counter(contexts[current_context].sim_sdb, "IFQ_fcount", "cumulative IFQ full count",
												&contexts[context_id].IFQ_fcount, /* initial value */0, /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "ifq_occupancy", "avg IFQ occupancy (insn's)",
												"IFQ_count / sim_cycle", /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "ifq_rate", "avg IFQ dispatch rate (insn/cycle)",
												"sim_total_insn / sim_cycle", /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "ifq_latency", "avg IFQ occupant latency (cycle's)",
												"ifq_occupancy / ifq_rate", /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "ifq_full", "fraction of time (cycle's) IFQ was full",
												"IFQ_fcount / sim_cycle", /* format */NULL);

				stat_reg_counter(contexts[current_context].sim_sdb, "RUU_count", "cumulative RUU occupancy",
												&contexts[context_id].RUU_count, /* initial value */0, /* format */NULL);
				stat_reg_counter(contexts[current_context].sim_sdb, "RUU_fcount", "cumulative RUU full count",
												&contexts[context_id].RUU_fcount, /* initial value */0, /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "ruu_occupancy", "avg RUU occupancy (insn's)",
												"RUU_count / sim_cycle", /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "ruu_rate", "avg RUU dispatch rate (insn/cycle)",
												"sim_total_insn / sim_cycle", /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "ruu_latency", "avg RUU occupant latency (cycle's)",
												"ruu_occupancy / ruu_rate", /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "ruu_full", "fraction of time (cycle's) RUU was full",
												"RUU_fcount / sim_cycle", /* format */NULL);

				stat_reg_counter(contexts[current_context].sim_sdb, "LSQ_count", "cumulative LSQ occupancy",
												&contexts[context_id].LSQ_count, /* initial value */0, /* format */NULL);
				stat_reg_counter(contexts[current_context].sim_sdb, "LSQ_fcount", "cumulative LSQ full count",
												&contexts[context_id].LSQ_fcount, /* initial value */0, /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "lsq_occupancy", "avg LSQ occupancy (insn's)",
												"LSQ_count / sim_cycle", /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "lsq_rate", "avg LSQ dispatch rate (insn/cycle)",
												"sim_total_insn / sim_cycle", /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "lsq_latency", "avg LSQ occupant latency (cycle's)",
												"lsq_occupancy / lsq_rate", /* format */NULL);
				stat_reg_formula(contexts[current_context].sim_sdb, "lsq_full", "fraction of time (cycle's) LSQ was full",
												"LSQ_fcount / sim_cycle", /* format */NULL);

				stat_reg_counter(contexts[current_context].sim_sdb, "sim_slip",
												"total number of slip cycles",
												&contexts[context_id].sim_slip, 0, NULL);
				/* register baseline stats */
				stat_reg_formula(contexts[current_context].sim_sdb, "avg_sim_slip",
												"the average slip between issue and retirement",
												"sim_slip / sim_num_insn", NULL);

				/* register predictor stats */
				if (contexts[context_id].pred)
								bpred_reg_stats(contexts[context_id].pred, contexts[current_context].sim_sdb);

				/* register cache stats */
				if (contexts[context_id].cache_il1
												&& (contexts[context_id].cache_il1 != contexts[context_id].cache_dl1 && contexts[context_id].cache_il1 != contexts[context_id].cache_dl2))
								cache_reg_stats(contexts[context_id].cache_il1, contexts[current_context].sim_sdb);
				if (contexts[context_id].cache_il2
												&& (contexts[context_id].cache_il2 != contexts[context_id].cache_dl1 && contexts[context_id].cache_il2 != contexts[context_id].cache_dl2))
								cache_reg_stats(contexts[context_id].cache_il2, contexts[current_context].sim_sdb);
				if (contexts[context_id].cache_dl1)
								cache_reg_stats(contexts[context_id].cache_dl1, contexts[current_context].sim_sdb);
				if (contexts[context_id].cache_dl2)
								cache_reg_stats(contexts[context_id].cache_dl2, contexts[current_context].sim_sdb);
				if (contexts[context_id].itlb)
								cache_reg_stats(contexts[context_id].itlb, contexts[current_context].sim_sdb);
				if (contexts[context_id].dtlb)
								cache_reg_stats(contexts[context_id].dtlb, contexts[current_context].sim_sdb);

				/* register bus delay status (valid only for multi-core execution) */
				stat_reg_counter(contexts[current_context].sim_sdb, "shared_bus_delay", "Total shared bus delay suffered",
												&contexts[context_id].bdelay, /* initial value */0, /* format */NULL);

				/* debug variable(s) */
				stat_reg_counter(contexts[current_context].sim_sdb, "sim_invalid_addrs",
												"total non-speculative bogus addresses seen (debug var)",
												&contexts[context_id].sim_invalid_addrs, /* initial value */0, /* format */NULL);

				for (i=0; i<contexts[context_id].pcstat_nelt; i++)
				{
								char buf[512], buf1[512];
								struct stat_stat_t *stat;

								/* track the named statistical variable by text address */

								/* find it... */
								stat = stat_find_stat(contexts[current_context].sim_sdb, contexts[context_id].pcstat_vars[i]);
								if (!stat)
												fatal("cannot locate any statistic named `%s'", contexts[context_id].pcstat_vars[i]);

								/* stat must be an integral type */
								if (stat->sc != sc_int && stat->sc != sc_uint && stat->sc != sc_counter)
												fatal("`-pcstat' statistical variable `%s' is not an integral type",
																				stat->name);

								/* register this stat */
								contexts[context_id].pcstat_stats[i] = stat;
								contexts[context_id].pcstat_lastvals[i] = STATVAL(stat);

								/* declare the sparce text distribution */
								sprintf(buf, "%s_by_pc", stat->name);
								sprintf(buf1, "%s (by text address)", stat->desc);
								contexts[context_id].pcstat_sdists[i] = stat_reg_sdist(contexts[current_context].sim_sdb, buf, buf1,
																/* initial value */0,
																/* print format */(PF_COUNT|PF_PDF),
																/* format */"0x%lx %lu %.2f",
																/* print fn */NULL);
				}
				ld_reg_stats(contexts[current_context].sim_sdb,contexts[i].mem);
				mem_reg_stats(contexts[context_id].mem, contexts[current_context].sim_sdb);
}

/* forward declarations */
static void ruu_init(int context_id);
static void lsq_init(int context_id);
static void rslink_init(int nlinks,int context_id);
static void eventq_init(int context_id);
static void readyq_init(int context_id);
static void cv_init(int context_id);
static void tracer_init(int context_id);
static void fetch_init(int context_id);
#if 0
/* initialize the simulator */
				void
sim_init(void)
{
				contexts[context_id].sim_num_refs = 0;

				/* allocate and initialize register file */
				regs_init(&contexts[context_id].regs);

				/* allocate and initialize memory space */
				contexts[context_id].mem = mem_create("mem");
				mem_init(contexts[context_id].mem);
}
#endif
/* default register state accessor, used by DLite */
#if 0
static char *					/* err str, NULL for no err */
simoo_reg_obj(struct regs_t *regs,		/* registers to access */
								int is_write,			/* access type */
								enum md_reg_type rt,		/* reg bank to probe */
								int reg,				/* register number */
								struct eval_value_t *val,int context_id);	/* input, output */

/* default memory state accessor, used by DLite */
static char *					/* err str, NULL for no err */
simoo_mem_obj(struct mem_t *mem,		/* memory space to access */
								int is_write,			/* access type */
								md_addr_t addr,			/* address to access */
								char *p,				/* input/output buffer */
								int nbytes,int context_id);			/* size of access */

/* default machine state accessor, used by DLite */
static char *					/* err str, NULL for no err */
simoo_mstate_obj(FILE *stream,			/* output stream */
								char *cmd,			/* optional command string */
								struct regs_t *regs,		/* registers to access */
								struct mem_t *mem,
								int context_id);		/* memory space to access */

#endif
/* total RS links allocated at program start */
#define MAX_RS_LINKS                    4096

/* load program into simulated state */
				void
sim_load_prog(char *fname,		/* program to load */
								int argc, char **argv,	/* program arguments */
								char **envp,int context_id)		/* program environment */
{

#if 1  
				/* load the program into the next available context */
				struct regs_t* regs = &contexts[context_id].regs;

				//printf("\n****** In sim-outorder file name is : %s \n",fname);
				//printf("\n****** In sim-outorder now loading argv %s \n",argv);
				//printf("\n****** In sim-outorder now loading argc %d \n",argc);
				/* load program text and data, set up environment, memory, and regs */
				ld_load_prog(fname, argc, argv, envp, regs, contexts[context_id].mem, TRUE);

				/* initalize the PC */
				regs->regs_PC = contexts[context_id].mem->ld_prog_entry;
				regs->regs_NPC = contexts[context_id].mem->ld_prog_entry + 4;
				/* initalize the context_id */
				regs->context_id = context_id;

				/* initialize here, so symbols can be loaded */
				if (contexts[context_id].ptrace_nelt == 2){
								/* generate a pipeline trace */
								ptrace_open(/* fname */contexts[context_id].ptrace_opts[0], /* range */contexts[context_id].ptrace_opts[1], contexts[context_id].mem);
				}
				else if (contexts[context_id].ptrace_nelt == 0){
								/* no pipetracing */;
				}
				else{
								fatal("bad pipetrace args, use: <fname|stdout|stderr> <range>");
				}

				/* finish initialization of the simulation engine */
				contexts[context_id].fu_pool = res_create_pool("fu-pool", contexts[context_id].fu_config, N_ELT(contexts[context_id].fu_config));
				rslink_init(MAX_RS_LINKS, context_id);
				tracer_init(context_id);
				fetch_init(context_id);
				cv_init(context_id);
				eventq_init(context_id);
				readyq_init(context_id);
				ruu_init(context_id);
				lsq_init(context_id);

#endif


				/* finish initialization of the simulation engine */
				contexts[context_id].fu_pool = res_create_pool("fu-pool", contexts[context_id].fu_config, N_ELT(contexts[context_id].fu_config));
				rslink_init(MAX_RS_LINKS,context_id);
				tracer_init(context_id);
				fetch_init(context_id);
				cv_init(context_id);
				eventq_init(context_id);
				readyq_init(context_id);
				ruu_init(context_id);
				lsq_init(context_id);
#if 0
				/* initialize the DLite debugger */
				dlite_init(simoo_reg_obj, simoo_mem_obj, simoo_mstate_obj, contexts[context_id].mem,context_id);
#endif

}

/* dump simulator-specific auxiliary simulator statistics */
				void
sim_aux_stats(FILE *stream)             /* output stream */
{
				/* nada */
}




/* total input dependencies possible */
#define MAX_IDEPS               3

/* total output dependencies possible */
#define MAX_ODEPS               2
#define OPERANDS_READY(RS)                                              \
				((RS)->idep_ready[0] && (RS)->idep_ready[1] && (RS)->idep_ready[2])

/* register update unit, combination of reservation stations and reorder
	 buffer device, organized as a circular queue */
//static struct RUU_station *RUU;		/* register update unit */
//static int RUU_head, RUU_tail;		/* RUU head and tail pointers */
//static int RUU_num;			/* num entries currently in RUU */

/* allocate and initialize register update unit (RUU) */
				static void
ruu_init(int context_id)
{
				contexts[context_id].RUU = calloc(contexts[context_id].RUU_size, sizeof(struct RUU_station));
				if (!contexts[context_id].RUU)
								fatal("out of virtual memory");

				contexts[context_id].RUU_num = 0;
				contexts[context_id].RUU_head = contexts[context_id].RUU_tail = 0;
				contexts[context_id].RUU_count = 0;
				contexts[context_id].RUU_fcount = 0;
}

#if 0
/* dump the contents of the RUU */
				static void
ruu_dumpent(struct RUU_station *rs,		/* ptr to RUU station */
								int index,				/* entry index */
								FILE *stream,			/* output stream */
								int header)				/* print header? */
{
				if (!stream)
								stream = stderr;

				if (header)
								fprintf(stream, "idx: %2d: opcode: %s, inst: `",
																index, MD_OP_NAME(rs->op));
				else
								fprintf(stream, "       opcode: %s, inst: `",
																MD_OP_NAME(rs->op));
				md_print_insn(rs->IR, rs->PC, stream);
				fprintf(stream, "'\n");
				myfprintf(stream, "         PC: 0x%08p, NPC: 0x%08p (pred_PC: 0x%08p)\n",
												rs->PC, rs->next_PC, rs->pred_PC);
				fprintf(stream, "         in_LSQ: %s, ea_comp: %s, recover_inst: %s\n",
												rs->in_LSQ ? "t" : "f",
												rs->ea_comp ? "t" : "f",
												rs->recover_inst ? "t" : "f");
				myfprintf(stream, "         spec_mode: %s, addr: 0x%08p, tag: 0x%08x\n",
												rs->spec_mode ? "t" : "f", rs->addr, rs->tag);
				fprintf(stream, "         seq: 0x%08x, ptrace_seq: 0x%08x\n",
												rs->seq, rs->ptrace_seq);
				fprintf(stream, "         queued: %s, issued: %s, completed: %s\n",
												rs->queued ? "t" : "f",
												rs->issued ? "t" : "f",
												rs->completed ? "t" : "f");
				fprintf(stream, "         operands ready: %s\n",
												OPERANDS_READY(rs) ? "t" : "f");
}

#endif

#if 0
/* output stream */
/* dump the contents of the RUU */
				static void
ruu_dump(FILE *stream, int context_id)				/* output stream */
{
				int num, head;
				struct RUU_station *rs;

				if (!stream)
								stream = stderr;

				fprintf(stream, "** RUU state **\n");
				fprintf(stream, "RUU_head: %d, RUU_tail: %d\n", contexts[context_id].RUU_head, contexts[context_id].RUU_tail);
				fprintf(stream, "RUU_num: %d\n", contexts[context_id].RUU_num);

				num = contexts[context_id].RUU_num;
				head = contexts[context_id].RUU_head;
				while (num)
				{
								rs = &(contexts[context_id].RUU_head);
								ruu_dumpent(rs, rs - contexts[context_id].RUU, stream, /* header */TRUE);
								head = (head + 1) % contexts[context_id].RUU_size;
								num--;
				}
}
#endif
#define STORE_OP_INDEX                  0
#define STORE_ADDR_INDEX                1

#define STORE_OP_READY(RS)              ((RS)->idep_ready[STORE_OP_INDEX])
#define STORE_ADDR_READY(RS)            ((RS)->idep_ready[STORE_ADDR_INDEX])

/* allocate and initialize the load/store queue (LSQ) */
				static void
lsq_init(int context_id)
{
				contexts[context_id].LSQ = calloc(contexts[context_id].LSQ_size, sizeof(struct RUU_station));
				if (!contexts[context_id].LSQ)
								fatal("out of virtual memory");

				contexts[context_id].LSQ_num = 0;
				contexts[context_id].LSQ_head = contexts[context_id].LSQ_tail = 0;
				contexts[context_id].LSQ_count = 0;
				contexts[context_id].LSQ_fcount = 0;
}
#if 0
/* dump the contents of the RUU */
				static void
lsq_dump(FILE *stream,int context_id)				/* output stream */
{
				int num, head;
				struct RUU_station *rs;

				if (!stream)
								stream = stderr;

				fprintf(stream, "** LSQ state **\n");
				fprintf(stream, "LSQ_head: %d, LSQ_tail: %d\n", contexts[context_id].LSQ_head, contexts[context_id].LSQ_tail);
				fprintf(stream, "LSQ_num: %d\n", contexts[context_id].LSQ_num);

				num = contexts[context_id].LSQ_num;
				head = contexts[context_id].LSQ_head;
				while (num)
				{
								rs = &contexts[context_id].LSQ[head];
								ruu_dumpent(rs, rs - contexts[context_id].LSQ, stream, /* header */TRUE);
								head = (head + 1) % contexts[context_id].LSQ_size;
								num--;
				}
}
#endif

/* NULL value for an RS link */
#define RSLINK_NULL_DATA		{ NULL, NULL, 0 }
static struct RS_link RSLINK_NULL = RSLINK_NULL_DATA;

/* create and initialize an RS link */
#define RSLINK_INIT(RSL, RS)						\
				((RSL).next = NULL, (RSL).rs = (RS), (RSL).tag = (RS)->tag)

/* non-zero if RS link is NULL */
#define RSLINK_IS_NULL(LINK)            ((LINK)->rs == NULL)

/* non-zero if RS link is to a valid (non-squashed) entry */
#define RSLINK_VALID(LINK)              ((LINK)->tag == (LINK)->rs->tag)

/* extra RUU reservation station pointer */
#define RSLINK_RS(LINK)                 ((LINK)->rs)

/* get a new RS link record */
#define RSLINK_NEW(DST, RS, FREE_LIST)						\
{ struct RS_link *n_link;						\
				if (!(FREE_LIST))						\
				panic("out of rs links");						\
				n_link = (FREE_LIST);						\
				(FREE_LIST) = (FREE_LIST)->next;				\
				n_link->next = NULL;						\
				n_link->rs = (RS); n_link->tag = n_link->rs->tag;			\
				(DST) = n_link;							\
}

/* free an RS link record */
#define RSLINK_FREE(LINK, FREE_LIST)						\
{  struct RS_link *f_link = (LINK);					\
				f_link->rs = NULL; f_link->tag = 0;				\
				f_link->next = (FREE_LIST);					\
				(FREE_LIST) = f_link;						\
}

/* FIXME: could this be faster!!! */
/* free an RS link list */
#define RSLINK_FREE_LIST(LINK, FREE_LIST)						\
{  struct RS_link *fl_link, *fl_link_next;				\
				for (fl_link=(LINK); fl_link; fl_link=fl_link_next)		\
				{								\
								fl_link_next = fl_link->next;					\
								RSLINK_FREE(fl_link,(FREE_LIST));						\
				}								\
}

//* initialize the free RS_LINK pool */
				static void
rslink_init(int nlinks,int context_id)			/* total number of RS_LINK available */
{
				int i;
				struct RS_link *link;

				contexts[context_id].rslink_free_list = NULL;
				for (i=0; i<nlinks; i++)
				{
								link = calloc(1, sizeof(struct RS_link));
								if (!link)
												fatal("out of virtual memory");
								link->next = contexts[context_id].rslink_free_list;
								contexts[context_id].rslink_free_list = link;
				}
}



/* service all functional unit release events, this function is called
	 once per cycle, and it used to step the BUSY timers attached to each
	 functional unit in the function unit resource pool, as long as a functional
	 unit's BUSY count is > 0, it cannot be issued an operation */
				static void
ruu_release_fu(int context_id)
{
				int i;

				/* walk all resource units, decrement busy counts by one */
				for (i=0; i<contexts[context_id].fu_pool->num_resources; i++)
				{
								/* resource is released when BUSY hits zero */
								if (contexts[context_id].fu_pool->resources[i].busy > 0)
												contexts[context_id].fu_pool->resources[i].busy--;
				}
}


/*
 * the execution unit event queue implementation follows, the event queue
 * indicates which instruction will complete next, the writeback handler
 * drains this queue
 */

/* pending event queue, sorted from soonest to latest event (in time), NOTE:
	 RS_LINK nodes are used for the event queue list so that it need not be
	 updated during squash events */
//static struct RS_link *event_queue;

/* initialize the event queue structures */
				static void
eventq_init(int context_id)
{
				contexts[context_id].event_queue = NULL;
}
#if 0
/* dump the contents of the event queue */
				static void
eventq_dump(FILE *stream, int context_id)			/* output stream */
{
				struct RS_link *ev;

				if (!stream)
								stream = stderr;

				fprintf(stream, "** event queue state **\n");

				for (ev = event_queue; ev != NULL; ev = ev->next)
				{
								/* is event still valid? */
								if (RSLINK_VALID(ev))
								{
												struct RUU_station *rs = RSLINK_RS(ev);

												fprintf(stream, "idx: %2d: @ %.0f\n",
																				(int)(rs - (rs->in_LSQ ? contexts[context_id].LSQ : contexts[context_id].RUU)), (double)ev->x.when);
												ruu_dumpent(rs, rs - (rs->in_LSQ ? contexts[context_id].LSQ : contexts[context_id].RUU),
																				stream, /* !header */FALSE);
								}
				}
}
#endif
/* insert an event for RS into the event queue, event queue is sorted from
	 earliest to latest event, event and associated side-effects will be
	 apparent at the start of cycle WHEN */
				static void
eventq_queue_event(struct RUU_station *rs, tick_t when,int context_id)
{
				struct RS_link *prev, *ev, *new_ev;

				if (rs->completed)
								panic("event completed");

				if (when <= sim_cycle)
								panic("event occurred in the past");

				/* get a free event record */
				RSLINK_NEW(new_ev, rs,contexts[context_id].rslink_free_list);
				new_ev->x.when = when;

				/* locate insertion point */
				for (prev=NULL, ev=contexts[context_id].event_queue;
												ev && ev->x.when < when;
												prev=ev, ev=ev->next);

				if (prev)
				{
								/* insert middle or end */
								new_ev->next = prev->next;
								prev->next = new_ev;
				}
				else
				{
								/* insert at beginning */
								new_ev->next = contexts[context_id].event_queue;
								contexts[context_id].event_queue = new_ev;
				}
}

/* return the next event that has already occurred, returns NULL when no
	 remaining events or all remaining events are in the future */
				static struct RUU_station *
eventq_next_event(int context_id)
{
				struct RS_link *ev;

				if (contexts[context_id].event_queue && contexts[context_id].event_queue->x.when <= sim_cycle)
				{
								/* unlink and return first event on priority list */
								ev = contexts[context_id].event_queue;
								contexts[context_id].event_queue = contexts[context_id].event_queue->next;

								/* event still valid? */
								if (RSLINK_VALID(ev))
								{
												struct RUU_station *rs = RSLINK_RS(ev);

												/* reclaim event record */
												RSLINK_FREE(ev,contexts[context_id].rslink_free_list);

												/* event is valid, return resv station */
												return rs;
								}
								else
								{
												/* reclaim event record */
												RSLINK_FREE(ev,contexts[context_id].rslink_free_list);

												/* receiving inst was squashed, return next event */
												return eventq_next_event(context_id);
								}
				}
				else
				{
								/* no event or no event is ready */
								return NULL;
				}
}




/*
 * the ready instruction queue implementation follows, the ready instruction
 * queue indicates which instruction have all of there *register* dependencies
 * satisfied, instruction will issue when 1) all memory dependencies for
 * the instruction have been satisfied (see lsq_refresh() for details on how
 * this is accomplished) and 2) resources are available; ready queue is fully
 * constructed each cycle before any operation is issued from it -- this
 * ensures that instruction issue priorities are properly observed; NOTE:
 * RS_LINK nodes are used for the event queue list so that it need not be
 * updated during squash events
 */

/* the ready instruction queue */
//static struct RS_link *ready_queue;

/* initialize the event queue structures */
				static void
readyq_init(int context_id)
{
				contexts[context_id].ready_queue = NULL;
}
#if 0
/* dump the contents of the ready queue */
				static void
readyq_dump(FILE *stream,int context_id)			/* output stream */
{
				struct RS_link *link;

				if (!stream)
								stream = stderr;

				fprintf(stream, "** ready queue state **\n");

				for (link = contexts[context_id].ready_queue; link != NULL; link = link->next)
				{
								/* is entry still valid? */
								if (RSLINK_VALID(link))
								{
												struct RUU_station *rs = RSLINK_RS(link);

												ruu_dumpent(rs, rs - (rs->in_LSQ ? contexts[context_id].LSQ : contexts[context_id].RUU),
																				stream, /* header */TRUE);
								}
				}
}
#endif
/* insert ready node into the ready list using ready instruction scheduling
	 policy; currently the following scheduling policy is enforced:

	 memory and long latency operands, and branch instructions first

	 then

	 all other instructions, oldest instructions first

	 this policy works well because branches pass through the machine quicker
	 which works to reduce branch misprediction latencies, and very long latency
	 instructions (such loads and multiplies) get priority since they are very
	 likely on the program's critical path */
				static void
readyq_enqueue(struct RUU_station *rs, int context_id)		/* RS to enqueue */
{
				struct RS_link *prev, *node, *new_node;

				/* node is now queued */
				if (rs->queued)
								panic("node is already queued");
				rs->queued = TRUE;

				/* get a free ready list node */
				RSLINK_NEW(new_node, rs,contexts[context_id].rslink_free_list);
				new_node->x.seq = rs->seq;

				/* sudiptac: for Chronos, change the scheduling policy to oldest first */
#if 0
				/* locate insertion point */
				if (rs->in_LSQ || MD_OP_FLAGS(rs->op) & (F_LONGLAT|F_CTRL))
				{
								/* insert loads/stores and long latency ops at the head of the queue */
								prev = NULL;
								node = contexts[context_id].ready_queue;
				}
				else
#endif
				{
								/* otherwise insert in program order (earliest seq first) */
								for (prev=NULL, node=contexts[context_id].ready_queue;
																node && node->x.seq < rs->seq;
																prev=node, node=node->next);
				}

				if (prev)
				{
								/* insert middle or end */
								new_node->next = prev->next;
								prev->next = new_node;
				}
				else
				{
								/* insert at beginning */
								new_node->next = contexts[context_id].ready_queue;
								contexts[context_id].ready_queue = new_node;
				}
}

#if 0
/*
 * the create vector maps a logical register to a creator in the RUU (and
 * specific output operand) or the architected register file (if RS_link
 * is NULL)
 */

/* an entry in the create vector */
struct CV_link {
				struct RUU_station *rs;               /* creator's reservation station */
				int odep_num;                         /* specific output operand */
};
#endif
/* a NULL create vector entry */
static struct CV_link CVLINK_NULL = { NULL, 0 };

/* get a new create vector link */
#define CVLINK_INIT(CV, RS,ONUM)	((CV).rs = (RS), (CV).odep_num = (ONUM))

/* size of the create vector (one entry per architected register) */
#define CV_BMAP_SZ              (BITMAP_SIZE(MD_TOTAL_REGS))

/* the create vector, NOTE: speculative copy on write storage provided
	 for fast recovery during wrong path execute (see tracer_recover() for
	 details on this process */
//static BITMAP_TYPE(MD_TOTAL_REGS, use_spec_cv);
//static struct CV_link create_vector[MD_TOTAL_REGS];
//static struct CV_link spec_create_vector[MD_TOTAL_REGS];

/* these arrays shadow the create vector an indicate when a register was
	 last created */
//static tick_t create_vector_rt[MD_TOTAL_REGS];
//static tick_t spec_create_vector_rt[MD_TOTAL_REGS];

/* get a new create vector link */
#define CVLINK_INIT(CV, RS,ONUM)	((CV).rs = (RS), (CV).odep_num = (ONUM))

/* size of the create vector (one entry per architected register) */
#define CV_BMAP_SZ              (BITMAP_SIZE(MD_TOTAL_REGS))

/* read a create vector entry */
#define CREATE_VECTOR(N,context_id)        (BITMAP_SET_P(contexts[context_id].use_spec_cv, CV_BMAP_SZ, (N))\
								? contexts[context_id].spec_create_vector[N]                \
								: contexts[context_id].create_vector[N])

/* read a create vector timestamp entry */
#define CREATE_VECTOR_RT(N,context_id)     (BITMAP_SET_P(contexts[context_id].use_spec_cv, CV_BMAP_SZ, (N))\
								? contexts[context_id].spec_create_vector_rt[N]             \
								: contexts[context_id].create_vector_rt[N])

/* set a create vector entry */
#define SET_CREATE_VECTOR(N, L, context_id) (contexts[context_id].spec_mode                              \
								? (BITMAP_SET(contexts[context_id].use_spec_cv, CV_BMAP_SZ, (N)),\
												contexts[context_id].spec_create_vector[N] = (L))        \
								: (contexts[context_id].create_vector[N] = (L)))

//* initialize the create vector */
				static void
cv_init(int context_id)
{
				int i;

				/* initially all registers are valid in the architected register file,
					 i.e., the create vector entry is CVLINK_NULL */
				for (i=0; i < MD_TOTAL_REGS; i++)
				{
								contexts[context_id].create_vector[i] = CVLINK_NULL;
								contexts[context_id].create_vector_rt[i] = 0;
								contexts[context_id].spec_create_vector[i] = CVLINK_NULL;
								contexts[context_id].spec_create_vector_rt[i] = 0;
				}

				/* all create vector entries are non-speculative */
				BITMAP_CLEAR_MAP(contexts[context_id].use_spec_cv, CV_BMAP_SZ);
}
#if 0
/* dump the contents of the create vector */
				static void
cv_dump(FILE *stream,int context_id)				/* output stream */
{
				int i;
				struct CV_link ent;

				if (!stream)
								stream = stderr;

				fprintf(stream, "** create vector state **\n");

				for (i=0; i < MD_TOTAL_REGS; i++)
				{
								ent = CREATE_VECTOR(i,context_id);
								if (!ent.rs)
												fprintf(stream, "[cv%02d]: from architected reg file\n", i);
								else
												fprintf(stream, "[cv%02d]: from %s, idx: %d\n",
																				i, (ent.rs->in_LSQ ? "LSQ" : "RUU"),
																				(int)(ent.rs - (ent.rs->in_LSQ ? contexts[context_id].LSQ : contexts[context_id].RUU)));
				}
}
#endif

/*
 *  RUU_COMMIT() - instruction retirement pipeline stage
 */

/* this function commits the results of the oldest completed entries from the
	 RUU and LSQ to the architected reg file, stores in the LSQ will commit
	 their store data to the data cache at this point as well */
				static void
ruu_commit(int context_id)
{
				int i, lat, events, committed = 0;
				static counter_t sim_ret_insn = 0;

				//effect_commit = 0; 

				/* all values must be retired to the architected reg file in program order */
				while (contexts[context_id].RUU_num > 0 && committed < contexts[context_id].ruu_commit_width)
				{
								struct RUU_station *rs = &(contexts[context_id].RUU[contexts[context_id].RUU_head]);

								if (!rs->completed)
								{
												break;
								}

								/* default commit events */
								events = 0;

								/* load/stores must retire load/store queue entry as well */
								if (contexts[context_id].RUU[contexts[context_id].RUU_head].ea_comp)
								{
												/* load/store, retire head of LSQ as well */
												if (contexts[context_id].LSQ_num <= 0 || !contexts[context_id].LSQ[contexts[context_id].LSQ_head].in_LSQ)
																panic("RUU out of sync with LSQ");

												/* load/store operation must be complete */
												if (!(contexts[context_id].LSQ[contexts[context_id].LSQ_head].completed))
												{
																/* load/store operation is not yet complete */
																break;
												}

												if ((MD_OP_FLAGS(contexts[context_id].LSQ[contexts[context_id].LSQ_head].op) & (F_MEM|F_STORE))
																				== (F_MEM|F_STORE))
												{
																struct res_template *fu;


																/* stores must retire their store value to the cache at commit,
																	 try to get a store port (functional unit allocation) */
																fu = res_get(contexts[context_id].fu_pool, MD_OP_FUCLASS(contexts[context_id].LSQ[contexts[context_id].LSQ_head].op));

																if (fu)
																{
																				/* reserve the functional unit */
																				if (fu->master->busy)
																								panic("functional unit already in use");

																				/* schedule functional unit release event */
																				fu->master->busy = fu->issuelat;

																				/* go to the data cache */
																				if (contexts[context_id].cache_dl1)
																				{
																								/* commit store value to D-cache */
																								lat =
																												cache_access(contexts[context_id].cache_dl1, Write, 
																																				(contexts[context_id].LSQ[contexts[context_id].LSQ_head].addr&~3),context_id
																																				,NULL, 4, sim_cycle, NULL, NULL);
																								if (lat > contexts[context_id].cache_dl1_lat)
																												events |= PEV_CACHEMISS;
																				}

																				/* all loads and stores must to access D-TLB */
																				if (contexts[context_id].dtlb)
																				{
																								/* access the D-TLB */
																								lat =
																												cache_access(contexts[context_id].dtlb, Read, 
																																				(contexts[context_id].LSQ[contexts[context_id].LSQ_head].addr & ~3),
																																				context_id,NULL, 4, sim_cycle, NULL, NULL);
																								if (lat > 1)
																												events |= PEV_TLBMISS;
																				}
																}
																else
																{
																				/* no store ports left, cannot continue to commit insts */
																				break;
																}
												}

												/* invalidate load/store operation instance */
												contexts[context_id].LSQ[contexts[context_id].LSQ_head].tag++;
												contexts[context_id].sim_slip += (sim_cycle - contexts[context_id].LSQ[contexts[context_id].LSQ_head].slip);

												/* indicate to pipeline trace that this instruction retired */
												//ptrace_newstage(contexts[context_id].LSQ[contexts[context_id].LSQ_head].ptrace_seq, PST_COMMIT, events);
												//ptrace_endinst(contexts[context_id].LSQ[contexts[context_id].LSQ_head].ptrace_seq);

												/* commit head of LSQ as well */
												contexts[context_id].LSQ_head = (contexts[context_id].LSQ_head + 1) % contexts[context_id].LSQ_size;
												contexts[context_id].LSQ_num--;
								}

								if (contexts[context_id].pred
																&& contexts[context_id].bpred_spec_update == spec_CT
																&& (MD_OP_FLAGS(rs->op) & F_CTRL))
								{
												bpred_update(contexts[context_id].pred,
																				/* branch address */rs->PC,
																				/* actual target address */rs->next_PC,
																				/* taken? */rs->next_PC != (rs->PC +
																								sizeof(md_inst_t)),
																				/* pred taken? */rs->pred_PC != (rs->PC +
																								sizeof(md_inst_t)),
																				/* correct pred? */rs->pred_PC == rs->next_PC,
																				/* opcode */rs->op,
																				/* dir predictor update pointer */&rs->dir_update, 
																				/* current core */ context_id);
								}

								/* invalidate RUU operation instance */
								contexts[context_id].RUU[contexts[context_id].RUU_head].tag++;
								//md_print_insn(contexts[context_id].RUU[contexts[context_id].RUU_head].IR, 
								//contexts[context_id].RUU[contexts[context_id].RUU_head].PC, stderr);
								contexts[context_id].sim_slip += (sim_cycle - contexts[context_id].RUU[contexts[context_id].RUU_head].slip);
								/* print retirement trace if in verbose mode */
								if (verbose)
								{	
												sim_ret_insn++;
								}

								/* indicate to pipeline trace that this instruction retired */
								//ptrace_newstage(contexts[context_id].RUU[contexts[context_id].RUU_head].ptrace_seq, PST_COMMIT, events);
								//ptrace_endinst(  contexts[context_id].RUU[contexts[context_id].RUU_head].ptrace_seq);

								/* sudiptac :::: effective commit only if the instruction is in effective(user)
								 * code range */
								if((rs->PC >= contexts[context_id].mem->start_addr) && 
																(rs->PC < contexts[context_id].mem->end_addr))
								{		
												contexts[context_id].effect_commit = 1;
#ifdef _PRINT
												fprintf(stdout, "Context %d effective instruction committed\n", context_id);
#endif
												if (rs->icache_miss == TRUE)
																contexts[context_id].effect_icache_l1_miss++;

												/* sudiptac : handling effective cache misses in L2 caches (shared/non-shared) */
												#if 0
												#define CACHE_SET(cp, addr)	(((addr) >> (cp)->set_shift) & (cp)->set_mask)
												if(context_id == 1) {
																fprintf(stdout, "effective L2 cache missed at address %x, set %d\n", \
																								rs->PC, CACHE_SET(contexts[0].cache_il2, rs->PC));
												}
												#endif
												if (contexts[context_id].cache_il2 && rs->icache_miss_l2 == TRUE) {
												#if 0
																if(context_id == 0) {
																				fprintf(stdout, "L2 cache missed at address %x, set %d\n", \
																								rs->PC, CACHE_SET(contexts[0].cache_il2, rs->PC));
																}
												#endif
																contexts[context_id].effect_icache_l2_miss++;
												}

								}	
								else
												contexts[context_id].effect_commit = 0;

								/* commit head entry of RUU */
								contexts[context_id].RUU_head = (contexts[context_id].RUU_head + 1) % contexts[context_id].RUU_size;
								contexts[context_id].RUU_num--;

								/* one more instruction committed to architected state */
								committed++;

								for (i=0; i<MAX_ODEPS; i++)
								{
												if (rs->odep_list[i])
																panic ("retired instruction has odeps\n");
								}
				}
}


/*
 *  RUU_RECOVER() - squash mispredicted microarchitecture state
 */

/* recover processor microarchitecture state back to point of the
	 mis-predicted branch at RUU[BRANCH_INDEX] */
				static void
ruu_recover(int branch_index,int context_id )			/* index of mis-pred branch */
{
				int i, RUU_index = contexts[context_id].RUU_tail, LSQ_index = contexts[context_id].LSQ_tail;
				int RUU_prev_tail = contexts[context_id].RUU_tail, LSQ_prev_tail = contexts[context_id].LSQ_tail;

				/* recover from the tail of the RUU towards the head until the branch index
					 is reached, this direction ensures that the LSQ can be synchronized with
					 the RUU */

				/* go to first element to squash */
				RUU_index = (RUU_index + (contexts[context_id].RUU_size-1)) % contexts[context_id].RUU_size;
				LSQ_index = (LSQ_index + (contexts[context_id].LSQ_size-1)) % contexts[context_id].LSQ_size;

				/* traverse to older insts until the mispredicted branch is encountered */
				while (RUU_index != branch_index)
				{
								/* the RUU should not drain since the mispredicted branch will remain */
								if (!contexts[context_id].RUU_num)
												panic("empty RUU");

								/* should meet up with the tail first */
								if (RUU_index == contexts[context_id].RUU_head)
												panic("RUU head and tail broken");

								/* is this operation an effective addr calc for a load or store? */
								if (contexts[context_id].RUU[RUU_index].ea_comp)
								{
												/* should be at least one load or store in the LSQ */
												if (!contexts[context_id].LSQ_num)
																panic("RUU and LSQ out of sync");

												/* recover any resources consumed by the load or store operation */
												for (i=0; i<MAX_ODEPS; i++)
												{
																RSLINK_FREE_LIST(contexts[context_id].LSQ[LSQ_index].odep_list[i],contexts[context_id].rslink_free_list);
																/* blow away the consuming op list */
																contexts[context_id].LSQ[LSQ_index].odep_list[i] = NULL;
												}

												/* squash this LSQ entry */
												contexts[context_id].LSQ[LSQ_index].tag++;

												/* indicate in pipetrace that this instruction was squashed */
												//ptrace_endinst( contexts[context_id].LSQ[LSQ_index].ptrace_seq);

												/* go to next earlier LSQ slot */
												LSQ_prev_tail = LSQ_index;
												LSQ_index = (LSQ_index + (contexts[context_id].LSQ_size-1)) % contexts[context_id].LSQ_size;
												contexts[context_id].LSQ_num--;
								}

								/* recover any resources used by this RUU operation */
								for (i=0; i<MAX_ODEPS; i++)
								{
												RSLINK_FREE_LIST(contexts[context_id].RUU[RUU_index].odep_list[i],contexts[context_id].rslink_free_list);
												/* blow away the consuming op list */
												contexts[context_id].RUU[RUU_index].odep_list[i] = NULL;
								}

								/* squash this RUU entry */
								contexts[context_id].RUU[RUU_index].tag++;

								/* indicate in pipetrace that this instruction was squashed */
								//ptrace_endinst(contexts[context_id].RUU[RUU_index].ptrace_seq);

								/* go to next earlier slot in the RUU */
								RUU_prev_tail = RUU_index;
								RUU_index = (RUU_index + (contexts[context_id].RUU_size-1)) % contexts[context_id].RUU_size;
								contexts[context_id].RUU_num--;
				}

				/* reset head/tail pointers to point to the mis-predicted branch */
				contexts[context_id].RUU_tail = RUU_prev_tail;
				contexts[context_id].LSQ_tail = LSQ_prev_tail;

				/* revert create vector back to last precise create vector state, NOTE:
					 this is accomplished by resetting all the copied-on-write bits in the
					 USE_SPEC_CV bit vector */
				BITMAP_CLEAR_MAP(contexts[context_id].use_spec_cv, CV_BMAP_SZ);

				/* FIXME: could reset functional units at squash time */
}


/*
 *  RUU_WRITEBACK() - instruction result writeback pipeline stage
 */

/* forward declarations */
static void tracer_recover(int context_id);
/* recover instruction trace generator state to precise state state immediately
	 before the first mis-predicted branch; this is accomplished by resetting
	 all register value copied-on-write bitmasks are reset, and the speculative
	 memory hash table is cleared */
				static void
tracer_recover(int context_id)
{
				int i;
				struct spec_mem_ent *ent, *ent_next;

				/* better be in mis-speculative trace generation mode */
				if (!contexts[current_context].spec_mode)
								panic("cannot recover unless in speculative mode");

				/* reset to non-speculative trace generation mode */
				contexts[current_context].spec_mode = FALSE;

				/* reset copied-on-write register bitmasks back to non-speculative state */
				BITMAP_CLEAR_MAP(contexts[current_context].use_spec_R, R_BMAP_SZ);
				BITMAP_CLEAR_MAP(contexts[current_context].use_spec_F, F_BMAP_SZ);
				BITMAP_CLEAR_MAP(contexts[current_context].use_spec_C, C_BMAP_SZ);

				/* reset memory state back to non-speculative state */
				/* FIXME: could version stamps be used here?!?!? */
				for (i=0; i<STORE_HASH_SIZE; i++)
				{
								/* release all hash table buckets */
								for (ent=contexts[current_context].store_htable[i]; ent; ent=ent_next)
								{
												ent_next = ent->next;
												ent->next = contexts[current_context].bucket_free_list;
												contexts[current_context].bucket_free_list = ent;
								}
								contexts[current_context].store_htable[i] = NULL;
				}

				/* if pipetracing, indicate squash of instructions in the inst fetch queue */
				if (ptrace_active)
				{
								while (contexts[current_context].fetch_num != 0)
								{
												/* squash the next instruction from the IFETCH -> DISPATCH queue */
												//ptrace_endinst(contexts[current_context].fetch_data[contexts[current_context].fetch_head].ptrace_seq);

												/* consume instruction from IFETCH -> DISPATCH queue */
												contexts[current_context].fetch_head = (contexts[current_context].fetch_head+1) & (contexts[current_context].ruu_ifq_size - 1);
												contexts[current_context].fetch_num--;
								}
				}

				/* reset IFETCH state */
				contexts[current_context].fetch_num = 0;
				contexts[current_context].fetch_tail = contexts[current_context].fetch_head = 0;
				contexts[current_context].fetch_pred_PC = contexts[current_context].fetch_regs_PC = contexts[current_context].recover_PC;
}
/* writeback completed operation results from the functional units to RUU,
	 at this point, the output dependency chains of completing instructions
	 are also walked to determine if any dependent instruction now has all
	 of its register operands, if so the (nearly) ready instruction is inserted
	 into the ready instruction queue */
				static void
ruu_writeback(int context_id)
{
				int i;
				struct RUU_station *rs;

				/* service all completed events */
				while ((rs = eventq_next_event(context_id)))
				{
								/* RS has completed execution and (possibly) produced a result */
								if (!OPERANDS_READY(rs) || rs->queued || !rs->issued || rs->completed)
												panic("inst completed and !ready, !issued, or completed");

								/* operation has completed */
								rs->completed = TRUE;

								/* does this operation reveal a mis-predicted branch? */
								if (rs->recover_inst)
								{
												if (rs->in_LSQ)
																panic("mis-predicted load or store?!?!?");

												/* recover processor state and reinit fetch to correct path */
												ruu_recover(rs - (contexts[context_id].RUU), context_id);
												tracer_recover(context_id);
												bpred_recover(contexts[context_id].pred, rs->PC, rs->stack_recover_idx);

												/* stall fetch until I-fetch and I-decode recover */
												contexts[context_id].ruu_fetch_issue_delay = contexts[context_id].ruu_branch_penalty;

												/* continue writeback of the branch/control instruction */
								}

								/* if we speculatively update branch-predictor, do it here */
								if (contexts[context_id].pred
																&& contexts[context_id].bpred_spec_update == spec_WB
																&& !rs->in_LSQ
																&& (MD_OP_FLAGS(rs->op) & F_CTRL))
								{
												bpred_update(contexts[context_id].pred,
																				/* branch address */rs->PC,
																				/* actual target address */rs->next_PC,
																				/* taken? */rs->next_PC != (rs->PC +
																								sizeof(md_inst_t)),
																				/* pred taken? */rs->pred_PC != (rs->PC +
																								sizeof(md_inst_t)),
																				/* correct pred? */rs->pred_PC == rs->next_PC,
																				/* opcode */rs->op,
																				/* dir predictor update pointer */&rs->dir_update, 
																				/* current core */ context_id);
								}

								/* entered writeback stage, indicate in pipe trace */
								//ptrace_newstage(rs->ptrace_seq, PST_WRITEBACK,
								//	      rs->recover_inst ? PEV_MPDETECT : 0);

								/* broadcast results to consuming operations, this is more efficiently
									 accomplished by walking the output dependency chains of the
									 completed instruction */
								for (i=0; i<MAX_ODEPS; i++)
								{
												if (rs->onames[i] != NA)
												{
																struct CV_link link;
																struct RS_link *olink, *olink_next;

																if (rs->spec_mode)
																{
																				/* update the speculative create vector, future operations
																					 get value from later creator or architected reg file */
																				link = contexts[context_id].spec_create_vector[rs->onames[i]];
																				if (/* !NULL */link.rs
																												&& /* refs RS */(link.rs == rs && link.odep_num == i))
																				{
																								/* the result can now be read from a physical register,
																									 indicate this as so */
																								contexts[context_id].spec_create_vector[rs->onames[i]] = CVLINK_NULL;
																								contexts[context_id].spec_create_vector_rt[rs->onames[i]] = sim_cycle;
																				}
																				/* else, creator invalidated or there is another creator */
																}
																else
																{
																				/* update the non-speculative create vector, future
																					 operations get value from later creator or architected
																					 reg file */
																				link = contexts[context_id].create_vector[rs->onames[i]];
																				if (/* !NULL */link.rs
																												&& /* refs RS */(link.rs == rs && link.odep_num == i))
																				{
																								/* the result can now be read from a physical register,
																									 indicate this as so */
																								contexts[context_id].create_vector[rs->onames[i]] = CVLINK_NULL;
																								contexts[context_id].create_vector_rt[rs->onames[i]] = sim_cycle;
																				}
																				/* else, creator invalidated or there is another creator */
																}

																/* walk output list, queue up ready operations */
																for (olink=rs->odep_list[i]; olink; olink=olink_next)
																{
																				if (RSLINK_VALID(olink))
																				{
																								if (olink->rs->idep_ready[olink->x.opnum])
																												panic("output dependence already satisfied");

																								/* input is now ready */
																								olink->rs->idep_ready[olink->x.opnum] = TRUE;

																								/* are all the register operands of target ready? */
																								if (OPERANDS_READY(olink->rs))
																								{
																												/* yes! enqueue instruction as ready, NOTE: stores
																													 complete at dispatch, so no need to enqueue
																													 them */
																												if (!olink->rs->in_LSQ
																																				|| ((MD_OP_FLAGS(olink->rs->op)&(F_MEM|F_STORE))
																																								== (F_MEM|F_STORE)))
																																readyq_enqueue(olink->rs, context_id);
																												/* else, ld op, issued when no mem conflict */
																								}
																				}

																				/* grab link to next element prior to free */
																				olink_next = olink->next;

																				/* free dependence link element */
																				RSLINK_FREE(olink, contexts[context_id].rslink_free_list);
																}
																/* blow away the consuming op list */
																rs->odep_list[i] = NULL;

												} /* if not NA output */

								} /* for all outputs */

				} /* for all writeback events */

}


/*
 *  LSQ_REFRESH() - memory access dependence checker/scheduler
 */

/* this function locates ready instructions whose memory dependencies have
	 been satisfied, this is accomplished by walking the LSQ for loads, looking
	 for blocking memory dependency condition (e.g., earlier store with an
	 unknown address) */
#define MAX_STD_UNKNOWNS		64
				static void
lsq_refresh(int context_id)
{
				int i, j, index, n_std_unknowns;
				md_addr_t std_unknowns[MAX_STD_UNKNOWNS];

				/* scan entire queue for ready loads: scan from oldest instruction
					 (head) until we reach the tail or an unresolved store, after which no
					 other instruction will become ready */
				for (i=0, index=contexts[context_id].LSQ_head, n_std_unknowns=0;
												i < contexts[context_id].LSQ_num;
												i++, index=(index + 1) % contexts[context_id].LSQ_size)
				{
								/* terminate search for ready loads after first unresolved store,
									 as no later load could be resolved in its presence */
								if (/* store? */
																(MD_OP_FLAGS(contexts[context_id].LSQ[index].op) & (F_MEM|F_STORE)) == (F_MEM|F_STORE))
								{
												if (!STORE_ADDR_READY(&(contexts[context_id].LSQ[index])))
												{
																/* FIXME: a later STD + STD known could hide the STA unknown */
																/* sta unknown, blocks all later loads, stop search */
																break;
												}
												else if (!OPERANDS_READY(&(contexts[context_id].LSQ[index])))
												{
																/* sta known, but std unknown, may block a later store, record
																	 this address for later referral, we use an array here because
																	 for most simulations the number of entries to search will be
																	 very small */
																if (n_std_unknowns == MAX_STD_UNKNOWNS)
																				fatal("STD unknown array overflow, increase MAX_STD_UNKNOWNS");
																std_unknowns[n_std_unknowns++] = contexts[context_id].LSQ[index].addr;
												}
												else /* STORE_ADDR_READY() && OPERANDS_READY() */
												{
																/* a later STD known hides an earlier STD unknown */
																for (j=0; j<n_std_unknowns; j++)
																{
																				if (std_unknowns[j] == /* STA/STD known */contexts[context_id].LSQ[index].addr)
																								std_unknowns[j] = /* bogus addr */0;
																}
												}
								}

								if (/* load? */
																((MD_OP_FLAGS(contexts[context_id].LSQ[index].op) & (F_MEM|F_LOAD)) == (F_MEM|F_LOAD))
																&& /* queued? */!contexts[context_id].LSQ[index].queued
																&& /* waiting? */!contexts[context_id].LSQ[index].issued
																&& /* completed? */!contexts[context_id].LSQ[index].completed
																&& /* regs ready? */OPERANDS_READY(&(contexts[context_id].LSQ[index])))
								{
												/* no STA unknown conflict (because we got to this check), check for
													 a STD unknown conflict */
												for (j=0; j<n_std_unknowns; j++)
												{
																/* found a relevant STD unknown? */
																if (std_unknowns[j] == contexts[context_id].LSQ[index].addr)
																				break;
												}
												if (j == n_std_unknowns)
												{
																/* no STA or STD unknown conflicts, put load on ready queue */
																readyq_enqueue(&(contexts[context_id].LSQ[index]), context_id);
												}
								}
				}
}

/*
 *  RUU_ISSUE() - issue instructions to functional units
 */

/*
 *  RUU_ISSUE() - issue instructions to functional units
 */

/* attempt to issue all operations in the ready queue; insts in the ready
	 instruction queue have all register dependencies satisfied, this function
	 must then 1) ensure the instructions memory dependencies have been satisfied
	 (see lsq_refresh() for details on this process) and 2) a function unit
	 is available in this cycle to commence execution of the operation; if all
	 goes well, the function unit is allocated, a writeback event is scheduled,
	 and the instruction begins execution */
				static void
ruu_issue(int context_id)
{
				int i, n_issued;
				unsigned int load_lat, tlb_lat;
				struct RS_link *node, *next_node;
				struct res_template *fu;
				struct mem_t* mem = contexts[context_id].mem;


				/* FIXME: could be a little more efficient when scanning the ready queue */

				/* copy and then blow away the ready list, NOTE: the ready list is
					 always totally reclaimed each cycle, and instructions that are not
					 issue are explicitly reinserted into the ready instruction queue,
					 this management strategy ensures that the ready instruction queue
					 is always properly sorted */
				node = contexts[context_id].ready_queue;
				contexts[context_id].ready_queue = NULL;

				/* visit all ready instructions (i.e., insts whose register input
					 dependencies have been satisfied, stop issue when no more instructions
					 are available or issue bandwidth is exhausted */
				for (n_issued=0;
												node && n_issued < contexts[context_id].ruu_issue_width;
												node = next_node)
				{
								next_node = node->next;

								/* still valid? */
								if (RSLINK_VALID(node))
								{
												struct RUU_station *rs = RSLINK_RS(node);

												/* issue operation, both reg and mem deps have been satisfied */
												if (!OPERANDS_READY(rs) || !rs->queued
																				|| rs->issued || rs->completed)
																panic("issued inst !ready, issued, or completed");

												/* node is now un-queued */
												rs->queued = FALSE;

												if (rs->in_LSQ
																				&& ((MD_OP_FLAGS(rs->op) & (F_MEM|F_STORE)) == (F_MEM|F_STORE)))
												{
																/* stores complete in effectively zero time, result is
																	 written into the load/store queue, the actual store into
																	 the memory system occurs when the instruction is retired
																	 (see ruu_commit()) */
																rs->issued = TRUE;
																rs->completed = TRUE;
																if (rs->onames[0] || rs->onames[1])
																				panic("store creates result");

																if (rs->recover_inst)
																				panic("mis-predicted store");

																/* entered execute stage, indicate in pipe trace */
																//ptrace_newstage(rs->ptrace_seq, PST_WRITEBACK, 0);

																/* one more inst issued */
																n_issued++;
												}
												else
												{
																/* issue the instruction to a functional unit */
																if (MD_OP_FUCLASS(rs->op) != NA)
																{
																				fu = res_get(contexts[context_id].fu_pool, MD_OP_FUCLASS(rs->op));
																				if (fu)
																				{
																								/* got one! issue inst to functional unit */
																								rs->issued = TRUE;
																								/* reserve the functional unit */
																								if (fu->master->busy)
																												panic("functional unit already in use");

																								/* schedule functional unit release event */
																								fu->master->busy = fu->issuelat;

																								/* schedule a result writeback event */
																								if (rs->in_LSQ
																																&& ((MD_OP_FLAGS(rs->op) & (F_MEM|F_LOAD))
																																				== (F_MEM|F_LOAD)))
																								{
																												int events = 0;

																												/* for loads, determine cache access latency:
																													 first scan LSQ to see if a store forward is
																													 possible, if not, access the data cache */
																												load_lat = 0;
																												i = (rs - contexts[context_id].LSQ);
																												if (i != contexts[context_id].LSQ_head)
																												{
																																for (;;)
																																{
																																				/* go to next earlier LSQ entry */
																																				i = (i + (contexts[context_id].LSQ_size-1)) % contexts[context_id].LSQ_size;

																																				/* FIXME: not dealing with partials! */
																																				if ((MD_OP_FLAGS(contexts[context_id].LSQ[i].op) & F_STORE)
																																												&& (contexts[context_id].LSQ[i].addr == rs->addr))
																																				{
																																								/* hit in the LSQ */
																																								load_lat = 1;
																																								break;
																																				}

																																				/* scan finished? */
																																				if (i == contexts[context_id].LSQ_head)
																																								break;
																																}
																												}

																												/* was the value store forwared from the LSQ? */
																												if (!load_lat)
																												{
																																int valid_addr = MD_VALID_ADDR(rs->addr);

																																if (!contexts[context_id].spec_mode && !valid_addr)
																																				contexts[context_id].sim_invalid_addrs++;

																																/* no! go to the data cache if addr is valid */
																																if (contexts[context_id].cache_dl1 && valid_addr)
																																{
																																				/* access the cache if non-faulting */
																																				load_lat =
																																								cache_access(contexts[context_id].cache_dl1, Read,
																																																(rs->addr & ~3),context_id, NULL, 4,
																																																sim_cycle, NULL, NULL);
																																				if (load_lat > contexts[context_id].cache_dl1_lat)
																																								events |= PEV_CACHEMISS;
																																}
																																else
																																{
																																				/* no caches defined, just use op latency */
																																				load_lat = fu->oplat;
																																}
																												}

																												/* all loads and stores must to access D-TLB */
																												if (contexts[context_id].dtlb && MD_VALID_ADDR(rs->addr))
																												{
																																/* access the D-DLB, NOTE: this code will
																																	 initiate speculative TLB misses */
																																tlb_lat =
																																				cache_access(contexts[context_id].dtlb, Read, (rs->addr & ~3),
																																												context_id,NULL, 4, sim_cycle, NULL, NULL);
																																if (tlb_lat > 1)
																																				events |= PEV_TLBMISS;

																																/* D-cache/D-TLB accesses occur in parallel */
																																load_lat = MAX(tlb_lat, load_lat);
																												}

																												/* use computed cache access latency */
																												eventq_queue_event(rs, sim_cycle + load_lat, context_id);

																												/* entered execute stage, indicate in pipe trace */
																												//ptrace_newstage(rs->ptrace_seq, PST_EXECUTE,
																												//		  ((rs->ea_comp ? PEV_AGEN : 0)
																												//		   | events));
																								}
																								else /* !load && !store */
																								{
																												/* use deterministic functional unit latency */
																												eventq_queue_event(rs, sim_cycle + fu->oplat, context_id);

																												/* entered execute stage, indicate in pipe trace */
																												//ptrace_newstage(rs->ptrace_seq, PST_EXECUTE, 
																												//		  rs->ea_comp ? PEV_AGEN : 0);
																								}

																								/* one more inst issued */
																								n_issued++;
																				}
																				else /* no functional unit */
																				{
																								/* insufficient functional unit resources, put operation
																									 back onto the ready list, we'll try to issue it
																									 again next cycle */
																								readyq_enqueue(rs, context_id);
																				}
																}
																else /* does not require a functional unit! */
																{
																				/* FIXME: need better solution for these */
																				/* the instruction does not need a functional unit */
																				rs->issued = TRUE;

																				/* schedule a result event */
																				eventq_queue_event(rs, sim_cycle + 1, context_id);

																				/* entered execute stage, indicate in pipe trace */
																				//ptrace_newstage(rs->ptrace_seq, PST_EXECUTE,
																				//		  rs->ea_comp ? PEV_AGEN : 0);

																				/* one more inst issued */
																				n_issued++;
																}
												} /* !store */

								}
								/* else, RUU entry was squashed */

								/* reclaim ready list entry, NOTE: this is done whether or not the
									 instruction issued, since the instruction was once again reinserted
									 into the ready queue if it did not issue, this ensures that the ready
									 queue is always properly sorted */
								RSLINK_FREE(node,contexts[context_id].rslink_free_list);
				}

				/* put any instruction not issued back into the ready queue, go through
					 normal channels to ensure instruction stay ordered correctly */
				for (; node; node = next_node)
				{
								next_node = node->next;

								/* still valid? */
								if (RSLINK_VALID(node))
								{
												struct RUU_station *rs = RSLINK_RS(node);

												/* node is now un-queued */
												rs->queued = FALSE;

												/* not issued, put operation back onto the ready list, we'll try to
													 issue it again next cycle */
												readyq_enqueue(rs, context_id);
								}
								/* else, RUU entry was squashed */

								/* reclaim ready list entry, NOTE: this is done whether or not the
									 instruction issued, since the instruction was once again reinserted
									 into the ready queue if it did not issue, this ensures that the ready
									 queue is always properly sorted */
								RSLINK_FREE(node,contexts[context_id].rslink_free_list);
				}
}


/*
 * routines for generating on-the-fly instruction traces with support
 * for control and data misspeculation modeling
 */

/* integer register file */
#define R_BMAP_SZ       (BITMAP_SIZE(MD_NUM_IREGS))
//static BITMAP_TYPE(MD_NUM_IREGS, use_spec_R);
//static md_gpr_t spec_regs_R;

/* floating point register file */
#define F_BMAP_SZ       (BITMAP_SIZE(MD_NUM_FREGS))
//static BITMAP_TYPE(MD_NUM_FREGS, use_spec_F);
//static md_fpr_t spec_regs_F;

/* miscellaneous registers */
#define C_BMAP_SZ       (BITMAP_SIZE(MD_NUM_CREGS))
//static BITMAP_TYPE(MD_NUM_FREGS, use_spec_C);
//static md_ctrl_t spec_regs_C;
#if 0
/* dump speculative register state */
				static void
rspec_dump(FILE *stream, int context_id)			/* output stream */
{
				int i;

				if (!stream)
								stream = stderr;

				fprintf(stream, "** speculative register contents **\n");

				fprintf(stream, "spec_mode: %s\n", contexts[context_id].spec_mode ? "t" : "f");

				/* dump speculative integer regs */
				for (i=0; i < MD_NUM_IREGS; i++)
				{
								if (BITMAP_SET_P(contexts[context_id].use_spec_R, R_BMAP_SZ, i))
								{
												md_print_ireg(contexts[context_id].spec_regs_R, i, stream);
												fprintf(stream, "\n");
								}
				}

				/* dump speculative FP regs */
				for (i=0; i < MD_NUM_FREGS; i++)
				{
								if (BITMAP_SET_P(contexts[context_id].use_spec_F, F_BMAP_SZ, i))
								{
												md_print_fpreg(contexts[context_id].spec_regs_F, i, stream);
												fprintf(stream, "\n");
								}
				}

				/* dump speculative CTRL regs */
				for (i=0; i < MD_NUM_CREGS; i++)
				{
								if (BITMAP_SET_P(contexts[context_id].use_spec_C, C_BMAP_SZ, i))
								{
												md_print_creg(contexts[context_id].spec_regs_C, i, stream);
												fprintf(stream, "\n");
								}
				}
}
#endif



/* speculative memory hash table size, NOTE: this must be a power-of-two */
#define STORE_HASH_SIZE		32
#if 0
/* speculative memory hash table definition, accesses go through this hash
	 table when accessing memory in speculative mode, the hash table flush the
	 table when recovering from mispredicted branches */
struct spec_mem_ent {
				struct spec_mem_ent *next;		/* ptr to next hash table bucket */
				md_addr_t addr;			/* virtual address of spec state */
				unsigned int data[2];			/* spec buffer, up to 8 bytes */
};
#endif
/* speculative memory hash table */
//static struct spec_mem_ent *store_htable[STORE_HASH_SIZE];

/* speculative memory hash table bucket free list */
//static struct spec_mem_ent *bucket_free_list = NULL;


/* program counter */
//static md_addr_t pred_PC;
//static md_addr_t recover_PC;

/* fetch unit next fetch address */
//static md_addr_t fetch_regs_PC;
//static md_addr_t fetch_pred_PC;
#if 0
/* IFETCH -> DISPATCH instruction queue definition */
struct fetch_rec {
				md_inst_t IR;				/* inst register */
				md_addr_t regs_PC, pred_PC;		/* current PC, predicted next PC */
				struct bpred_update_t dir_update;	/* bpred direction update info */
				int stack_recover_idx;		/* branch predictor RSB index */
				unsigned int ptrace_seq;		/* print trace sequence id */
};
#endif
//static struct fetch_rec *fetch_data;	/* IFETCH -> DISPATCH inst queue */
//static int fetch_num;			/* num entries in IF -> DIS queue */
//static int fetch_tail, fetch_head;	/* head and tail pointers of queue */

/* recover instruction trace generator state to precise state state immediately
	 before the first mis-predicted branch; this is accomplished by resetting
	 all register value copied-on-write bitmasks are reset, and the speculative
	 memory hash table is cleared */

/* initialize the speculative instruction state generator state */
				static void
tracer_init(int context_id)
{
				int i;

				/* initially in non-speculative mode */
				contexts[context_id].spec_mode = FALSE;

				/* register state is from non-speculative state buffers */
				BITMAP_CLEAR_MAP(contexts[context_id].use_spec_R, R_BMAP_SZ);
				BITMAP_CLEAR_MAP(contexts[context_id].use_spec_F, F_BMAP_SZ);
				BITMAP_CLEAR_MAP(contexts[context_id].use_spec_C, C_BMAP_SZ);

				/* memory state is from non-speculative memory pages */
				for (i=0; i<STORE_HASH_SIZE; i++)
								contexts[context_id].store_htable[i] = NULL;
}


/* speculative memory hash table address hash function */
#define HASH_ADDR(ADDR)							\
				((((ADDR) >> 24)^((ADDR) >> 16)^((ADDR) >> 8)^(ADDR)) & (STORE_HASH_SIZE-1))

/* this functional provides a layer of mis-speculated state over the
	 non-speculative memory state, when in mis-speculation trace generation mode,
	 the simulator will call this function to access memory, instead of the
	 non-speculative memory access interfaces defined in memory.h; when storage
	 is written, an entry is allocated in the speculative memory hash table,
	 future reads and writes while in mis-speculative trace generation mode will
	 access this buffer instead of non-speculative memory state; when the trace
	 generator transitions back to non-speculative trace generation mode,
	 tracer_recover() clears this table, returns any access fault */
				static enum md_fault_type
spec_mem_access(struct mem_t *mem,		/* memory space to access */
								enum mem_cmd cmd,		/* Read or Write access cmd */
								md_addr_t addr,			/* virtual address of access */
								void *p,			/* input/output buffer */
								int nbytes,			/* number of bytes to access */
								int context_id)
{
				int i, index;
				struct spec_mem_ent *ent, *prev;

				/* FIXME: partially overlapping writes are not combined... */
				/* FIXME: partially overlapping reads are not handled correctly... */

				/* check alignments, even speculative this test should always pass */
				if ((nbytes & (nbytes-1)) != 0 || (addr & (nbytes-1)) != 0)
				{
								/* no can do, return zero result */
								for (i=0; i < nbytes; i++)
												((char *)p)[i] = 0;

								return md_fault_none;
				}

				/* check permissions */
				if (!((addr >= (contexts[context_id].mem->ld_text_base ) && addr < ((contexts[context_id].mem->ld_text_base) +(contexts[context_id].mem->ld_text_size))
																				&& cmd == Read)
																|| MD_VALID_ADDR(addr)))
				{
								/* no can do, return zero result */
								for (i=0; i < nbytes; i++)
												((char *)p)[i] = 0;

								return md_fault_none;
				}

				/* has this memory state been copied on mis-speculative write? */
				index = HASH_ADDR(addr);
				for (prev=NULL,ent=contexts[context_id].store_htable[index]; ent; prev=ent,ent=ent->next)
				{
								if (ent->addr == addr)
								{
												/* reorder chains to speed access into hash table */
												if (prev != NULL)
												{
																/* not at head of list, relink the hash table entry at front */
																prev->next = ent->next;
																ent->next = contexts[context_id].store_htable[index];
																contexts[context_id].store_htable[index] = ent;
												}
												break;
								}
				}

				/* no, if it is a write, allocate a hash table entry to hold the data */
				if (!ent && cmd == Write)
				{
								/* try to get an entry from the free list, if available */
								if (!contexts[context_id].bucket_free_list)
								{
												/* otherwise, call calloc() to get the needed storage */
												contexts[context_id].bucket_free_list = calloc(1, sizeof(struct spec_mem_ent));
												if (!contexts[context_id].bucket_free_list)
																fatal("out of virtual memory");
								}
								ent = contexts[context_id].bucket_free_list;
								contexts[context_id].bucket_free_list = contexts[context_id].bucket_free_list->next;

								if (!bugcompat_mode)
								{
												/* insert into hash table */
												ent->next = contexts[context_id].store_htable[index];
												contexts[context_id].store_htable[index] = ent;
												ent->addr = addr;
												ent->data[0] = 0; ent->data[1] = 0;
								}
				}

				/* handle the read or write to speculative or non-speculative storage */
				switch (nbytes)
				{
								case 1:
												if (cmd == Read)
												{
																if (ent)
																{
																				/* read from mis-speculated state buffer */
																				*((byte_t *)p) = *((byte_t *)(&ent->data[0]));
																}
																else
																{
																				/* read from non-speculative memory state, don't allocate
																					 memory pages with speculative loads */
																				*((byte_t *)p) = MEM_READ_BYTE(contexts[context_id].mem, addr);
																}
												}
												else
												{
																/* always write into mis-speculated state buffer */
																*((byte_t *)(&ent->data[0])) = *((byte_t *)p);
												}
												break;
								case 2:
												if (cmd == Read)
												{
																if (ent)
																{
																				/* read from mis-speculated state buffer */
																				*((half_t *)p) = *((half_t *)(&ent->data[0]));
																}
																else
																{
																				/* read from non-speculative memory state, don't allocate
																					 memory pages with speculative loads */
																				*((half_t *)p) = MEM_READ_HALF(contexts[context_id].mem, addr);
																}
												}
												else
												{
																/* always write into mis-speculated state buffer */
																*((half_t *)&ent->data[0]) = *((half_t *)p);
												}
												break;
								case 4:
												if (cmd == Read)
												{
																if (ent)
																{
																				/* read from mis-speculated state buffer */
																				*((word_t *)p) = *((word_t *)&ent->data[0]);
																}
																else
																{
																				/* read from non-speculative memory state, don't allocate
																					 memory pages with speculative loads */
																				*((word_t *)p) = MEM_READ_WORD(contexts[context_id].mem, addr);
																}
												}
												else
												{
																/* always write into mis-speculated state buffer */
																*((word_t *)&ent->data[0]) = *((word_t *)p);
												}
												break;
								case 8:
												if (cmd == Read)
												{
																if (ent)
																{
																				/* read from mis-speculated state buffer */
																				*((word_t *)p) = *((word_t *)&ent->data[0]);
																				*(((word_t *)p)+1) = *((word_t *)&ent->data[1]);
																}
																else
																{
																				/* read from non-speculative memory state, don't allocate
																					 memory pages with speculative loads */
																				*((word_t *)p) = MEM_READ_WORD(contexts[context_id].mem, addr);
																				*(((word_t *)p)+1) =
																								MEM_READ_WORD(contexts[context_id].mem, addr + sizeof(word_t));
																}
												}
												else
												{
																/* always write into mis-speculated state buffer */
																*((word_t *)&ent->data[0]) = *((word_t *)p);
																*((word_t *)&ent->data[1]) = *(((word_t *)p)+1);
												}
												break;
								default:
												panic("access size not supported in mis-speculative mode");
				}

				return md_fault_none;
}
#if 0
/* dump speculative memory state */
				static void
mspec_dump(FILE *stream, int context_id)			/* output stream */
{
				int i;
				struct spec_mem_ent *ent;

				if (!stream)
								stream = stderr;

				fprintf(stream, "** speculative memory contents **\n");

				fprintf(stream, "spec_mode: %s\n", contexts[context_id].spec_mode ? "t" : "f");

				for (i=0; i<STORE_HASH_SIZE; i++)
				{
								/* dump contents of all hash table buckets */
								for (ent=contexts[context_id].store_htable[i]; ent; ent=ent->next)
								{
												myfprintf(stream, "[0x%08p]: %12.0f/0x%08x:%08x\n",
																				ent->addr, (double)(*((double *)ent->data)),
																				*((unsigned int *)&ent->data[0]),
																				*(((unsigned int *)&ent->data[0]) + 1));
								}
				}
}
#endif

#if 0
/* default memory state accessor, used by DLite */
				static char *					/* err str, NULL for no err */
simoo_mem_obj(struct mem_t *mem,		/* memory space to access */
								int is_write,			/* access type */
								md_addr_t addr,			/* address to access */
								char *p,				/* input/output buffer */
								int nbytes,			/* size of access */
								int context_id)
{
				enum mem_cmd cmd;

				if (!is_write)
								cmd = Read;
				else
								cmd = Write;

#if 0
				char *errstr;

				errstr = mem_valid(cmd, addr, nbytes, /* declare */FALSE);
				if (errstr)
								return errstr;
#endif

				/* else, no error, access memory */
				if (contexts[context_id].spec_mode)
								spec_mem_access(contexts[context_id].mem, cmd, addr, p, nbytes,context_id);
				else
								mem_access(contexts[context_id].mem, cmd, addr, p, nbytes);

				/* no error */
				return NULL;
}

#endif


/* link RS onto the output chain number of whichever operation will next
	 create the architected register value IDEP_NAME */
				static INLINE void
ruu_link_idep(struct RUU_station *rs,		/* rs station to link */
								int idep_num,			/* input dependence number */
								int idep_name,			/* input register name */
								int context_id )
{
				struct CV_link head;
				struct RS_link *link;

				/* any dependence? */
				if (idep_name == NA)
				{
								/* no input dependence for this input slot, mark operand as ready */
								rs->idep_ready[idep_num] = TRUE;
								return;
				}

				/* locate creator of operand */
				head = CREATE_VECTOR(idep_name,context_id);

				/* any creator? */
				if (!head.rs)
				{
								/* no active creator, use value available in architected reg file,
									 indicate the operand is ready for use */
								rs->idep_ready[idep_num] = TRUE;
								return;
				}
				/* else, creator operation will make this value sometime in the future */

				/* indicate value will be created sometime in the future, i.e., operand
					 is not yet ready for use */
				rs->idep_ready[idep_num] = FALSE;

				/* link onto creator's output list of dependant operand */
				RSLINK_NEW(link, rs,contexts[context_id].rslink_free_list); link->x.opnum = idep_num;
				link->next = head.rs->odep_list[head.odep_num];
				head.rs->odep_list[head.odep_num] = link;
}



/* make RS the creator of architected register ODEP_NAME */
				static INLINE void
ruu_install_odep(struct RUU_station *rs,	/* creating RUU station */
								int odep_num,			/* output operand number */
								int odep_name,			/* output register name */
								int context_id)
{
				struct CV_link cv;

				/* any dependence? */
				if (odep_name == NA)
				{
								/* no value created */
								rs->onames[odep_num] = NA;
								return;
				}
				/* else, create a RS_NULL terminated output chain in create vector */

				/* record output name, used to update create vector at completion */
				rs->onames[odep_num] = odep_name;

				/* initialize output chain to empty list */
				rs->odep_list[odep_num] = NULL;

				/* indicate this operation is latest creator of ODEP_NAME */
				CVLINK_INIT(cv, rs, odep_num);
				SET_CREATE_VECTOR(odep_name, cv,context_id);
}


/*
 * configure the instruction decode engine
 */

#define DNA			(0)

#if defined(TARGET_PISA)

/* general register dependence decoders */
#define DGPR(N)			(N)
#define DGPR_D(N)		((N) &~1)

/* floating point register dependence decoders */
#define DFPR_L(N)		(((N)+32)&~1)
#define DFPR_F(N)		(((N)+32)&~1)
#define DFPR_D(N)		(((N)+32)&~1)

/* miscellaneous register dependence decoders */
#define DHI			(0+32+32)
#define DLO			(1+32+32)
#define DFCC			(2+32+32)
#define DTMP			(3+32+32)

#elif defined(TARGET_ALPHA)

/* general register dependence decoders, $r31 maps to DNA (0) */
#define DGPR(N)			(31 - (N)) /* was: (((N) == 31) ? DNA : (N)) */

/* floating point register dependence decoders */
#define DFPR(N)			(((N) == 31) ? DNA : ((N)+32))

/* miscellaneous register dependence decoders */
#define DFPCR			(0+32+32)
#define DUNIQ			(1+32+32)
#define DTMP			(2+32+32)

#else
#error No ISA target defined...
#endif


/*
 * configure the execution engine
 */

/* next program counter */
#define SET_NPC(EXPR)           (contexts[context_id].regs.regs_NPC = (EXPR))

/* target program counter */
#undef  SET_TPC
#define SET_TPC(EXPR)		(target_PC = (EXPR))

/* current program counter */
#define CPC                     (contexts[context_id].regs.regs_PC)
#define SET_CPC(EXPR)           (contexts[context_id].regs.regs_PC = (EXPR))

/* general purpose register accessors, NOTE: speculative copy on write storage
	 provided for fast recovery during wrong path execute (see tracer_recover()
	 for details on this process */
#define GPR(N)        (BITMAP_SET_P(contexts[context_id].use_spec_R, R_BMAP_SZ, (N))\
								? contexts[context_id].spec_regs_R[N]                       \
								: contexts[context_id].regs.regs_R[N])
#define SET_GPR(N,EXPR)         (contexts[context_id].spec_mode				\
								? ((contexts[context_id].spec_regs_R[N] = (EXPR)),		\
												BITMAP_SET(contexts[context_id].use_spec_R, R_BMAP_SZ, (N)),\
												contexts[context_id].spec_regs_R[N])			\
								: (contexts[context_id].regs.regs_R[N] = (EXPR)))

#if defined(TARGET_PISA)

/* floating point register accessors, NOTE: speculative copy on write storage
	 provided for fast recovery during wrong path execute (see tracer_recover()
	 for details on this process */
#define FPR_L(N)                (BITMAP_SET_P(contexts[context_id].use_spec_F, F_BMAP_SZ, ((N)&~1))\
								? contexts[context_id].spec_regs_F.l[(N)]                   \
								: contexts[context_id].regs.regs_F.l[(N)])
#define SET_FPR_L(N,EXPR)       (contexts[context_id].spec_mode				\
								? ((contexts[context_id].spec_regs_F.l[(N)] = (EXPR)),	\
												BITMAP_SET(contexts[context_id].use_spec_F,F_BMAP_SZ,((N)&~1)),\
												contexts[context_id].spec_regs_F.l[(N)])			\
								: (contexts[context_id].regs.regs_F.l[(N)] = (EXPR)))
#define FPR_F(N)                (BITMAP_SET_P(contexts[context_id].use_spec_F, F_BMAP_SZ, ((N)&~1))\
								? contexts[context_id].spec_regs_F.f[(N)]                   \
								: contexts[context_id].regs.regs_F.f[(N)])
#define SET_FPR_F(N,EXPR)       (contexts[context_id].spec_mode				\
								? ((contexts[context_id].spec_regs_F.f[(N)] = (EXPR)),	\
												BITMAP_SET(contexts[context_id].use_spec_F,F_BMAP_SZ,((N)&~1)),\
												contexts[context_id].spec_regs_F.f[(N)])			\
								: (contexts[context_id].regs.regs_F.f[(N)] = (EXPR)))
#define FPR_D(N)                (BITMAP_SET_P(contexts[context_id].use_spec_F, F_BMAP_SZ, ((N)&~1))\
								? contexts[context_id].spec_regs_F.d[(N) >> 1]              \
								: contexts[context_id].regs.regs_F.d[(N) >> 1])
#define SET_FPR_D(N,EXPR)       (contexts[context_id].spec_mode				\
								? ((contexts[context_id].spec_regs_F.d[(N) >> 1] = (EXPR)),	\
												BITMAP_SET(contexts[context_id].use_spec_F,F_BMAP_SZ,((N)&~1)),\
												contexts[context_id].spec_regs_F.d[(N) >> 1])		\
								: (contexts[context_id].regs.regs_F.d[(N) >> 1] = (EXPR)))

/* miscellanous register accessors, NOTE: speculative copy on write storage
	 provided for fast recovery during wrong path execute (see tracer_recover()
	 for details on this process */
#define HI			(BITMAP_SET_P(contexts[context_id].use_spec_C, C_BMAP_SZ, /*hi*/0)\
								? contexts[context_id].spec_regs_C.hi			\
								: contexts[context_id].regs.regs_C.hi)
#define SET_HI(EXPR)		(contexts[context_id].spec_mode				\
								? ((contexts[context_id].spec_regs_C.hi = (EXPR)),		\
												BITMAP_SET(contexts[context_id].use_spec_C, C_BMAP_SZ,/*hi*/0),\
												contexts[context_id].spec_regs_C.hi)			\
								: (contexts[context_id].regs.regs_C.hi = (EXPR)))
#define LO			(BITMAP_SET_P(contexts[context_id].use_spec_C, C_BMAP_SZ, /*lo*/1)\
								? contexts[context_id].spec_regs_C.lo			\
								: contexts[context_id].regs.regs_C.lo)
#define SET_LO(EXPR)		(contexts[context_id].spec_mode				\
								? ((contexts[context_id].spec_regs_C.lo = (EXPR)),		\
												BITMAP_SET(contexts[context_id].use_spec_C, C_BMAP_SZ,/*lo*/1),\
												contexts[context_id].spec_regs_C.lo)			\
								: (contexts[context_id].regs.regs_C.lo = (EXPR)))
#define FCC			(BITMAP_SET_P(contexts[context_id].use_spec_C, C_BMAP_SZ,/*fcc*/2)\
								? contexts[context_id].spec_regs_C.fcc			\
								: contexts[context_id].regs.regs_C.fcc)
#define SET_FCC(EXPR)		(contexts[context_id].spec_mode				\
								? ((contexts[context_id].spec_regs_C.fcc = (EXPR)),		\
												BITMAP_SET(contexts[context_id].use_spec_C,C_BMAP_SZ,/*fcc*/2),\
												contexts[context_id].spec_regs_C.fcc)			\
								: (contexts[context_id].regs.regs_C.fcc = (EXPR)))

#elif defined(TARGET_ALPHA)

/* floating point register accessors, NOTE: speculative copy on write storage
	 provided for fast recovery during wrong path execute (see tracer_recover()
	 for details on this process */
#define FPR_Q(N)		(BITMAP_SET_P(contexts[context_id].use_spec_F, F_BMAP_SZ, (N))\
								? contexts[context_id].spec_regs_F.q[(N)]                   \
								: contexts[context_id].regs.regs_F.q[(N)])
#define SET_FPR_Q(N,EXPR)	(contexts[context_id].spec_mode				\
								? ((contexts[context_id].spec_regs_F.q[(N)] = (EXPR)),	\
												BITMAP_SET(contexts[context_id].use_spec_F,F_BMAP_SZ, (N)),\
												contexts[context_id].spec_regs_F.q[(N)])			\
								: (contexts[context_id].regs.regs_F.q[(N)] = (EXPR)))
#define FPR(N)			(BITMAP_SET_P(contexts[context_id].use_spec_F, F_BMAP_SZ, (N))\
								? contexts[context_id].spec_regs_F.d[(N)]			\
								: contexts[context_id].regs.regs_F.d[(N)])
#define SET_FPR(N,EXPR)		(contexts[context_id].spec_mode				\
								? ((contexts[context_id].spec_regs_F.d[(N)] = (EXPR)),	\
												BITMAP_SET(contexts[context_id].use_spec_F,F_BMAP_SZ, (N)),\
												contexts[context_id].spec_regs_F.d[(N)])			\
								: (contexts[context_id].regs.regs_F.d[(N)] = (EXPR)))

/* miscellanous register accessors, NOTE: speculative copy on write storage
	 provided for fast recovery during wrong path execute (see tracer_recover()
	 for details on this process */
#define FPCR			(BITMAP_SET_P(contexts[context_id].use_spec_C, C_BMAP_SZ,/*fpcr*/0)\
								? contexts[context_id].spec_regs_C.fpcr			\
								: contexts[context_id].regs.regs_C.fpcr)
#define SET_FPCR(EXPR)		(contexts[context_id].spec_mode				\
								? ((contexts[context_id].spec_regs_C.fpcr = (EXPR)),	\
												BITMAP_SET(contexts[context_id].use_spec_C,C_BMAP_SZ,/*fpcr*/0),\
												contexts[context_id].spec_regs_C.fpcr)			\
								: (contexts[context_id].regs.regs_C.fpcr = (EXPR)))
#define UNIQ			(BITMAP_SET_P(contexts[context_id].use_spec_C, C_BMAP_SZ,/*uniq*/1)\
								? contexts[context_id].spec_regs_C.uniq			\
								: contexts[context_id].regs.regs_C.uniq)
#define SET_UNIQ(EXPR)		(contexts[context_id].spec_mode				\
								? ((contexts[context_id].spec_regs_C.uniq = (EXPR)),	\
												BITMAP_SET(contexts[context_id].use_spec_C,C_BMAP_SZ,/*uniq*/1),\
												contexts[context_id].spec_regs_C.uniq)			\
								: (contexts[context_id].regs.regs_C.uniq = (EXPR)))
#define FCC			(BITMAP_SET_P(contexts[context_id].use_spec_C, C_BMAP_SZ,/*fcc*/2)\
								? contexts[context_id].spec_regs_C.fcc			\
								: contexts[context_id].regs.regs_C.fcc)
#define SET_FCC(EXPR)		(contexts[context_id].spec_mode				\
								? ((contexts[context_id].spec_regs_C.fcc = (EXPR)),		\
												BITMAP_SET(contexts[context_id].use_spec_C,C_BMAP_SZ,/*fcc*/1),\
												contexts[context_id].spec_regs_C.fcc)			\
								: (contexts[context_id].regs.regs_C.fcc = (EXPR)))

#else
#error No ISA target defined...
#endif

/* precise architected memory state accessor macros, NOTE: speculative copy on
	 write storage provided for fast recovery during wrong path execute (see
	 tracer_recover() for details on this process */
#define __READ_SPECMEM(SRC, SRC_V, FAULT)				\
				(addr = (SRC),							\
				 (contexts[context_id].spec_mode								\
					? ((FAULT) = spec_mem_access(contexts[context_id].mem, Read, addr, &SRC_V, sizeof(SRC_V),context_id))\
					: ((FAULT) = mem_access(contexts[context_id].mem, Read, addr, &SRC_V, sizeof(SRC_V)),context_id)),	\
				 SRC_V)

#define READ_BYTE(SRC, FAULT)						\
				__READ_SPECMEM((SRC), temp_byte, (FAULT))
#define READ_HALF(SRC, FAULT)						\
				MD_SWAPH(__READ_SPECMEM((SRC), temp_half, (FAULT)))
#define READ_WORD(SRC, FAULT)						\
				MD_SWAPW(__READ_SPECMEM((SRC), temp_word, (FAULT)))
#ifdef HOST_HAS_QWORD
#define READ_QWORD(SRC, FAULT)						\
				MD_SWAPQ(__READ_SPECMEM((SRC), temp_qword, (FAULT)))
#endif /* HOST_HAS_QWORD */


#define __WRITE_SPECMEM(SRC, DST, DST_V, FAULT)				\
				(DST_V = (SRC), addr = (DST),						\
				 (contexts[context_id].spec_mode								\
					? ((FAULT) = spec_mem_access(contexts[context_id].mem, Write, addr, &DST_V, sizeof(DST_V),context_id))\
					: ((FAULT) = mem_access(contexts[context_id].mem, Write, addr, &DST_V, sizeof(DST_V)))))

#define WRITE_BYTE(SRC, DST, FAULT)					\
				__WRITE_SPECMEM((SRC), (DST), temp_byte, (FAULT))
#define WRITE_HALF(SRC, DST, FAULT)					\
				__WRITE_SPECMEM(MD_SWAPH(SRC), (DST), temp_half, (FAULT))
#define WRITE_WORD(SRC, DST, FAULT)					\
				__WRITE_SPECMEM(MD_SWAPW(SRC), (DST), temp_word, (FAULT))
#ifdef HOST_HAS_QWORD
#define WRITE_QWORD(SRC, DST, FAULT)					\
				__WRITE_SPECMEM(MD_SWAPQ(SRC), (DST), temp_qword, (FAULT))
#endif /* HOST_HAS_QWORD */

/* system call handler macro */
#define SYSCALL(INST)							\
				(/* only execute system calls in non-speculative mode */		\
				 (contexts[current_context].spec_mode ? panic("speculative syscall") : (void) 0),		\
				 sys_syscall(&contexts[current_context].regs, mem_access,contexts[current_context].mem, INST, TRUE))

#if 0
/* default register state accessor, used by DLite */
				static char *					/* err str, NULL for no err */
simoo_reg_obj(struct regs_t *xregs,		/* registers to access */
								int is_write,			/* access type */
								enum md_reg_type rt,		/* reg bank to probe */
								int reg,				/* register number */
								struct eval_value_t *val,		/* input, output */
								int context_id)
{
				switch (rt)
				{
								case rt_gpr:
												if (reg < 0 || reg >= MD_NUM_IREGS)
																return "register number out of range";

												if (!is_write)
												{
																val->type = et_uint;
																val->value.as_uint = GPR(reg);
												}
												else
																SET_GPR(reg, eval_as_uint(*val));
												break;

								case rt_lpr:
												if (reg < 0 || reg >= MD_NUM_FREGS)
																return "register number out of range";

												/* FIXME: this is not portable... */
												abort();
#if 0
												if (!is_write)
												{
																val->type = et_uint;
																val->value.as_uint = FPR_L(reg,context_id);
												}
												else
																SET_FPR_L(reg, eval_as_uint(*val),context_id);
#endif
												break;

								case rt_fpr:
												/* FIXME: this is not portable... */
												abort();
#if 0
												if (!is_write)
																val->value.as_float = FPR_F(reg,context_id);
												else
																SET_FPR_F(reg, val->value.as_float,context_id);
#endif
												break;

								case rt_dpr:
												/* FIXME: this is not portable... */
												abort();
#if 0
												/* 1/2 as many regs in this mode */
												if (reg < 0 || reg >= MD_NUM_REGS/2)
																return "register number out of range";

												if (at == at_read)
																val->as_double = FPR_D(reg * 2,context_id);
												else
																SET_FPR_D(reg * 2, val->as_double,context_id);
#endif
												break;

												/* FIXME: this is not portable... */
#if 0
												abort();
								case rt_hi:
												if (at == at_read)
																val->as_word = HI(context_id);
												else
																SET_HI(val->as_word,context_id);
												break;
								case rt_lo:
												if (at == at_read)
																val->as_word = LO(context_id);
												else
																SET_LO(val->as_word,context_id);
												break;
								case rt_FCC:
												if (at == at_read)
																val->as_condition = FCC(context_id);
												else
																SET_FCC(val->as_condition,context_id);
												break;
#endif

								case rt_PC:
												if (!is_write)
												{
																val->type = et_addr;
																val->value.as_addr = contexts[context_id].regs.regs_PC;
												}
												else
																contexts[context_id].regs.regs_PC = eval_as_addr(*val);
												break;

								case rt_NPC:
												if (!is_write)
												{
																val->type = et_addr;
																val->value.as_addr = contexts[context_id].regs.regs_NPC;
												}
												else
																contexts[context_id].regs.regs_NPC = eval_as_addr(*val);
												break;

								default:
												panic("bogus register bank");
				}

				/* no error */
				return NULL;
}
#endif
/* dispatch instructions from the IFETCH -> DISPATCH queue: instructions are
	 first decoded, then they allocated RUU (and LSQ for load/stores) resources
	 and input and output dependence chains are updated accordingly */
				static void
ruu_dispatch(int context_id)
{
				int i;
				int n_dispatched;			/* total insts dispatched */
				md_inst_t inst;			/* actual instruction bits */
				enum md_opcode op;			/* decoded opcode enum */
				int out1, out2, in1, in2, in3;	/* output/input register names */
				md_addr_t target_PC;			/* actual next/target PC address */
				md_addr_t addr;			/* effective address, if load/store */
				struct RUU_station *rs;		/* RUU station being allocated */
				struct RUU_station *lsq;		/* LSQ station for ld/st's */
				struct bpred_update_t *dir_update_ptr;/* branch predictor dir update ptr */
				int stack_recover_idx;		/* bpred retstack recovery index */
				unsigned int pseq;			/* pipetrace sequence number */
				int is_write;				/* store? */
				int made_check;			/* used to ensure DLite entry */
				int br_taken, br_pred_taken;		/* if br, taken?  predicted taken? */
				int fetch_redirected = FALSE;
				byte_t temp_byte = 0;			/* temp variable for spec mem access */
				half_t temp_half = 0;			/* " ditto " */
				word_t temp_word = 0;			/* " ditto " */
#ifdef HOST_HAS_QWORD
				qword_t temp_qword = 0;		/* " ditto " */
#endif /* HOST_HAS_QWORD */
				enum md_fault_type fault;
				int icache_miss;
				int icache_miss_l2;

				made_check = FALSE;
				n_dispatched = 0;
				while (/* instruction decode B/W left? */
												n_dispatched < (contexts[context_id].ruu_decode_width * contexts[context_id].fetch_speed)
												/* RUU and LSQ not full? */
												&& contexts[context_id].RUU_num < contexts[context_id].RUU_size && contexts[context_id].LSQ_num < contexts[context_id].LSQ_size
												/* insts still available from fetch unit? */
												&& contexts[context_id].fetch_num != 0
												/* on an acceptable trace path */
												&& (contexts[context_id].ruu_include_spec || !(contexts[context_id].spec_mode)))
				{
								/* if issuing in-order, block until last op issues if inorder issue */
								if (contexts[context_id].ruu_inorder_issue
																&& (contexts[current_context].last_op.rs && RSLINK_VALID(&contexts[current_context].last_op)
																				&& !OPERANDS_READY(contexts[current_context].last_op.rs)))
								{
												/* stall until last operation is ready to issue */
												break;
								}

								/* get the next instruction from the IFETCH -> DISPATCH queue */
								inst = contexts[context_id].fetch_data[contexts[context_id].fetch_head].IR;
								contexts[context_id].regs.regs_PC = contexts[context_id].fetch_data[contexts[context_id].fetch_head].regs_PC;
								contexts[context_id].pred_PC = contexts[context_id].fetch_data[contexts[context_id].fetch_head].pred_PC;
								dir_update_ptr = &(contexts[context_id].fetch_data[contexts[context_id].fetch_head].dir_update);
								stack_recover_idx = contexts[context_id].fetch_data[contexts[context_id].fetch_head].stack_recover_idx;
								pseq = contexts[context_id].fetch_data[contexts[context_id].fetch_head].ptrace_seq;
								/* sudiptac: for Chronos */
								icache_miss = contexts[context_id].fetch_data[contexts[context_id].fetch_head].icache_miss;
								icache_miss_l2 = contexts[context_id].fetch_data[contexts[context_id].fetch_head].icache_miss_l2;


								/* decode the inst */
								MD_SET_OPCODE(op, inst);

								/* compute default next PC */
								contexts[context_id].regs.regs_NPC = contexts[context_id].regs.regs_PC + sizeof(md_inst_t);

								/* drain RUU for TRAPs and system calls */
								if (MD_OP_FLAGS(op) & F_TRAP)
								{
												if (contexts[context_id].RUU_num != 0)
																break;

												/* else, syscall is only instruction in the machine, at this
													 point we should not be in (mis-)speculative mode */
												if (contexts[context_id].spec_mode)
																panic("drained and speculative");
								}

								/* maintain $r0 semantics (in spec and non-spec space) */
								contexts[context_id].regs.regs_R[MD_REG_ZERO] = 0; contexts[context_id].spec_regs_R[MD_REG_ZERO] = 0;
#ifdef TARGET_ALPHA
								contexts[context_id].regs.regs_F.d[MD_REG_ZERO] = 0.0; contexts[context_id].spec_regs_F.d[MD_REG_ZERO] = 0.0;
#endif /* TARGET_ALPHA */

								if (!contexts[context_id].spec_mode)
								{
												/* one more non-speculative instruction executed */
												contexts[context_id].sim_num_insn++;
								}

								/* default effective address (none) and access */
								addr = 0; is_write = FALSE;

								/* set default fault - none */
								fault = md_fault_none;

								/* more decoding and execution */
								switch (op)
								{
#define DEFINST(OP,MSK,NAME,OPFORM,RES,CLASS,O1,O2,I1,I2,I3)		\
												case OP:							\
																							/* compute output/input dependencies to out1-2 and in1-3 */	\
												out1 = O1; out2 = O2;						\
												in1 = I1; in2 = I2; in3 = I3;					\
												/* execute the instruction */					\
												SYMCAT(OP,_IMPL);						\
												break;
#define DEFLINK(OP,MSK,NAME,MASK,SHIFT)					\
												case OP:							\
																							/* could speculatively decode a bogus inst, convert to NOP */	\
												op = MD_NOP_OP;						\
												/* compute output/input dependencies to out1-2 and in1-3 */	\
												out1 = NA; out2 = NA;						\
												in1 = NA; in2 = NA; in3 = NA;					\
												/* no EXPR */							\
												break;
#define CONNECT(OP)	/* nada... */
												/* the following macro wraps the instruction fault declaration macro
													 with a test to see if the trace generator is in non-speculative
													 mode, if so the instruction fault is declared, otherwise, the
													 error is shunted because instruction faults need to be masked on
													 the mis-speculated instruction paths */
#define DECLARE_FAULT(FAULT)						\
												{								\
																if (!contexts[context_id].spec_mode)						\
																fault = (FAULT);						\
																/* else, spec fault, ignore it, always terminate exec... */	\
																break;							\
												}
#include "machine.def"
												default:
																/* can speculatively decode a bogus inst, convert to a NOP */
																op = MD_NOP_OP;
																/* compute output/input dependencies to out1-2 and in1-3 */	\
																				out1 = NA; out2 = NA;
																in1 = NA; in2 = NA; in3 = NA;
																/* no EXPR */
								}
								/* operation sets next PC */

								/* print retirement trace if in verbose mode */
								if (!contexts[context_id].spec_mode && verbose)
								{
												myfprintf(stderr, "++ %10n [xor: 0x%08x] {%d} @ 0x%08p: ",
																				contexts[context_id].sim_num_insn, md_xor_regs(&(contexts[context_id].regs)),
																				(contexts[context_id].inst_seq)+1, contexts[context_id].regs.regs_PC);
												md_print_insn(inst, contexts[context_id].regs.regs_PC, stderr);
												fprintf(stderr, "\n");
												/* fflush(stderr); */
								}

								if (fault != md_fault_none)
												fatal("non-speculative fault (%d) detected @ 0x%08p",
																				fault, contexts[context_id].regs.regs_PC);

								/* update memory access stats */
								if (MD_OP_FLAGS(op) & F_MEM)
								{
												contexts[context_id].sim_total_refs++;
												if (!contexts[context_id].spec_mode)
																contexts[context_id].sim_num_refs++;

												if (MD_OP_FLAGS(op) & F_STORE)
																is_write = TRUE;
												else
												{
																contexts[context_id].sim_total_loads++;
																if (!contexts[context_id].spec_mode)
																				contexts[context_id].sim_num_loads++;
												}
								}

								br_taken = (contexts[context_id].regs.regs_NPC != (contexts[context_id].regs.regs_PC + sizeof(md_inst_t)));
								br_pred_taken = (contexts[context_id].pred_PC != (contexts[context_id].regs.regs_PC + sizeof(md_inst_t)));

								if ((contexts[context_id].pred_PC != contexts[context_id].regs.regs_NPC && contexts[context_id].pred_perfect)
																|| ((MD_OP_FLAGS(op) & (F_CTRL|F_DIRJMP)) == (F_CTRL|F_DIRJMP)
																				&& target_PC != contexts[context_id].pred_PC && br_pred_taken))
								{
												/* Either 1) we're simulating perfect prediction and are in a
													 mis-predict state and need to patch up, or 2) We're not simulating
													 perfect prediction, we've predicted the branch taken, but our
													 predicted target doesn't match the computed target (i.e.,
													 mis-fetch).  Just update the PC values and do a fetch squash.
													 This is just like calling fetch_squash() except we pre-anticipate
													 the updates to the fetch values at the end of this function.  If
													 case #2, also charge a mispredict penalty for redirecting fetch */
												contexts[context_id].fetch_pred_PC = contexts[context_id].fetch_regs_PC = contexts[context_id].regs.regs_NPC;

												/* sudiptac: for Chronos: dont' let unconditional branches stall the fetcher */
												if (contexts[context_id].pred_perfect || ((MD_OP_FLAGS(op) & (F_CTRL|F_UNCOND)) == (F_CTRL|F_UNCOND))) {
																contexts[context_id].pred_PC = contexts[context_id].regs.regs_NPC;
																contexts[context_id].ruu_fetch_issue_delay = 0;
												} else {
																contexts[context_id].ruu_fetch_issue_delay = contexts[context_id].ruu_branch_penalty;
												}

#if 0
												/* was: if (pred_perfect) */
												if (contexts[context_id].pred_perfect)
																contexts[context_id].pred_PC = contexts[context_id].regs.regs_NPC;
#endif

												contexts[context_id].fetch_head = (contexts[context_id].ruu_ifq_size-1);
												contexts[context_id].fetch_num = 1;
												contexts[context_id].fetch_tail = 0;

												if (!contexts[context_id].pred_perfect)
																contexts[context_id].ruu_fetch_issue_delay = contexts[context_id].ruu_branch_penalty;

												fetch_redirected = TRUE;
												contexts[context_id].my_last_inst_missed = FALSE;
												contexts[context_id].my_last_inst_missed_in_l2 = FALSE;
								}

								/* is this a NOP */
								if (op != MD_NOP_OP)
								{
												/* for load/stores:
													 idep #0     - store operand (value that is store'ed)
													 idep #1, #2 - eff addr computation inputs (addr of access)

													 resulting RUU/LSQ operation pair:
													 RUU (effective address computation operation):
													 idep #0, #1 - eff addr computation inputs (addr of access)
													 LSQ (memory access operation):
													 idep #0     - operand input (value that is store'd)
													 idep #1     - eff addr computation result (from RUU op)

													 effective address computation is transfered via the reserved
													 name DTMP
													 */

												/* fill in RUU reservation station */
												rs = &contexts[context_id].RUU[contexts[context_id].RUU_tail];
												rs->slip = sim_cycle - 1;
												rs->IR = inst;
												rs->op = op;
												rs->PC = contexts[context_id].regs.regs_PC;
												rs->next_PC = contexts[context_id].regs.regs_NPC; rs->pred_PC = contexts[context_id].pred_PC;
												rs->in_LSQ = FALSE;
												rs->ea_comp = FALSE;
												rs->recover_inst = FALSE;
												rs->dir_update = *dir_update_ptr;
												rs->stack_recover_idx = stack_recover_idx;
												rs->spec_mode = contexts[context_id].spec_mode;
												rs->addr = 0;
												/* rs->tag is already set */
												rs->seq = ++(contexts[context_id].inst_seq);
												rs->queued = rs->issued = rs->completed = FALSE;
												rs->ptrace_seq = pseq;
												/* sudiptac: for Chronos */
												rs->icache_miss = icache_miss;
												rs->icache_miss_l2 = icache_miss_l2;

												/* split ld/st's into two operations: eff addr comp + mem access */
												if (MD_OP_FLAGS(op) & F_MEM)
												{
																/* convert RUU operation from ld/st to an add (eff addr comp) */
																rs->op = MD_AGEN_OP;
																rs->ea_comp = TRUE;

																/* fill in LSQ reservation station */
																lsq = &contexts[context_id].LSQ[contexts[context_id].LSQ_tail];
																lsq->slip = sim_cycle - 1;
																lsq->IR = inst;
																lsq->op = op;
																lsq->PC = contexts[context_id].regs.regs_PC;
																lsq->next_PC = contexts[context_id].regs.regs_NPC; lsq->pred_PC = contexts[context_id].pred_PC;
																lsq->in_LSQ = TRUE;
																lsq->ea_comp = FALSE;
																lsq->recover_inst = FALSE;
																lsq->dir_update.pdir1 = lsq->dir_update.pdir2 = NULL;
																lsq->dir_update.pmeta = NULL;
																lsq->stack_recover_idx = 0;
																lsq->spec_mode = contexts[context_id].spec_mode;
																lsq->addr = addr;
																/* lsq->tag is already set */
																lsq->seq = ++(contexts[context_id].inst_seq);
																lsq->queued = lsq->issued = lsq->completed = FALSE;
																lsq->ptrace_seq = (contexts[context_id].ptrace_seq)++;

																/* pipetrace this uop */
																//ptrace_newuop(lsq->ptrace_seq, "internal ld/st", lsq->PC, 0);
																//ptrace_newstage(lsq->ptrace_seq, PST_DISPATCH, 0);

																/* link eff addr computation onto operand's output chains */
																ruu_link_idep(rs, /* idep_ready[] index */0, NA, context_id);
																ruu_link_idep(rs, /* idep_ready[] index */1, in2, context_id);
																ruu_link_idep(rs, /* idep_ready[] index */2, in3, context_id);

																/* install output after inputs to prevent self reference */
																ruu_install_odep(rs, /* odep_list[] index */0, DTMP, context_id);
																ruu_install_odep(rs, /* odep_list[] index */1, NA, context_id);

																/* link memory access onto output chain of eff addr operation */
																ruu_link_idep(lsq,
																								/* idep_ready[] index */STORE_OP_INDEX/* 0 */,
																								in1, context_id);
																ruu_link_idep(lsq,
																								/* idep_ready[] index */STORE_ADDR_INDEX/* 1 */,
																								DTMP, context_id);
																ruu_link_idep(lsq, /* idep_ready[] index */2, NA, context_id);

																/* install output after inputs to prevent self reference */
																ruu_install_odep(lsq, /* odep_list[] index */0, out1, context_id);
																ruu_install_odep(lsq, /* odep_list[] index */1, out2, context_id);

																/* install operation in the RUU and LSQ */
																n_dispatched++;
																contexts[context_id].RUU_tail = (contexts[context_id].RUU_tail + 1) % contexts[context_id].RUU_size;
																contexts[context_id].RUU_num++;
																contexts[context_id].LSQ_tail = (contexts[context_id].LSQ_tail + 1) % contexts[context_id].LSQ_size;
																contexts[context_id].LSQ_num++;

																if (OPERANDS_READY(rs))
																{
																				/* eff addr computation ready, queue it on ready list */
																				readyq_enqueue(rs, context_id);
																}
																/* issue may continue when the load/store is issued */
																RSLINK_INIT(contexts[current_context].last_op, lsq);

																/* issue stores only, loads are issued by lsq_refresh() */
																if (((MD_OP_FLAGS(op) & (F_MEM|F_STORE)) == (F_MEM|F_STORE))
																								&& OPERANDS_READY(lsq))
																{
																				/* panic("store immediately ready"); */
																				/* put operation on ready list, ruu_issue() issue it later */
																				readyq_enqueue(lsq, context_id);
																}
												}
												else /* !(MD_OP_FLAGS(op) & F_MEM) */
												{
																/* link onto producing operation */
																ruu_link_idep(rs, /* idep_ready[] index */0, in1, context_id);
																ruu_link_idep(rs, /* idep_ready[] index */1, in2, context_id);
																ruu_link_idep(rs, /* idep_ready[] index */2, in3, context_id);

																/* install output after inputs to prevent self reference */
																ruu_install_odep(rs, /* odep_list[] index */0, out1, context_id);
																ruu_install_odep(rs, /* odep_list[] index */1, out2, context_id);

																/* install operation in the RUU */
																n_dispatched++;
																contexts[context_id].RUU_tail = (contexts[context_id].RUU_tail + 1) % contexts[context_id].RUU_size;
																contexts[context_id].RUU_num++;

																/* issue op if all its reg operands are ready (no mem input) */
																if (OPERANDS_READY(rs))
																{
																				/* put operation on ready list, ruu_issue() issue it later */
																				readyq_enqueue(rs, context_id);
																				/* issue may continue */
																				contexts[current_context].last_op = RSLINK_NULL;
																}
																else
																{
																				/* could not issue this inst, stall issue until we can */
																				RSLINK_INIT(contexts[current_context].last_op, rs);
																}
												}
								}
								else
								{
												/* this is a NOP, no need to update RUU/LSQ state */
												rs = NULL;
								}

								/* one more instruction executed, speculative or otherwise */
								contexts[context_id].sim_total_insn++;
								if (MD_OP_FLAGS(op) & F_CTRL)
												contexts[context_id].sim_total_branches++;

								if (!contexts[context_id].spec_mode)
								{
#if 0 /* moved above for EIO trace file support */
												/* one more non-speculative instruction executed */
												sim_num_insn++;
#endif

												/* if this is a branching instruction update BTB, i.e., only
													 non-speculative state is committed into the BTB */
												if (MD_OP_FLAGS(op) & F_CTRL)
												{
																contexts[context_id].sim_num_branches++;
																if (contexts[context_id].pred && contexts[context_id].bpred_spec_update == spec_ID)
																{
																				bpred_update(contexts[context_id].pred,
																												/* branch address */contexts[context_id].regs.regs_PC,
																												/* actual target address */contexts[context_id].regs.regs_NPC,
																												/* taken? */contexts[context_id].regs.regs_NPC != (contexts[context_id].regs.regs_PC +
																																sizeof(md_inst_t)),
																												/* pred taken? */contexts[context_id].pred_PC != (contexts[context_id].regs.regs_PC +
																																sizeof(md_inst_t)),
																												/* correct pred? */contexts[context_id].pred_PC == contexts[context_id].regs.regs_NPC,
																												/* opcode */op,
																												/* predictor update ptr */&rs->dir_update, 
																												/* current core */ context_id);
																}
												}

												/* is the trace generator trasitioning into mis-speculation mode? */
												if (contexts[context_id].pred_PC != contexts[context_id].regs.regs_NPC && !fetch_redirected)
												{
																/* entering mis-speculation mode, indicate this and save PC */
																contexts[context_id].spec_mode = TRUE;
																rs->recover_inst = TRUE;
																contexts[context_id].recover_PC = contexts[context_id].regs.regs_NPC;
																/* sudiptac: for Chronos */
																/* not counting cache misses in misprediction */
																contexts[context_id].my_last_inst_missed = FALSE;
																contexts[context_id].my_last_inst_missed_in_l2 = FALSE;
												}
								}

								/* entered decode/allocate stage, indicate in pipe trace */
								//ptrace_newstage(pseq, PST_DISPATCH,
								//(contexts[context_id].pred_PC != contexts[context_id].regs.regs_NPC) ? PEV_MPOCCURED : 0);
								if (op == MD_NOP_OP)
								{
												/* end of the line */
												//  ptrace_endinst(pseq);
								}

								/* update any stats tracked by PC */
								for (i=0; i<contexts[context_id].pcstat_nelt; i++)
								{
												counter_t newval;
												int delta;

												/* check if any tracked stats changed */
												newval = STATVAL(contexts[context_id].pcstat_stats[i]);
												delta = newval - contexts[context_id].pcstat_lastvals[i];
												if (delta != 0)
												{
																stat_add_samples(contexts[context_id].pcstat_sdists[i], contexts[context_id].regs.regs_PC, delta);
																contexts[context_id].pcstat_lastvals[i] = newval;
												}
								}

								/* consume instruction from IFETCH -> DISPATCH queue */
								contexts[context_id].fetch_head = (contexts[context_id].fetch_head+1) & (contexts[context_id].ruu_ifq_size - 1);
								contexts[context_id].fetch_num--;

								/* check for DLite debugger entry condition */
								made_check = TRUE;
								if (dlite_check_break(contexts[context_id].pred_PC,
																				is_write ? ACCESS_WRITE : ACCESS_READ,
																				addr, contexts[context_id].sim_num_insn, sim_cycle))
												dlite_main(contexts[context_id].regs.regs_PC, contexts[context_id].pred_PC, sim_cycle, &(contexts[context_id].regs), contexts[context_id].mem);
				}

				/* need to enter DLite at least once per cycle */
				if (!made_check)
				{
								if (dlite_check_break(/* no next PC */0,
																				is_write ? ACCESS_WRITE : ACCESS_READ,
																				addr,contexts[context_id].sim_num_insn, sim_cycle))
												dlite_main(contexts[context_id].regs.regs_PC, /* no next PC */0, sim_cycle, &(contexts[context_id].regs), contexts[context_id].mem);
				}
}
/*
 *  RUU_FETCH() - instruction fetch pipeline stage(s)
 */

/* initialize the instruction fetch pipeline stage */
				static void
fetch_init(int context_id)
{
				/* allocate the IFETCH -> DISPATCH instruction queue */
				contexts[context_id].fetch_data =
								(struct fetch_rec *)calloc(contexts[context_id].ruu_ifq_size, sizeof(struct fetch_rec));
				if (!contexts[context_id].fetch_data)
								fatal("out of virtual memory");

				contexts[context_id].fetch_num = 0;
				contexts[context_id].fetch_tail = contexts[context_id].fetch_head = 0;
				contexts[context_id].IFQ_count = 0;
				contexts[context_id].IFQ_fcount = 0;
}/* dump contents of fetch stage registers and fetch queue */

				void
fetch_dump(FILE *stream,int context_id)			/* output stream */
{
				int num, head;

				if (!stream)
								stream = stderr;

				fprintf(stream, "** fetch stage state **\n");

				fprintf(stream, "spec_mode: %s\n", contexts[context_id].spec_mode ? "t" : "f");
				myfprintf(stream, "pred_PC: 0x%08p, recover_PC: 0x%08p\n",
												contexts[context_id].pred_PC, contexts[context_id].recover_PC);
				myfprintf(stream, "fetch_regs_PC: 0x%08p, fetch_pred_PC: 0x%08p\n",
												contexts[context_id].fetch_regs_PC, contexts[context_id].fetch_pred_PC);
				fprintf(stream, "\n");

				fprintf(stream, "** fetch queue contents **\n");
				fprintf(stream, "fetch_num: %d\n", contexts[context_id].fetch_num);
				fprintf(stream, "fetch_head: %d, fetch_tail: %d\n",
												contexts[context_id].fetch_head, contexts[context_id].fetch_tail);

				num = contexts[context_id].fetch_num;
				head = contexts[context_id].fetch_head;
				while (num)
				{
								fprintf(stream, "idx: %2d: inst: `", head);
								md_print_insn(contexts[context_id].fetch_data[head].IR, contexts[context_id].fetch_data[head].regs_PC, stream);
								fprintf(stream, "'\n");
								myfprintf(stream, "         regs_PC: 0x%08p, pred_PC: 0x%08p\n",
																contexts[context_id].fetch_data[head].regs_PC, contexts[context_id].fetch_data[head].pred_PC);
								head = (head + 1) & (contexts[context_id].ruu_ifq_size - 1);
								num--;
				}
}

//static int last_inst_missed = FALSE;
//static int last_inst_tmissed = FALSE;

/* fetch up as many instruction as one branch prediction and one cache line
	 acess will support without overflowing the IFETCH -> DISPATCH QUEUE */
				static void
ruu_fetch(int context_id)
{
				int i, done = FALSE;
				unsigned int lat, tlb_lat;
				md_inst_t inst;
				int stack_recover_idx;
				int branch_cnt;

				for (i=0, branch_cnt=0;
												/* fetch up to as many instruction as the DISPATCH stage can decode */
												i < (contexts[context_id].ruu_decode_width * contexts[context_id].fetch_speed)
												/* fetch until IFETCH -> DISPATCH queue fills */
												&& contexts[context_id].fetch_num < contexts[context_id].ruu_ifq_size
												/* and no IFETCH blocking condition encountered */
												&& !done;
												i++)
				{
								/* fetch an instruction at the next predicted fetch address */
								contexts[context_id].fetch_regs_PC = contexts[context_id].fetch_pred_PC;

								/* is this a bogus text address? (can happen on mis-spec path) */
								if (contexts[context_id].mem->ld_text_base <= contexts[context_id].fetch_regs_PC
																&& contexts[context_id].fetch_regs_PC < (contexts[context_id].mem->ld_text_base+contexts[context_id].mem->ld_text_size)
																&& !(contexts[context_id].fetch_regs_PC & (sizeof(md_inst_t)-1))) 
								{
												/* read instruction from memory */
												MD_FETCH_INST(inst, contexts[context_id].mem, contexts[context_id].fetch_regs_PC);

												/* address is within program text, read instruction from memory */
												lat = contexts[context_id].cache_il1_lat;
												if (contexts[context_id].cache_il1)
												{
																/* for multi-tasking, make sure you are not accessing the virtual cache */
																int phy_context_id;
																
																/* sudiptac: for chronos and measuring only cache misses at user code due to preemption */
																if(contexts[context_id].fetch_regs_PC >= contexts[context_id].mem->start_addr && 
																				contexts[context_id].fetch_regs_PC < contexts[context_id].mem->end_addr)	  
																				phy_context_id = virtual_to_physical_core_map[context_id];
																else
																				phy_context_id = context_id;
																				
																/* access the I-cache */
																lat = cache_access(contexts[phy_context_id].cache_il1, Read, \
																				IACOMPRESS(contexts[context_id].fetch_regs_PC, \
																				contexts[context_id].compress_icache_addrs, \
																				(contexts[context_id].mem)->ld_text_base),context_id,\
																				NULL, ISCOMPRESS(sizeof(md_inst_t), \
																				contexts[context_id].compress_icache_addrs), sim_cycle,\
																				NULL, NULL);
																if (lat > contexts[context_id].cache_il1_lat) {
																				contexts[context_id].last_inst_missed = TRUE;

																				if (!contexts[context_id].spec_mode) {
																								contexts[context_id].my_last_inst_missed = TRUE;

																								if (contexts[context_id].cache_il2 && lat > contexts[context_id].cache_il2_lat)
																												contexts[context_id].my_last_inst_missed_in_l2 = TRUE;
																				}
																				#if 0
																				#define CACHE_SET(cp, addr)	(((addr) >> (cp)->set_shift) & (cp)->set_mask)
																				if (contexts[context_id].cache_il2 && lat > contexts[context_id].cache_il2_lat) {
																								if (context_id == 1) {
																												fprintf(stdout, "miss latency = %d\n", lat);
																												fprintf(stdout, "L2 cache missed at %x at set %d\n", \
																																IACOMPRESS(contexts[context_id].fetch_regs_PC,
																																contexts[context_id].compress_icache_addrs,
																																(contexts[context_id].mem)->ld_text_base),
																																CACHE_SET(contexts[0].cache_il2, 
																																IACOMPRESS(contexts[context_id].fetch_regs_PC,
																																contexts[context_id].compress_icache_addrs,
																																(contexts[context_id].mem)->ld_text_base)));
																								}
																				}
																				#endif
																}

												}

												if (contexts[context_id].itlb)
												{
																/* access the I-TLB, NOTE: this code will initiate
																	 speculative TLB misses */
																tlb_lat =
																				cache_access(contexts[context_id].itlb, Read, IACOMPRESS(contexts[context_id].fetch_regs_PC,
																																contexts[context_id].compress_icache_addrs,contexts[context_id].mem->ld_text_base),context_id,
																												NULL, ISCOMPRESS(sizeof(md_inst_t),contexts[context_id].compress_icache_addrs), sim_cycle,
																												NULL, NULL);
																if (tlb_lat > 1)
																				contexts[context_id].last_inst_tmissed = TRUE;

																/* I-cache/I-TLB accesses occur in parallel */
																lat = MAX(tlb_lat, lat);
												}

												/* I-cache/I-TLB miss? assumes I-cache hit >= I-TLB hit */
												if (lat != contexts[context_id].cache_il1_lat)
												{
																/* I-cache miss, block fetch until it is resolved */
																contexts[context_id].ruu_fetch_issue_delay += lat - 1;
																break;
												}
												/* else, I-cache/I-TLB hit */
								}
								else
								{
												/* fetch PC is bogus, send a NOP down the pipeline */
												inst = MD_NOP_INST;
								}

								/* have a valid inst, here */

								/* possibly use the BTB target */
								if (contexts[context_id].pred)
								{
												enum md_opcode op;

												/* pre-decode instruction, used for bpred stats recording */
												MD_SET_OPCODE(op, inst);

												/* get the next predicted fetch address; only use branch predictor
													 result for branches (assumes pre-decode bits); NOTE: returned
													 value may be 1 if bpred can only predict a direction */
												if (MD_OP_FLAGS(op) & F_CTRL)
																contexts[context_id].fetch_pred_PC =
																				bpred_lookup(contexts[context_id].pred,
																												/* branch address */contexts[context_id].fetch_regs_PC,
																												/* target address *//* FIXME: not computed */0,
																												/* opcode */op,
																												/* call? */MD_IS_CALL(op),
																												/* return? */MD_IS_RETURN(op),
																												/* updt */&(contexts[context_id].fetch_data[contexts[context_id].fetch_tail].dir_update),
																												/* RSB index */&stack_recover_idx);
												else
																contexts[context_id].fetch_pred_PC = 0;

												/* valid address returned from branch predictor? */
												if (!contexts[context_id].fetch_pred_PC)
												{
																/* no predicted taken target, attempt not taken target */
																contexts[context_id].fetch_pred_PC = contexts[context_id].fetch_regs_PC + sizeof(md_inst_t);
												}
												else
												{
																/* go with target, NOTE: discontinuous fetch, so terminate */
																branch_cnt++;
																if (branch_cnt >= contexts[context_id].fetch_speed)
																				done = TRUE;
												}
								}
								else
								{
												/* no predictor, just default to predict not taken, and
													 continue fetching instructions linearly */
												contexts[context_id].fetch_pred_PC = contexts[context_id].fetch_regs_PC + sizeof(md_inst_t);
								}

								/* commit this instruction to the IFETCH -> DISPATCH queue */
								contexts[context_id].fetch_data[contexts[context_id].fetch_tail].IR = inst;
								contexts[context_id].fetch_data[contexts[context_id].fetch_tail].regs_PC = contexts[context_id].fetch_regs_PC;
								contexts[context_id].fetch_data[contexts[context_id].fetch_tail].pred_PC = contexts[context_id].fetch_pred_PC;
								contexts[context_id].fetch_data[contexts[context_id].fetch_tail].stack_recover_idx = stack_recover_idx;
								contexts[context_id].fetch_data[contexts[context_id].fetch_tail].ptrace_seq = (contexts[context_id].ptrace_seq)++;
								/* sudiptac: for Chronos */
								contexts[context_id].fetch_data[contexts[context_id].fetch_tail].icache_miss = contexts[context_id].my_last_inst_missed;
								contexts[context_id].fetch_data[contexts[context_id].fetch_tail].icache_miss_l2 = contexts[context_id].my_last_inst_missed_in_l2;

								/* for pipe trace */
								//ptrace_newinst(contexts[context_id].fetch_data[contexts[context_id].fetch_tail].ptrace_seq,
								// inst, contexts[context_id].fetch_data[contexts[context_id].fetch_tail].regs_PC,
								// 0);
								//ptrace_newstage(contexts[context_id].fetch_data[contexts[context_id].fetch_tail].ptrace_seq,
								//		      PST_IFETCH,
								//		      ((contexts[context_id].last_inst_missed ? PEV_CACHEMISS : 0)
								//		       | (contexts[context_id].last_inst_tmissed ? PEV_TLBMISS : 0)));
								contexts[context_id].last_inst_missed = FALSE;
								contexts[context_id].last_inst_tmissed = FALSE;
								contexts[context_id].my_last_inst_missed = FALSE;
								contexts[context_id].my_last_inst_missed_in_l2 = FALSE;

								/* adjust instruction fetch queue */
								contexts[context_id].fetch_tail = (contexts[context_id].fetch_tail + 1) & (contexts[context_id].ruu_ifq_size - 1);
								contexts[context_id].fetch_num++;
				}
}
#if 0
/* default machine state accessor, used by DLite */
				static char *					/* err str, NULL for no err */
simoo_mstate_obj(FILE *stream,			/* output stream */
								char *cmd,			/* optional command string */
								struct regs_t *regs,		/* registers to access */
								struct mem_t *mem,		/* memory space to access */
								int context_id)
{
				if (!cmd || !strcmp(cmd, "help"))
								fprintf(stream,
																"mstate commands:\n"
																"\n"
																"    mstate help   - show all machine-specific commands (this list)\n"
																"    mstate stats  - dump all statistical variables\n"
																"    mstate res    - dump current functional unit resource states\n"
																"    mstate ruu    - dump contents of the register update unit\n"
																"    mstate lsq    - dump contents of the load/store queue\n"
																"    mstate eventq - dump contents of event queue\n"
																"    mstate readyq - dump contents of ready instruction queue\n"
																"    mstate cv     - dump contents of the register create vector\n"
																"    mstate rspec  - dump contents of speculative regs\n"
																"    mstate mspec  - dump contents of speculative memory\n"
																"    mstate fetch  - dump contents of fetch stage registers and fetch queue\n"
																"\n"
											 );
				else if (!strcmp(cmd, "stats"))
				{
								/* just dump intermediate stats */
								sim_print_stats(stream,context_id);
				}
				else if (!strcmp(cmd, "res"))
				{
								/* dump resource state */
								res_dump(contexts[context_id].fu_pool, stream);
				}
				else if (!strcmp(cmd, "ruu"))
				{
								/* dump RUU contents */
								ruu_dump(stream, context_id);
				}
				else if (!strcmp(cmd, "lsq"))
				{
								/* dump LSQ contents */
								lsq_dump(stream, context_id);
				}
				else if (!strcmp(cmd, "eventq"))
				{
								/* dump event queue contents */
								eventq_dump(stream,context_id);
				}
				else if (!strcmp(cmd, "readyq"))
				{
								/* dump event queue contents */
								readyq_dump(stream, context_id);
				}
				else if (!strcmp(cmd, "cv"))
				{
								/* dump event queue contents */
								cv_dump(stream, context_id);
				}
				else if (!strcmp(cmd, "rspec"))
				{
								/* dump event queue contents */
								rspec_dump(stream, context_id);
				}
				else if (!strcmp(cmd, "mspec"))
				{
								/* dump event queue contents */
								mspec_dump(stream, context_id);
				}
				else if (!strcmp(cmd, "fetch"))
				{
								/* dump event queue contents */
								fetch_dump(stream, context_id);
				}
				else
								return "unknown mstate command";

				/* no error */
				return NULL;
}
#endif
/* start simulation, program loaded, processor precise state initialized */
static void  sim_execute(int context_id)
{
				//int i=0;
				/* RUU/LSQ sanity checks */

				/* fprintf(stdout, "\n Current Context in sim execute %d \n",current_context); */

				if (contexts[context_id].RUU_num < contexts[context_id].LSQ_num)
								panic("RUU_num < LSQ_num");
				if (((contexts[context_id].RUU_head + contexts[context_id].RUU_num) % contexts[context_id].RUU_size) != contexts[context_id].RUU_tail)
								panic("RUU_head/RUU_tail wedged");
				if (((contexts[context_id].LSQ_head + contexts[context_id].LSQ_num) % contexts[context_id].LSQ_size) != contexts[context_id].LSQ_tail)
								panic("LSQ_head/LSQ_tail wedged");

				/* check if pipetracing is still active */
				//ptrace_check_active(contexts[context_id].regs.regs_PC, contexts[context_id].sim_num_insn, sim_cycle);

				/* indicate new cycle in pipetrace */

				/* commit entries from RUU/LSQ to architected register file */
				ruu_commit(context_id);


				/* service function unit release events */
				ruu_release_fu(context_id);

				/* ==> may have ready queue entries carried over from previous cycles */

				/* service result completions, also readies dependent operations */
				/* ==> inserts operations into ready queue --> register deps resolved */
				ruu_writeback(context_id);

				if (!bugcompat_mode)
				{
								/* try to locate memory operations that are ready to execute */
								/* ==> inserts operations into ready queue --> mem deps resolved */
								lsq_refresh(context_id);

								/* issue operations ready to execute from a previous cycle */
								/* <== drains ready queue <-- ready operations commence execution */
								ruu_issue(context_id);
				}

				/* decode and dispatch new operations */
				/* ==> insert ops w/ no deps or all regs ready --> reg deps resolved */
				ruu_dispatch(context_id);

				if (bugcompat_mode)
				{
								/* try to locate memory operations that are ready to execute */
								/* ==> inserts operations into ready queue --> mem deps resolved */
								lsq_refresh(context_id);

								/* issue operations ready to execute from a previous cycle */
								/* <== drains ready queue <-- ready operations commence execution */
								ruu_issue(context_id);
				}

				/* call instruction fetch unit if it is not blocked */
				if (!contexts[context_id].ruu_fetch_issue_delay)
								ruu_fetch(context_id);
				else
								contexts[context_id].ruu_fetch_issue_delay--;

				/* update buffer occupancy stats */
				contexts[context_id].IFQ_count += contexts[context_id].fetch_num;
				contexts[context_id].IFQ_fcount += ((contexts[context_id].fetch_num == contexts[context_id].ruu_ifq_size) ? 1 : 0);
				contexts[context_id].RUU_count += contexts[context_id].RUU_num;
				contexts[context_id].RUU_fcount += ((contexts[context_id].RUU_num == contexts[context_id].RUU_size) ? 1 : 0);
				contexts[context_id].LSQ_count += contexts[context_id].LSQ_num;
				contexts[context_id].LSQ_fcount += ((contexts[context_id].LSQ_num == contexts[context_id].LSQ_size) ? 1 : 0);

				/* sudiptac :::: Check whether the commit of the instruction was from
				 * effective code range. In that case increment the effective cycle 
				 * of the program running in this core by one */
				if(contexts[context_id].effect_commit) { 
								contexts[context_id].effect_cycles++;
#ifdef _PRINT
								fprintf(stdout, "Setting effective cycle of context %d = %u\n",
																context_id, contexts[context_id].effect_cycles);
#endif
				}  
}

static  void sim_fastfwd(int context_id){
				//printf("\n Entering sim_fastfwd \n");
				/* ignore any floating point exceptions, they may occur on mis-speculated execution paths */
				signal(SIGFPE, SIG_IGN);
				/* set up program entry state */
				contexts[context_id].regs.regs_PC = contexts[context_id].mem->ld_prog_entry;
				contexts[context_id].regs.regs_NPC = contexts[context_id].regs.regs_PC + sizeof(md_inst_t);
				if (dlite_check_break(contexts[context_id].regs.regs_PC, /* no access */0, /* addr */0, 0, 0))
								dlite_main(contexts[context_id].regs.regs_PC, contexts[context_id].regs.regs_PC + sizeof(md_inst_t),
																sim_cycle, &(contexts[context_id].regs), contexts[context_id].mem);
				if (contexts[context_id].fastfwd_count > 0){
								int icount;
								md_inst_t inst;			/* actual instruction bits */
								enum md_opcode op;		/* decoded opcode enum */
								md_addr_t target_PC;		/* actual next/target PC address */
								md_addr_t addr;			/* effective address, if load/store */
								int is_write;			/* store? */
								byte_t temp_byte = 0;		/* temp variable for spec mem access */
								half_t temp_half = 0;		/* " ditto " */
								word_t temp_word = 0;		/* " ditto " */
#ifdef HOST_HAS_QWORD
								qword_t temp_qword = 0;		/* " ditto " */
#endif /* HOST_HAS_QWORD */
								enum md_fault_type fault;

								fprintf(stderr, "sim: ** fast forwarding %d insts **\n", contexts[context_id].fastfwd_count);

								for (icount=0; icount < contexts[context_id].fastfwd_count; icount++){
												/* maintain $r0 semantics */

												contexts[context_id].regs.regs_R[MD_REG_ZERO] = 0;
#ifdef TARGET_ALPHA
												contexts[context_id].regs.regs_F.d[MD_REG_ZERO] = 0.0;
#endif /* TARGET_ALPHA */

												/* get the next instruction to execute */
												MD_FETCH_INST(inst, contexts[context_id].mem, contexts[context_id].regs.regs_PC);

												/* set default reference address */
												addr = 0; is_write = FALSE;

												/* set default fault - none */
												fault = md_fault_none;

												/* decode the instruction */
												MD_SET_OPCODE(op, inst);

												/* execute the instruction */
												switch (op) {
#define DEFINST(OP,MSK,NAME,OPFORM,RES,FLAGS,O1,O2,I1,I2,I3)		\
																case OP:							\
																											SYMCAT(OP,_IMPL);						\
																break;
#define DEFLINK(OP,MSK,NAME,MASK,SHIFT)					\
																case OP:							\
																											panic("attempted to execute a linking opcode");
#define CONNECT(OP)
#undef DECLARE_FAULT
#define DECLARE_FAULT(FAULT)						\
																{ fault = (FAULT); break; }
#include "machine.def"
																default:
																				panic("attempted to execute a bogus opcode");
												}

												if (fault != md_fault_none){
																fatal("fault (%d) detected @ 0x%08p", fault, contexts[context_id].regs.regs_PC);
												}

												/* update memory access stats */
												if (MD_OP_FLAGS(op) & F_MEM){
																if (MD_OP_FLAGS(op) & F_STORE){
																				is_write = TRUE;
																}	
												}

												/* check for DLite debugger entry condition */
												if (dlite_check_break(contexts[context_id].regs.regs_NPC,
																								is_write ? ACCESS_WRITE : ACCESS_READ,
																								addr, contexts[context_id].sim_num_insn, contexts[context_id].sim_num_insn)){
																dlite_main(contexts[context_id].regs.regs_PC, contexts[context_id].regs.regs_NPC, 
																								contexts[context_id].sim_num_insn, &(contexts[context_id].regs), contexts[context_id].mem);
												}		

												/* go to the next instruction */
												contexts[context_id].regs.regs_PC = contexts[context_id].regs.regs_NPC;
												contexts[context_id].regs.regs_NPC += sizeof(md_inst_t);
								}
				}
				printf("\n Exiting sim_fastfwd \n");
}

void sim_setiming(int context_id){
				/* set up timing simulation entry state */
				contexts[context_id].fetch_regs_PC = contexts[context_id].regs.regs_PC - sizeof(md_inst_t);
				contexts[context_id].fetch_pred_PC = contexts[context_id].regs.regs_PC;
				contexts[context_id].regs.regs_PC = contexts[context_id].regs.regs_PC - sizeof(md_inst_t);
}

/* start simulation, program loaded, processor precise state initialized */
void sim_main(void) {

				signal(SIGFPE, SIG_IGN);
				int exit_code;
				int id;

				for(current_context = 0; current_context < MAX_CMP; current_context++) {
								/* sudiptac : Disabling sim fast forward. Don't think it would affect 
								 * simulation result */
								//sim_fastfwd(current_context);
								sim_setiming(current_context);

								/* sudiptac : Load symbols for calculating effective code range */
								sym_loadsyms(contexts[current_context].mem->ld_prog_fname, TRUE, 
																contexts[current_context].mem); 
								printf("Program name = %s\n", contexts[current_context].mem->ld_prog_fname);	
								printf("Program start addr = 0x%x\n", contexts[current_context].mem->start_addr);	
								printf("Program end addr = 0x%x\n", contexts[current_context].mem->end_addr);	
				}	

				/* sudiptac: check whether any of the contexts has an associated high priority context, 
				 * if so, update the virtual to physical core mapping appropriately */
				update_virtual_to_physical_core_mapping();

				fprintf(stderr, "sim: ** starting performance simulation **\n");
				current_context=0;

				while(1) {
								sim_execute(current_context);	

								/*main simulator loop, NOTE: the pipe stages are traverse in reverse order
									to eliminate this/next state synchronization and relaxation problems */

								/* go to next cycle */

								/* TBD exit when all the CPU's reach their max number of instructions finish early? */
								if ((contexts[current_context].max_insts) && 
																(contexts[current_context].sim_num_insn >= contexts[current_context].max_insts) 
																&& (contexts[current_context].context_expired == 0)) {

												no_context_expired++;
												contexts[current_context].context_expired=1;
								}	

								/* sudiptac : moved the "setjmp" code here. Otherwise the simulation
								 * would have been ended if any of the context has been expired. But we 
								 * need to continue simulation as long as all of them are expired. The 
								 * simulation will end from this loop now */
								if ((exit_code = setjmp(sim_exit_buf)) != 0) {
												/* special handling as longjmp cannot pass 0 */
												no_context_expired++;
												contexts[current_context].context_expired=1;
												contexts[current_context].sim_cycles = sim_cycle;

												/* sudiptac : For debugging */
#ifdef _DEBUG
												fprintf(stdout, "Setting sim cycle = %d\n", contexts[current_context].sim_cycles);	
#endif
								}

								/**********************SHARED MEMORY LOGIC************************/
								if((contexts[current_context].shared_mem_latency) && (no_process_entered_shared_memory)) {
												(contexts[current_context].shared_mem_latency)--;
												if(contexts[current_context].shared_mem_latency <= 0) {
																contexts[current_context].shared_mem_latency=0;
																no_process_entered_shared_memory--;
												}

												if(no_process_entered_shared_memory <= 0){
																contexts[current_context].shared_mem_latency=0;
																no_process_entered_shared_memory=0;
												}
								}
								/******************** END SHARED MEMORY LOGIC ********************/

								/* sudiptac : Moved the code here, since if all contexts have been 
								 * expired, the following code to find next context will go to an 
								 * infinite loop */
								if(no_context_expired == MAX_CMP){
												printf("\n final return from sim_main \n");

												return;
								}	

								/* sudiptac : changed. If one context has finished execution then no need
								 * to repeat it. Find the next context which is not yet expired. If all 
								 * contexts have been expired, control cannot come here as it would have 
								 * been exited by the previous code */
								do { 
												current_context++;
												if(current_context > (MAX_CMP-1)){
																current_context=0;
																sim_cycle++;
												}
								} while(contexts[current_context].context_expired 
												|| virtual_to_physical_core_map[current_context] != current_context);

								/* check whether it is the preemption point, in that case load the preempting task */
								/* the preempted task will be stucked at the preemption point, therefore, as long 
								 * as the preempting task run, the following condition will be satisfied */
								if (contexts[current_context].preempting_task && \
												contexts[current_context].preemption_point == contexts[current_context].sim_num_insn) {
												int preempting_context = contexts[current_context].preempting_context;
												/* change the current context in case the preempting task is not finished 
												 * execution */
												if (!contexts[preempting_context].context_expired) {
																static char flag = 0;
																current_context = contexts[current_context].preempting_context;
																if (!flag) {
																				fprintf(stdout, "current task is preempted by %s.....\n", \
																								contexts[current_context].mem->ld_prog_fname); 
																				flag = 1;
																}
												} else 
																fprintf(stdout, "preempting task has expired.....\n");
								}


#ifdef _DEBUG
								fprintf(stdout, "Value of sim cycle = %d\n", sim_cycle);
#endif
				}
}
