#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "sharedbus.h"
#include "sim.h"

/* Global data from simulation main */
extern SHARED_BUS_TYPE g_shared_bus_type;
extern int max_cores;
extern int max_cores_sharing_il2;
extern int max_cores_sharing_dl2;
extern int fx_slot_length;

/* compute waiting time for flexray static segment */
static int compute_bus_delay_FLEXRAY_STATIC(int off_val, int latency)
{
		  int delay_elem;
		  int fx_interval;

		  fx_interval = NO_CORE_SHARE_L2 * fx_slot_length;

		  /* FLEXRAY twik */
		  if (off_val < 0)
					 delay_elem = -off_val;
		  else if(off_val == 0)	 
					 delay_elem = 0;
		  else
					 delay_elem = fx_interval - (off_val % fx_interval);

		  return delay_elem;
}

/* Compute waiting time for a memory request and a given deterministic TDMA schedule */
static int compute_bus_delay_RR_TDMA(int off_val, int latency)
{
		  int delay_elem;
		  int fx_interval;

		  fx_interval = NO_CORE_SHARE_L2 * fx_slot_length;
		  
		  if (off_val < 0)
					 delay_elem = -off_val;
		  else if(off_val + latency <= fx_slot_length)	 
					 delay_elem = 0;
		  else
					 delay_elem = fx_interval - (off_val % fx_interval);

		  return delay_elem;
}

/* Top level routine which actually computes the total time for a bus access to complete */
int compute_bus_delay(int off_val, int latency)
{
		  switch(g_shared_bus_type)
		  {
					 case NOBUS:
								return 0;
					 case RR_TDMA:
								return compute_bus_delay_RR_TDMA(off_val, latency);
					 case FLEXRAY_STATIC:
								return compute_bus_delay_FLEXRAY_STATIC(off_val, latency);
					 default:
								fprintf(stdout, "Shared bus type not supported\n");
								exit(-1);
		  };

		  /* make compiler happy */
		  return -1;
}
