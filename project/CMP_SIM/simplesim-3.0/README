Compiling the simulator:

	- Go inside directory CMP_SIM/simplesim-3.0 and build the simulator
	- make

If the compilation goes successful, it will produce a binary "sim-outorder" which is used for simulation.

checking the simulator:

Execute the following command from CMP_SIM/simplesim-3.0 directory to test the simulator:

cmd> ./sim-outorder -sconfig sim-test/exp_cache/processor.opt sim-test/exp_cache/cnt.arg sim-test/exp_cache/jfdcint.arg

If executing the above command produces valid simulation results at end, then you have successfully 
installed the multi core simulator.

processor_config:

processor_config describes the micro-architectural configurations. The micro-architectural configuration follow 
the same format as simplescalar except a few modifications introduced to handle multi-core specific features. 
A typical processor configuration file looks as follows (description shown beside after # sign):

-cache:il1 il1:16:32:2:l 	# 2-way associative, 1 KB L1 cache
-cache:dl1 none 		# perfect L1 data cache
-cache:il2 il2:32:32:4:l 	# 4-way associative, 4 KB L2 cache
-cache:il2lat 6			# L2 cache hit latency = 6 cycles 
-mem:lat 30 2 			# memory latency = 30 cycles
-bpred 2lev			# 2 level branch predictor
-fetch:ifqsize 4		# 4-entry instruction fetch queue
-issue:inorder true 		# inorder processor
-decode:width 2 		# 2-way superscalar
-issue:width 2 			# 2-way superscalar
-commit:width 2 		# 2-way superscalar
-ruu:size 8			# 8-entry reorder buffer 
-il2:share 2			# 2 cores share an L2 cache
-core:ncores 2			# total number of cores = 2
-bus:bustype 0			# TDMA round robin shared bus
-bus:slotlength 50		# bus slot length assigned to each core = 50 cycles

Note that except the last four paramters, all other parameters are identical to simplescalar. A more detailed 
description of the parameters can be found by running the sim-outorder without any input. 

Last four parameters are introduced for multi-core simulation are detailed as follows:

-core:ncores    

Total number of cores in the processor, default value is 1

-il2:share

Total number of cores sharing an L2 cache. Default value is 1. Therefore providing only "-core:ncores 2"  
does not mean that the two cores share an L2 cache. We need to additionally provide "-il2:share 2" (as 
shown in the example above). Some examples of using the above two arguments:
	
	-core:ncores 2 , -il2:share 1 : 2 cores with private L2 caches
	-core:ncores 2 , -il2:share 2 : 2 cores with shared L2 cache
	-core:ncores 4 , -il2:share 2 : 4 cores with a group of two cores sharing an L2 cache

-bus:bustype	

Default value is -1 which means a perfect shared bus and introduces zero bus delay for any possible bus 
transaction. 

If the value is 0 (as in the example), it resembles a round robin TDMA bus where a fixed length bus slot 
is assigned to each available core. 

-bus:slotlnegth

Only relevant if -bus:bustype is set to 0. Represents the bus slot length assigned to each core in round 
robin bus schedule.


Example processor configurations:

Numerous processor configuration examples are provided in directories chronos-multi-core/processor_config/exp_*. 
Those can be tried for running example programs.


Running the Simulator:

From the directory "CMP_SIM/simplesim-3.0", you can run the run the multicore simulator using the following command:

cmd> ./sim-outorder -sconfig <processor_config> <arg_file 1> <arg_file 2> .....

Above command runs the simulation of task provided by <arg_file 1> in core 0, task provided by <arg_file 1> in core 1 
and so on with some shared configurations given through <processor_config> (explained below in more detail).

Here <arg_file 1>.... represents the tasks running on a dedicated core and the configurations of the corresponding core. 
A typical arg file looks like this: 

../../benchmarks/cnt/cnt > sim-test/exp_cache/cnt.out
-issue:inorder true
-issue:wrongpath false
-bpred 2lev
-mem:width 32
-ruu:size 8
-fetch:ifqsize 4
-cache:il1 il1:16:32:2:l
-cache:il2 il2:32:32:4:l
-cache:il2 none
-cache:dl1 none
-cache:dl2 none
-cache:il2lat 6
-mem:lat 30 2
-decode:width 1
-issue:width 1
-commit:width 1
-dtlb none
-itlb none

The first line of the above arg file provides the benchmark name running on the core (i.e. ../../benchmarks/cnt/cnt). The 
benchmark name must point to the simplescalar PISA binary of the corresponding benchmark (check that path). The detailed 
output of the simulation will be redirected to the file "sim-test/exp_cache/cnt.out". The simulation also provide the 
output in standard output. 

Rest of the lines in arg file resembles the micro-architectural configuration of the core which has exactly similar format 
with single core simplescalar release. 

<processor_config> file provides (given with -sconfig option) the shared configurations among cores. Following is an 
example:

-core:ncores 2		# total number of cores
-bus:bustype 0		# TDMA round robin bus
-bus:slotlength 50	# bus slot length assigned to each core
-il2:share 2		# number of cores sharing an instruction L2 cache
-dl2:share 2		# number of cores sharing an data L2 cache


Example simulations:

Numerous examples have been provided in directory CMP_SIM/simplesim-3.0/sim-test/exp_*. Check out the "run_one" script 
in directories CMP_SIM/simplesim-3.0/sim-test/exp_* to get some sample commands for simulations.

Contact:

Please contact sudipta.chattopadhyay@liu.se for any feedback, comments or difficulties in running the tools.

