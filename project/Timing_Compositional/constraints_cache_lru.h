#ifndef __CONSTRAINTS_CACHE_LRU_H__
#define __CONSTRAINTS_CACHE_LRU_H__
#include "constraints_all.h"
#include "cache.h"

#define LRU_CACHE_FILE "localinput/shared_cache_lru.dat"

static void generate_symbolic_cache_conflict_variables(Z3_context& ctx, ast_list_t& order_var_list, cache& lru_cache);
static void generate_lru_cache_conflict_constraints(Z3_context& ctx, ast_list_t& order_var_list, cache& lru_cache, log_t& logv);
static void lru_cache_conflict_constraints_per_block(Z3_context& ctx, Z3_ast& order_entry, loc_t& tloc, \
	long& mem, long& cmem, cache& lru_cache, log_t& logv); 
static void generate_memory_latency_constraints(Z3_context& ctx, cache& lru_cache, log_t& logv);
#endif
