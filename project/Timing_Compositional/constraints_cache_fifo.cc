#include "constraints_cache_fifo.h"
#include "constraints_sound_approx.h"

/* symbolic variables capturing cache conflict count */
static std::map<ctriple_t, Z3_ast> psi_var_list;
/* locating cache conflicting accesses */
static std::map<ctriple_t, std::vector<Z3_ast> > order_conflict_list;
/* locating symbolic delay variables */
static std::map<ctriple_t, std::vector<Z3_ast> > delay_conflict_list;
/* locating memory blocks */
static std::map<long, std::vector<Z3_ast> > memory_map;
/* locating delay faced by memory blocks */
static std::map<long, std::vector<Z3_ast> > delay_map;
/* enumerating cache conflicting memory blocks for a particular location */
static std::map<loc_t, std::vector<long> > mem_conflict_list;
/* all shared cache accesses across all threads (loaded from file) */
static all_access_t all_shared_cache_access;
/* all shared cache accesses across all threads (loaded from file) */
static std::map<int, std::vector<int> > residual_age;
/* all symbolic delays for shared resource accesses */
static std::vector<Z3_ast> all_delay_vars;

/* number of shared variables */
extern int total_shared_accesses;

/* for filename and policy and threshold delay */
extern char* local_analysis_file;
extern char* global_policy;
extern char* global_threshold;

/* for overriding the cache */
extern int nset_in;
extern int nline_in;
extern int nassoc_in;
/* end */

void constraint_system_shared_cache_fifo()
{
	std::cerr << "\n.....collecting the set of shared cache accesses from all threads.....\n\n";
	
	for (unsigned tid = 0; tid < NUM_OF_THREADS; tid++) {
		access_list_t access_list;
		std::vector<int> res_age_list;
		fetch_all_accesses(tid, access_list, res_age_list);
		all_shared_cache_access[tid] = access_list;
		residual_age[tid] = res_age_list;
	}
	std::cerr << "\n.....total shared cache accesses = " << total_shared_accesses << "\n\n";
	std::cerr << ".....initialize cache configuration.....\n\n";
	/* if cache is not provided as input, use default 2KB cache */
	if (!nset_in) {
		nset_in = 16; 
		nline_in = 32;
		nassoc_in = 4;
	}
	/* default initialization end */
	cache fifo_cache(nset_in, nassoc_in, nline_in, FIFO_POLICY); /* 2 KB FIFO cache is default */
	/* cache initialization end */
	std::cerr << ".....finished initializing cache configuration.....\n\n"; 
	std::cerr << ".....start building all constraints for shared FIFO caches.....\n\n";

	Z3_context fifo_ctx;
	log_t fifo_log;

	fifo_ctx = mk_context();	

	/* create variables to capture the total order in thread interleaving pattern */
	/* create variables to capture all symbolic delay variables across all threads */
	ast_list_t order_var_list;
	ast_list_t delay_var_list;
	Z3_ast uni_miss; /* for cache misses found in local execution */
	for (unsigned tid = 0; tid < NUM_OF_THREADS; tid++) {
		access_t it;
		std::vector<Z3_ast> var_list;
		for (unsigned II = 0; II < all_shared_cache_access[tid].size(); II++) {
			it = all_shared_cache_access[tid][II];
			long seq = it.first;	
			std::string order("order");
			order += "_";
			i2string(order, tid);
			order += "_";
			i2string(order, seq);
			/* create one integer variable for each shared cache access location */
			Z3_ast order_z3 = mk_int_var(fifo_ctx, order.c_str());
			var_list.push_back(order_z3);
		}
		order_var_list[tid] = var_list;
		std::vector<Z3_ast> delay_list;
		bool first = true;
		for (unsigned II = 0; II < all_shared_cache_access[tid].size(); II++) {
			it = all_shared_cache_access[tid][II];
			long seq = it.first;
			std::string delay("delay");
			delay += "_";
			i2string(delay, tid);
			delay += "_";
			i2string(delay, seq);
			/* create symbolic delay variables only if it was not a cache miss in isolation */
			if (residual_age[tid][II] != -1) {
				/* create symbolic delay variable */
				Z3_ast delay_z3 = mk_int_var(fifo_ctx, delay.c_str());
				delay_list.push_back(delay_z3);
				/* record symbolic delay variables per cache set */
				int nset = fifo_cache.get_mapped_set(it.second);
				log_set_wise_symbolic_delay(nset, delay_z3);
			} else { /* otherwise create exactly one universal miss variable */
				if (first) {
					uni_miss = mk_int_var(fifo_ctx, "miss");
					/* generate always-miss constraint (exactly once) */
					Z3_ast MISS = mk_int(fifo_ctx, MISS_LATENCY);
					Z3_ast miss_cnstr = Z3_mk_eq(fifo_ctx, uni_miss, MISS);
					/* <universal cache miss constraint> assert constraint into the constraint store */
					Z3_assert_cnstr(fifo_ctx, miss_cnstr);
					fifo_log.push_back(miss_cnstr);
					/* log universal miss constraints to all set-wise constraint log */
					for (int nset = 0; nset < fifo_cache.get_nsets(); nset++)
						log_set_wise_cache_constraints(nset, miss_cnstr);
					first = false;
				}
				delay_list.push_back(uni_miss);	
			}
			/* here we ensure that every possible shared cache access is associated with a delay variable */
		}
		delay_var_list[tid] = delay_list;
	}
	
	/* generate program order constraints */
	generate_program_order_constraints(fifo_ctx, order_var_list, fifo_log);
	
	/* generate symbolic cache conflict variables and other relevant data structures */
	generate_symbolic_cache_conflict_variables(fifo_ctx, order_var_list, delay_var_list, fifo_cache);

	/* generate cache conflict constraints for FIFO shared caches */
	generate_fifo_cache_conflict_constraints(fifo_ctx, order_var_list, delay_var_list, fifo_cache, fifo_log);

	/* generate memory latency constraints */
	generate_memory_latency_constraints(fifo_ctx, delay_var_list, fifo_cache, fifo_log);

	/* first generate the SMT2 constraint files for optimizing performance objective */
	print_constraint_log_for_optimization(fifo_ctx, all_delay_vars, fifo_log);	
	
	/* generate performance bug predicate */
	generate_performance_bug_predicate(fifo_ctx, all_delay_vars, fifo_log);

	/* print constraints in SMT2 format for Z3 to solve */
	print_constraint_log(fifo_ctx, fifo_log);
	fprintf(stdout, "\n");
	
#if 0
	fprintf(stdout, "\n*****finding models for the constraint system (shared FIFO cache)*****\n\n");

	/* compute model for the constraint system */
	check(fifo_ctx, Z3_L_TRUE); 

	fprintf(stdout, "*****finished finding models for the constraint system (shared FIFO cache)*****\n\n");
#endif

	/* perform sound approximation */
	shared_cache_sound_approx(fifo_ctx, all_shared_cache_access, order_var_list, fifo_cache);

	/* delete context */
	Z3_del_context(fifo_ctx);
}

/* generate symbolic cache conflict variables and relevant data structures */
static void generate_symbolic_cache_conflict_variables(Z3_context& ctx, ast_list_t& order_var_list, 
	ast_list_t& delay_var_list, cache& fifo_cache) {
	
	for (unsigned ti = 0; ti < NUM_OF_THREADS; ti++) {
		for (unsigned sizeI = 0; sizeI < all_shared_cache_access[ti].size(); sizeI++) {
			access_t it = all_shared_cache_access[ti][sizeI];
			std::map<int, bool> visited;
			long seqI = it.first;
			long memI = it.second;
			/* mapping memory blocks to locations, can also handle shared data accesses */
			if (!memory_map.count(memI)) {
				std::vector<Z3_ast> temp, tempd;
				temp.push_back(order_var_list[ti][sizeI]);
				tempd.push_back(delay_var_list[ti][sizeI]);
				memory_map[memI] = temp;
				delay_map[memI] = tempd;
			} else {
				memory_map[memI].push_back(order_var_list[ti][sizeI]);
				delay_map[memI].push_back(delay_var_list[ti][sizeI]);
			}
			/* If this shared cache access was already a cache miss (implies that residual age is -1), 
			 * there is no need to generate symbolic variable for the same */
			if (residual_age[ti][sizeI] == -1) continue;
			for (unsigned tj = 0; tj < NUM_OF_THREADS; tj++) {
				for (unsigned sizeJ = 0; sizeJ < all_shared_cache_access[tj].size(); sizeJ++) {
					/* FIFO specific (we do not consider conflict from instructions later in the program order) */
					if (ti == tj && sizeJ >= sizeI) continue;
					access_t jt = all_shared_cache_access[tj][sizeJ];
					long seqJ = jt.first;
					long memJ = jt.second;
					if (!fifo_cache.is_conflicting(memI, memJ)) continue;
					/* this triple uniquely identifies a conflicting memory block at a particular thread 
					 * location */
					ctriple_t cloc(ti, seqI, memJ);
					/* log the conflicting memory block for this location (sizeI) in this thread (ti) */
					if (!order_conflict_list.count(cloc)) {
						std::vector<Z3_ast> temp, tempd;	
						temp.push_back(order_var_list[tj][sizeJ]);
						tempd.push_back(delay_var_list[tj][sizeJ]);
						order_conflict_list[cloc] = temp;
						delay_conflict_list[cloc] = tempd;
					}
					else {
						order_conflict_list[cloc].push_back(order_var_list[tj][sizeJ]);
						delay_conflict_list[cloc].push_back(delay_var_list[tj][sizeJ]);
					}
					/* create cache conflict variables only for unique memory blocks */
					if (visited.count(memJ)) continue;
					/* update cache conflict list */
					loc_t tloc(ti, seqI);
					/* FIFO specific */
					if (!mem_conflict_list.count(tloc)) {
						std::vector<long> temp; 
						temp.push_back(memJ);
						mem_conflict_list[tloc] = temp;
					} else
						mem_conflict_list[tloc].push_back(memJ);

					visited[memJ] = true;
					std::string psi_str("psi_");
					i2string(psi_str, ti);
					psi_str += "_";
					i2string(psi_str, seqI);
					psi_str += "_";
					i2string(psi_str, memJ); /* to handle conflicts from unique memory blocks */
					/* create a variable to symbolize the cache conflict count */
					Z3_ast psi_z3 = mk_int_var(ctx, psi_str.c_str());
					/* log the variable for solver */
					psi_var_list[cloc] = psi_z3;
				}
			}
		}
	}
}

/* generate cache conflict constraints for FIFO shared caches */
static void generate_fifo_cache_conflict_constraints(Z3_context& ctx, ast_list_t& order_var_list, 
	ast_list_t& delay_var_list, cache& fifo_cache, log_t& logv)
{
	static int conflict_constraints = 0;
	/* generate constraints for cache conflict count for each thread at 
	 * each shared resource access location */
	for (unsigned tid = 0; tid < NUM_OF_THREADS; tid++) {
		for (unsigned sizeI = 0; sizeI < all_shared_cache_access[tid].size(); sizeI++) {
			access_t entry = all_shared_cache_access[tid][sizeI];
			long seqI = entry.first;
			long memI = entry.second;
			loc_t tloc(tid, seqI);
			Z3_ast order_entry = order_var_list[tid][sizeI];
			/* If this shared cache access was already a cache miss (implies that residual age is -1), 
			 * there is no need to generate constraints for the same */
			if (residual_age[tid][sizeI] == -1) continue;
			/* this location does not face any inter-thread cache conflict, therefore, 
			 * we do not need to generate any constraint for this location */
			if (!mem_conflict_list.count(tloc)) continue;
			for (unsigned nconfl = 0; nconfl < mem_conflict_list[tloc].size(); nconfl++) {
				long cmem = mem_conflict_list[tloc][nconfl];
				fifo_cache_conflict_constraints_per_block(ctx, order_entry, tloc, memI, cmem, fifo_cache, logv);
				conflict_constraints++;
				/* put a threshold on conflict constraints (intermediate results are always sound) */
				if (conflict_constraints >= CONFLICT_CONSTRAINTS_THRESHOLD)
					return;
			}
		}
	}			
}

/* generate cache conflict constraints for one conflicting memory block (specified via the parameter) */
static void fifo_cache_conflict_constraints_per_block(Z3_context& ctx, Z3_ast& order_entry, loc_t& tloc, 
	long& mem, long& cmem, cache& fifo_cache, log_t& logv)
{
	ctriple_t cloc(tloc.first, tloc.second, cmem);

	/* get the set number */
	assert(fifo_cache.get_mapped_set(mem) == fifo_cache.get_mapped_set(cmem));
	int nset = fifo_cache.get_mapped_set(mem);

	/* these lists must not be empty */
	assert(order_conflict_list.count(cloc));	
	assert(psi_var_list.count(cloc));

	/* cache conflict symbolic variable for <order_entry, cmem> pair */
	Z3_ast psi_var = psi_var_list[cloc];
	std::vector<Z3_ast> no_conflict;
	/* Cache miss latency */
	Z3_ast MISS = mk_int(ctx, MISS_LATENCY);

	/* constraint formulation */
	for (unsigned sizeI = 0; sizeI < order_conflict_list[cloc].size(); sizeI++) {
		Z3_ast order_conflict = order_conflict_list[cloc][sizeI];
		/* conflicting memory block must be accessed before */
		Z3_ast order_compare = Z3_mk_lt(ctx, order_conflict, order_entry);
		/* conflicting memory block must have been a miss (FIFO specific) */
		Z3_ast delay_conflict = delay_conflict_list[cloc][sizeI];
		Z3_ast delay_miss = Z3_mk_eq(ctx, delay_conflict, MISS);
		/* and finally, the memory block must not be reloaded since the cache conflict is realized */
		std::vector<Z3_ast> no_reload;
		for (unsigned mapI = 0; mapI < memory_map[mem].size(); mapI++) {
			Z3_ast order_map = memory_map[mem][mapI];
			Z3_ast delay_map_elem = delay_map[mem][mapI];
			Z3_ast order_left = Z3_mk_lt(ctx, order_map, order_entry);
			Z3_ast order_right = Z3_mk_lt(ctx, order_conflict, order_map);
			/* FIFO specific: memory block must be reloaded and not just be accessed */
			Z3_ast delay_map_miss = Z3_mk_eq(ctx, delay_map_elem, MISS);
			Z3_ast args[3];
			args[0] = order_left; 
			args[1] = order_right;
			args[2] = delay_map_miss;
			Z3_ast reload_cnstr = Z3_mk_and(ctx, 3, args);
			Z3_ast no_reload_cnstr = Z3_mk_not(ctx, reload_cnstr);
			no_reload.push_back(no_reload_cnstr);
		}
		/* no_reload captures the constraint that no reload of the memory block has taken place */
		Z3_ast conflict_cnstr;
		if (no_reload.size() > 0) {
			Z3_ast all_no_reload_cnstr;
			Z3_ast* args = new Z3_ast[no_reload.size()];
			for (unsigned II = 0; II < no_reload.size(); II++)
				args[II] = no_reload[II];	
			all_no_reload_cnstr = Z3_mk_and(ctx, no_reload.size(), args);
			Z3_ast args2[3];
			args2[0] = order_compare;
			args2[1] = delay_miss;
			args2[2] = all_no_reload_cnstr;
			/* this contraint precisely captures the condition that cache conflict appeared and it has not 
		 	 * been deminished by any intermediate reloading of the memory block */
			conflict_cnstr = Z3_mk_and(ctx, 3, args2);
			delete args;	
		} else {
			Z3_ast args2[2];
			args2[0] = order_compare;
			args2[1] = delay_miss;
			conflict_cnstr = Z3_mk_and(ctx, 2, args2); 
		}
		/* final phase: formulate to link the preceding constraints with the cache conflict count */
		Z3_ast no_conflict_cnstr = Z3_mk_not(ctx, conflict_cnstr);
		Z3_ast one = mk_int(ctx, 1);
		/* positive conflict */
		Z3_ast conflict_ve = Z3_mk_eq(ctx, psi_var, one);
		Z3_ast args[2];
		args[0] = no_conflict_cnstr;
		args[1] = conflict_ve;
		Z3_ast overall_conflict_cnstr = Z3_mk_or(ctx, 2, args);
	
		/* <cache conflict constraints> assert this constraint into the constraint system and log the constraint */
		Z3_assert_cnstr(ctx, overall_conflict_cnstr);
		logv.push_back(overall_conflict_cnstr);
		/* log cache set wise constraints (for approximation) */
		log_set_wise_cache_constraints(nset, overall_conflict_cnstr);

		/* now formulate the zero conflict constraints */
		no_conflict.push_back(conflict_cnstr);
	}
	/* generate constraints which means that memory block "cmem" does not generate any cache conflict to the 
	 * location <order_entry> */
	Z3_ast* args = new Z3_ast[no_conflict.size()];
	for(unsigned II = 0; II < no_conflict.size(); II++)
		args[II] = no_conflict[II];
	Z3_ast overall_no_conflict = Z3_mk_or(ctx, no_conflict.size(), args);
	/* free up memory */
	delete args;
	Z3_ast zero = mk_int(ctx, 0);
	/* zero conflict */
	Z3_ast conflict_zero = Z3_mk_eq(ctx, psi_var, zero);
	Z3_ast args2[2];
	args2[0] = overall_no_conflict;
	args2[1] = conflict_zero;
	Z3_ast overall_no_conflict_cnstr = Z3_mk_or(ctx, 2, args2);
	
	/* <cache conflict constraint> assert this constraint into the constraint store */
	Z3_assert_cnstr(ctx, overall_no_conflict_cnstr);
	logv.push_back(overall_no_conflict_cnstr);
	/* log cache set wise constraints (for approximation) */
	log_set_wise_cache_constraints(nset, overall_no_conflict_cnstr);
}

/* generate constraints to compute cache hit/miss latencies */
static void generate_memory_latency_constraints(Z3_context& ctx, ast_list_t& delay_var_list, cache& fifo_cache, log_t& logv)
{
	Z3_ast MISS = mk_int(ctx, MISS_LATENCY);
	Z3_ast HIT = mk_int(ctx, HIT_LATENCY);

	for (unsigned tid = 0; tid < NUM_OF_THREADS; tid++) {
		for (unsigned sizeI = 0; sizeI < all_shared_cache_access[tid].size(); sizeI++) {
			access_t acc = all_shared_cache_access[tid][sizeI];
			long seqI = acc.first;
			long memI = acc.second; 
			loc_t tloc(tid, seqI);
			/* If this shared cache access was already a cache miss (implies that residual age is -1), 
			 * there is no need to generate constraints for the same */
			if (residual_age[tid][sizeI] == -1) continue;
			/* if this instruction does not face conflict, there is no need to generate constraints */
			if (!mem_conflict_list.count(tloc)) continue;
			/* get the symbolic variable that capture the shared resource access delay */
			all_delay_vars.push_back(delay_var_list[tid][sizeI]);
			/* generate constraints on delay_var */
			std::vector<Z3_ast> psi_vars;
			for (unsigned nconfl = 0; nconfl < mem_conflict_list[tloc].size(); nconfl++) {
				long cmem = mem_conflict_list[tloc][nconfl];
				ctriple_t cloc(tid, seqI, cmem);
				psi_vars.push_back(psi_var_list[cloc]);	
			}
			Z3_ast* args = new Z3_ast[psi_vars.size()];
			for (unsigned II = 0; II < psi_vars.size(); II++)
				args[II] = psi_vars[II];
			Z3_ast add_conflict = Z3_mk_add(ctx, psi_vars.size(), args);
			/* condition to be evicted from the shared cache */
			Z3_ast delay_var = delay_var_list[tid][sizeI];
			Z3_ast MAX_RES = mk_int(ctx, fifo_cache.get_nassoc() - 1);
			Z3_ast cond_conflict = Z3_mk_gt(ctx, add_conflict, MAX_RES);
			Z3_ast not_cond_conflict = Z3_mk_not(ctx, cond_conflict);
			Z3_ast miss_latency = Z3_mk_eq(ctx, delay_var, MISS); 
			Z3_ast hit_latency = Z3_mk_eq(ctx, delay_var, HIT); 
			Z3_ast args2[2];
			args2[0] = not_cond_conflict;
			args2[1] = miss_latency;
			/* condition to capture shared cache miss latency */	
			Z3_ast miss_latency_cond = Z3_mk_or(ctx, 2, args2);
			args2[0] = cond_conflict;
			args2[1] = hit_latency;
			/* condition to capture shared cache hit latency */	
			Z3_ast hit_latency_cond = Z3_mk_or(ctx, 2, args2);
			/* <memory latency constraints> assert hit/miss latency constraints into the constraint system */
			Z3_assert_cnstr(ctx, miss_latency_cond);
			Z3_assert_cnstr(ctx, hit_latency_cond);
			/* log hit/miss latency constraints */
			logv.push_back(miss_latency_cond);
			logv.push_back(hit_latency_cond);
			/* log cache set wise constraints (for approximation) */
			int nset = fifo_cache.get_mapped_set(memI);
			log_set_wise_cache_constraints(nset, miss_latency_cond);
			log_set_wise_cache_constraints(nset, hit_latency_cond);
		}	
	}
}

