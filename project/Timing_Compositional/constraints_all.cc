#include "constraints_all.h"

/* global context */
Z3_context example_ctx;
/* for logging all constraints */
std::vector<Z3_ast> example_constraint_log;
/* used for all instantiations (local analysis file and arbitration/replacement policy) */
char* local_analysis_file = 0;
char* global_policy = 0;
char* global_threshold = NULL;
/* cache related parameters: sets, lines and associativity */
/* this eventually overrides default parameters in the cache class */
int nset_in = 0, nline_in = 0, nassoc_in = 0;

/* count all shared resource accesses */
int total_shared_accesses = 0;

/* set-wise cache constraint log */
set_log_t cache_set_log;
set_log_t symbolic_delay_per_set_log;

/* some utilities */
void i2string(std::string& str, int id) 
{
	std::ostringstream ss;
    ss << id;
	str += ss.str();
}

/* implements constraints for a simple example */
/************************************************/
/* Thread 1: 		Thread 2: */
/* [s] captures a shared resource access and [ns] */
/* captures an access to a private resource */
/************************************************/
/* load [ns]		store [ns] */ 
/* store [s]		load [ns] */
/* load [ns]		load [s] */
/* store [s]		load [s] */

/* This code is only for example purpose and not necessarily to be 
 * used to build the system . Therefore, in the following, we shall 
 * find many duplication of similar code patterns */

void running_example()
{
	fprintf(stdout, "\n.....computing constraints for the running example.....\n\n");
	
	example_ctx = mk_context();

	/* variables for total order */
	Z3_ast order_t1_c2, order_t1_c4;
	Z3_ast order_t2_c3, order_t2_c4;

	order_t1_c2 = mk_int_var(example_ctx, "order_t1_c2");	
	order_t1_c4 = mk_int_var(example_ctx, "order_t1_c4");	
	order_t2_c3 = mk_int_var(example_ctx, "order_t2_c3");	
	order_t2_c4 = mk_int_var(example_ctx, "order_t2_c4");	
	
	/* variables for cache conflict count in thread 1 */
	Z3_ast psi_t2c3_t1c2, psi_t2c3_t1c4;
	Z3_ast psi_t2c4_t1c2, psi_t2c4_t1c4;

	psi_t2c3_t1c2 = mk_int_var(example_ctx, "psi_t2c3_t1c2");
	psi_t2c3_t1c4 = mk_int_var(example_ctx, "psi_t2c3_t1c4");
	psi_t2c4_t1c2 = mk_int_var(example_ctx, "psi_t2c4_t1c2");
	psi_t2c4_t1c4 = mk_int_var(example_ctx, "psi_t2c4_t1c4");

	/* variables for cache conflict count in thread 2 */
	Z3_ast psi_t1c2_t2c3, psi_t1c2_t2c4;
	Z3_ast psi_t1c4_t2c3, psi_t1c4_t2c4;
		
	psi_t1c2_t2c3 = mk_int_var(example_ctx, "psi_t1c2_t2c3");
	psi_t1c2_t2c4 = mk_int_var(example_ctx, "psi_t1c2_t2c4");
	psi_t1c4_t2c3 = mk_int_var(example_ctx, "psi_t1c4_t2c3");
	psi_t1c4_t2c4 = mk_int_var(example_ctx, "psi_t1c4_t2c4");

	/* create constraints to compute cache conflict in thread 1 */
	example_cache_conflict_thread(order_t2_c3, order_t1_c2, psi_t2c3_t1c2);
	example_cache_conflict_thread(order_t2_c3, order_t1_c4, psi_t2c3_t1c4);
	example_cache_conflict_thread(order_t2_c4, order_t1_c2, psi_t2c4_t1c2);
	example_cache_conflict_thread(order_t2_c4, order_t1_c4, psi_t2c4_t1c4);

	/* create constraints to compute cache conflict in thread 2 */
	example_cache_conflict_thread(order_t1_c2, order_t2_c3, psi_t1c2_t2c3);
	example_cache_conflict_thread(order_t1_c2, order_t2_c4, psi_t1c2_t2c4);
	example_cache_conflict_thread(order_t1_c4, order_t2_c3, psi_t1c4_t2c3);
	example_cache_conflict_thread(order_t1_c4, order_t2_c4, psi_t1c4_t2c4);

	/* inequality constraints on the total order */
	example_inequality_constraints(order_t1_c2, order_t1_c4, order_t2_c3, order_t2_c4);
	
	/* constraints to capture program orders */
	example_program_order_constraints(order_t1_c2, order_t1_c4, order_t2_c3, order_t2_c4);
	
	/* variables and constraints to generate memory latencies */
	Z3_ast delay_t1_c2, delay_t1_c4;
	Z3_ast delay_t2_c3, delay_t2_c4;

	/* residual ages are hardcoded as the constant "1" */
	delay_t1_c2 = mk_int_var(example_ctx, "delay_t1_c2");
	delay_t1_c4 = mk_int_var(example_ctx, "delay_t1_c4");
	delay_t2_c3 = mk_int_var(example_ctx, "delay_t2_c3");
	delay_t2_c4 = mk_int_var(example_ctx, "delay_t2_c4");
	
	/* constraints to capture cache hit/miss delay in thread 1*/
	example_memory_latency_constraints(psi_t1c2_t2c3, psi_t1c2_t2c4, delay_t1_c2);
	example_memory_latency_constraints(psi_t1c4_t2c3, psi_t1c4_t2c4, delay_t1_c4);
	
	/* constraints to capture cache hit/miss delay in thread 2*/
	example_memory_latency_constraints(psi_t2c3_t1c2, psi_t2c3_t1c4, delay_t2_c3);
	example_memory_latency_constraints(psi_t2c4_t1c2, psi_t2c4_t1c4, delay_t2_c4);
	
	/* encode bug predicate (sample performance bug: exceeding threshold in shared cache latency) */
	example_encode_performance_bug_predicate(delay_t1_c2, delay_t1_c4, delay_t2_c3, delay_t2_c4);

	/* for debugging */	
#ifdef _DEBUG
	//display_ast(example_ctx, stdout, conflict_t2c3_t1c4);
	print_example_constraint_log();
	printf("\n");
#endif

	fprintf(stdout, "\n*****finding models for the constraint system*****\n\n");

	/* compute model for the constraint system */
	check(example_ctx, Z3_L_TRUE);

	fprintf(stdout, "*****finished finding models for the constraint system (end of one debugging step)*****\n\n");

	/* delete context */
	Z3_del_context(example_ctx);

	fprintf(stdout, "*****finished running the example*****\n\n");
}

/* create cache conflict constraints */
void example_cache_conflict_thread(Z3_ast& order_t1_cx, Z3_ast& order_t2_cy, Z3_ast& psi_t1cx_t2cy) 
{
	/* make constants */
	Z3_ast zero = mk_int(example_ctx, 0);
	Z3_ast one = mk_int(example_ctx, 1);
	
	Z3_ast c_t1_order11 = Z3_mk_lt(example_ctx, order_t1_cx, order_t2_cy);
	Z3_ast c_t1_order12 = Z3_mk_lt(example_ctx, order_t2_cy, order_t1_cx);

	Z3_ast example_ctx_11 = Z3_mk_not(example_ctx, c_t1_order11);
	Z3_ast example_ctx_12 = Z3_mk_not(example_ctx, c_t1_order12);

	/* cache conflict value */
	Z3_ast example_ctx_value_11 = Z3_mk_eq(example_ctx, psi_t1cx_t2cy, one);
	Z3_ast example_ctx_value_12 = Z3_mk_eq(example_ctx, psi_t1cx_t2cy, zero);

	Z3_ast args[2];

	args[0] = example_ctx_11;
	args[1] = example_ctx_value_11;
	
	Z3_ast constraint_11 = Z3_mk_or(example_ctx, 2, args);

	args[0] = example_ctx_12;
	args[1] = example_ctx_value_12;

	Z3_ast constraint_12 = Z3_mk_or(example_ctx, 2, args);

	args[0] = constraint_11;
	args[1] = constraint_12;

	Z3_ast conflict_t1cx_t2cy = Z3_mk_and(example_ctx, 2, args);

	/* <cnstr: cache conflict constraints> assert the constraint into the constraint store */
	Z3_assert_cnstr(example_ctx, conflict_t1cx_t2cy);
	example_constraint_log.push_back(conflict_t1cx_t2cy);
}

/* inequality constraints on total order */
void example_inequality_constraints(Z3_ast& order_t1_c2, Z3_ast& order_t1_c4, Z3_ast& order_t2_c3, Z3_ast& order_t2_c4)
{
	Z3_ast eq_1 = Z3_mk_eq(example_ctx, order_t1_c2, order_t2_c3);
	Z3_ast ineq_1 = Z3_mk_not(example_ctx, eq_1);
	Z3_ast eq_2 = Z3_mk_eq(example_ctx, order_t1_c2, order_t2_c4);
	Z3_ast ineq_2 = Z3_mk_not(example_ctx, eq_2);
	Z3_ast eq_3 = Z3_mk_eq(example_ctx, order_t1_c4, order_t2_c3);
	Z3_ast ineq_3 = Z3_mk_not(example_ctx, eq_3);
	Z3_ast eq_4 = Z3_mk_eq(example_ctx, order_t1_c4, order_t2_c4);
	Z3_ast ineq_4 = Z3_mk_not(example_ctx, eq_4);
	
	/* <cnstr: inequality constraints on total order> assert inequality constraints */
	Z3_assert_cnstr(example_ctx, ineq_1);
	Z3_assert_cnstr(example_ctx, ineq_2);
	Z3_assert_cnstr(example_ctx, ineq_3);
	Z3_assert_cnstr(example_ctx, ineq_4);
	example_constraint_log.push_back(ineq_1);
	example_constraint_log.push_back(ineq_2);
	example_constraint_log.push_back(ineq_3);
	example_constraint_log.push_back(ineq_4);
}

/* program order constraints on total order */
void example_program_order_constraints(Z3_ast& order_t1_c2, Z3_ast& order_t1_c4, Z3_ast& order_t2_c3, Z3_ast& order_t2_c4)
{
	Z3_ast prog_order_1 = Z3_mk_lt(example_ctx, order_t1_c2, order_t1_c4);
	Z3_ast prog_order_2 = Z3_mk_lt(example_ctx, order_t2_c3, order_t2_c4);

	/* <cnstr: program order constraints> assert program order constraints */
	Z3_assert_cnstr(example_ctx, prog_order_1);
	Z3_assert_cnstr(example_ctx, prog_order_2);
	example_constraint_log.push_back(prog_order_1);
	example_constraint_log.push_back(prog_order_2);
}

/* constraints to capture memory latency */
void example_memory_latency_constraints(Z3_ast& psi_t1cx_t2cy, Z3_ast& psi_t1cx_t2cz, Z3_ast& delay)
{
	Z3_ast HIT = mk_int(example_ctx, HIT_LATENCY);
	Z3_ast MISS = mk_int(example_ctx, MISS_LATENCY);

	Z3_ast args[2];

	args[0] = psi_t1cx_t2cy;
	args[1] = psi_t1cx_t2cz;

	Z3_ast one = mk_int(example_ctx, 1);

	Z3_ast all_conflict_1 = Z3_mk_add(example_ctx, 2, args);
	Z3_ast cond_1 = Z3_mk_le(example_ctx, all_conflict_1, one);
	Z3_ast not_cond_1 = Z3_mk_not(example_ctx, cond_1);
	Z3_ast latency_miss_1 = Z3_mk_eq(example_ctx, delay, MISS);
	Z3_ast latency_hit_1 = Z3_mk_eq(example_ctx, delay, HIT);

	args[0] = not_cond_1;
	args[1] = latency_miss_1;

	Z3_ast latency_cnstr_1 = Z3_mk_or(example_ctx, 2, args);
	
	args[0] = cond_1;
	args[1] = latency_hit_1;
	
	Z3_ast latency_cnstr_2 = Z3_mk_or(example_ctx, 2, args);

	/* <cnstr: cache hit/miss latency constraints> assert the latency constraints */
	Z3_assert_cnstr(example_ctx, latency_cnstr_1);
	Z3_assert_cnstr(example_ctx, latency_cnstr_2);
	example_constraint_log.push_back(latency_cnstr_1);
	example_constraint_log.push_back(latency_cnstr_2);
}

/* encode the performance bug as a predicate */
void example_encode_performance_bug_predicate(Z3_ast& delay_t1_c2, Z3_ast& delay_t1_c4, 
	Z3_ast& delay_t2_c3, Z3_ast& delay_t2_c4)
{
	Z3_ast args[4];
	Z3_ast threshold = mk_int(example_ctx, 2 * MISS_LATENCY);

	args[0] = delay_t1_c2;
	args[1] = delay_t1_c4;
	args[2] = delay_t2_c3;
	args[3] = delay_t2_c4;

	Z3_ast all_delay = Z3_mk_add(example_ctx, 4, args);
	Z3_ast performance_bug_predicate = Z3_mk_ge(example_ctx, all_delay, threshold);

	/* <cnstr: performance bug predicate> assert the bug predicate into the constraint system */
	Z3_assert_cnstr(example_ctx, performance_bug_predicate);
	example_constraint_log.push_back(performance_bug_predicate);
}

/* print all constraints */
void print_example_constraint_log() 
{
	std::vector<Z3_ast>::iterator II = example_constraint_log.begin();
	
	std::cerr << "\n******printing all constraints*****\n";	
	for(; II != example_constraint_log.end(); II++) {
		std::cerr << "\n";
		display_ast(example_ctx, stdout, *II);
		std::cerr << "\n";
	}
	std::cerr << "\n( Total number of constraints in the example = " << example_constraint_log.size() << " )\n";
	std::cerr << "\n******end printing all constraints*****\n";	
}	

/* constraint system for compositional performance debugging */
/* This is a generalized constraint system for performance debugging. This will be 
 * used for debugging, whether for a single input, a single path (symbolically 
 * executed) or for all inputs (to find worst-case execution time) */
void global_constraint_system(char* policy, char* filename, char* threshold, char* cache)
{
	/* four different instantiations of the constraint system */
	local_analysis_file = filename;
	global_policy = policy;
	global_threshold = threshold;
	
	/* read the cache parameters, if provided */
	if (cache) {
		sscanf(cache, "%d:%d:%d", &nset_in, &nline_in, &nassoc_in);
		std::cerr << "\nreading the cache=>";
		std::cerr << nset_in << ":" << nline_in << ":" << nassoc_in << "\n";
	}

	if(strcasecmp(policy, "LRUC") == 0)
		/* shared cache with LRU replacement policy */
		constraint_system_shared_cache_lru();
	else if (strcasecmp(policy, "PRIORITYB") == 0)
		/* shared resource with priority-based arbitration policy */
		constraint_system_shared_arbitration_priority();
	else if (strcasecmp(policy, "FIFOC") == 0)
		/* shared cache with FIFO replacement policy */
		constraint_system_shared_cache_fifo();
	else if (strcasecmp(policy, "FIFOB") == 0)
		/* shared resource with FIFO arbitration policy */
		constraint_system_shared_arbitration_fifo();
	else 
		assert(0 && "Unknown policy, exiting.....\n");
}

/* generate program order constraints generically */
/* These constraints are common for all instantiations */
void generate_program_order_constraints(Z3_context& ctx, ast_list_t& order_var_list, log_t& logv)
{
	for (unsigned tid = 0; tid < NUM_OF_THREADS; tid++) {
		for (unsigned size = 0; size < order_var_list[tid].size() - 1; size++) {
			Z3_ast prog_order = Z3_mk_lt(ctx, order_var_list[tid][size], order_var_list[tid][size + 1]);
			/* assert program order constraints */
			Z3_assert_cnstr(ctx, prog_order);
			/* log program order constraints */
			logv.push_back(prog_order);
		}
	}
}

/* generate inequality constraints on the total order */
/* These constraints are common for all instantiations */
void generate_order_inequality_constraints(Z3_context& ctx, ast_list_t& order_var_list, log_t& logv) 
{
	for (unsigned ti = 0; ti < NUM_OF_THREADS; ti++) {
		for (unsigned sizeI = 0; sizeI < order_var_list[ti].size(); sizeI++) {
			for (unsigned tj = ti+1; tj < NUM_OF_THREADS; tj++) {
				for (unsigned sizeJ = 0; sizeJ < order_var_list[tj].size(); sizeJ++) {
					Z3_ast eq_cnstr = Z3_mk_eq(ctx, order_var_list[ti][sizeI], order_var_list[tj][sizeJ]);
					Z3_ast ineq_cnstr = Z3_mk_not(ctx, eq_cnstr);
					/* assert the inequality constraint */
					Z3_assert_cnstr(ctx, ineq_cnstr);
					/* log inequality constraints */
					logv.push_back(ineq_cnstr);
				}
			}
		}
	}
}

/* print constraint log */
void print_constraint_log(Z3_context& ctx, log_t& logv) {
	log_t::iterator II = logv.begin();

#ifdef _NDEBUG	
	std::cerr << "\n******printing all constraints*****\n";	
	for(; II != logv.end(); II++) {
		fprintf(stdout, "\n");
		display_ast(ctx, stdout, *II);
		std::cerr << "\n";
	}
	std::cerr << "\n******end printing all constraints*****\n";	
#endif
	std::cerr << "\n( Total number of constraints in the system model = " << logv.size() << " )\n";

	/* print an SMT2 file for further processing */
	Z3_ast* args = new Z3_ast[logv.size()];
	for (unsigned sizeI = 0; sizeI < logv.size(); sizeI++)
		args[sizeI] = logv[sizeI];
	Z3_ast all_constraints = Z3_mk_and(ctx, logv.size(), args);
	/* cleanup memory */
	delete args;
#if 0
	Z3_ast tx = mk_int_var(ctx, "suf");
	Z3_ast rc = mk_int_var(ctx, "k1");
	Z3_ast obj = Z3_mk_lt(ctx, tx, rc);
	Z3_ast finalc = Z3_mk_implies(ctx, all_constraints, obj);
#endif
	Z3_set_ast_print_mode(ctx, Z3_PRINT_SMTLIB2_COMPLIANT);
	char const* smt_constraints = Z3_benchmark_to_smtlib_string(ctx, "cache_constraints", 0, 0, 0, 0, 0, all_constraints);
	//char const* smt_constraints = Z3_benchmark_to_smtlib_string(ctx, "cache_constraints", 0, 0, 0, 0, 0, finalc);
	std::ofstream smtfile;
	std::string filename(local_analysis_file);
	filename += ".smt2";
	smtfile.open(filename.c_str(), std::ofstream::out);
	//std::cerr << smt_constraints << "\n";
	smtfile << smt_constraints << "\n";
	smtfile.close();
	/* we are done printing and logging */
}

/* print constraint log for optimization objective */
void print_constraint_log_for_optimization(Z3_context& ctx, std::vector<Z3_ast>& all_delay_vars, log_t& logv) {
	log_t::iterator II = logv.begin();

	std::cerr << "\n( Total number of constraints in the cache model objective = " << logv.size() << " )\n";

	/* print an SMT2 file for further processing */
	Z3_ast* args = new Z3_ast[logv.size()];
	for (unsigned sizeI = 0; sizeI < logv.size(); sizeI++)
		args[sizeI] = logv[sizeI];
	Z3_ast all_constraints = Z3_mk_and(ctx, logv.size(), args);
	/* cleanup memory */
	delete args;

	Z3_ast* args_add = new Z3_ast[all_delay_vars.size()];
	for (unsigned sizeI = 0; sizeI < all_delay_vars.size(); sizeI++)
		args_add[sizeI] = all_delay_vars[sizeI];
	Z3_ast objective = Z3_mk_add(ctx, all_delay_vars.size(), args_add);
	Z3_ast symbolic_bound = mk_int_var(ctx, "wct_k1");
	Z3_ast consequent = Z3_mk_lt(ctx, objective, symbolic_bound);
	/* antecedent = all constraint and consequent = performance objective */
	Z3_ast optf = Z3_mk_implies(ctx, all_constraints, consequent);
	
	Z3_set_ast_print_mode(ctx, Z3_PRINT_SMTLIB2_COMPLIANT);
	char const* smt_constraints = Z3_benchmark_to_smtlib_string(ctx, "cache_constraints_opt", 0, 0, 0, 0, 0, optf);
	std::ofstream smtfile;
	std::string filename(local_analysis_file);
	filename += ".smt2.opt";
	smtfile.open(filename.c_str(), std::ofstream::out);
	//std::cerr << smt_constraints << "\n";
	smtfile << smt_constraints << "\n";
	smtfile.close();
	/* we are done printing and logging */
}

/* read all shared cache accesses for manipulation (common for all cache replacement policies) */
void fetch_all_accesses(unsigned tid, access_list_t& access_list, std::vector<int>& res_age_list)
{
	/* read from file to memory */	
	std::ifstream shfile;

	/* start computing filename (dirty string business) */
	std::string filename(local_analysis_file);
	filename += ".";
	i2string(filename, tid);
	/* end computing filename */
	
	shfile.open(filename.c_str(), std::ifstream::in);

	std::cerr << ".....reading accesses from a file " << filename.c_str() << ".....\n\n";

	while(!shfile.eof()) {
		long seq, mem;
		int res;
		shfile >> seq; /* sequence of shared resource access */
		/* guard to check invalid end */
		if (seq < 0) break;
		shfile >> mem; /* accessed memory block */	
		shfile >> res; /* residual age of the memory block in the shared cache */
		/* bring the content into memory */
		access_t item(seq,mem);
		access_list.push_back(item);
		/* record the residual age for constraint formulation */
		res_age_list.push_back(res);
#ifdef _DEBUG
		std::cerr << "reading sequence = " << seq << ", memory address = " << mem;
		std::cerr << ", residual age = " << res << "\n";
#endif
		total_shared_accesses++;
	}

	shfile.close();

	std::cerr << "\n.....end reading accesses from a file.....\n\n";
}

/* encode a predicate to capture performance bug */
/* several predicates/objective functions can be encoded */
/* currently, we encode to check whether the overall shared resource latency 
 * might exceed a pre-defined threshold */
void generate_performance_bug_predicate(Z3_context& ctx, std::vector<Z3_ast>& all_delay_vars, log_t& logv)
{
	/* sanity check */
	if(all_delay_vars.empty())
		return;

#ifdef _DEBUG
	std::cerr << "Number of symbolic delay variables = " << all_delay_vars.size() << "\n";
#endif

	std::vector<Z3_ast>::iterator II = all_delay_vars.begin();
	Z3_ast* args = new Z3_ast[all_delay_vars.size()];
	unsigned ti = 0;

	for (; II != all_delay_vars.end(); II++) {
		args[ti] = *II;
		ti++;
	}		
	Z3_ast all_shared_cache_delay = Z3_mk_add(ctx, all_delay_vars.size(), args);
	delete args;
	/* if no threshold delay is provided, use the default value */
	Z3_ast THRESHOLD_CONS;
	if (!global_threshold)
		THRESHOLD_CONS = mk_int(ctx, THRESHOLD);
	else
		THRESHOLD_CONS = mk_int(ctx, atoi(global_threshold));
	Z3_ast performance_bug_cnstr = Z3_mk_ge(ctx, all_shared_cache_delay, THRESHOLD_CONS);
	/* <performance bug constraint> assert the performance bug constraint and log the same */
	Z3_assert_cnstr(ctx, performance_bug_cnstr); 
	logv.push_back(performance_bug_cnstr);
}

/* (for sound over-approximation) log cache-set-wise constraints */
void log_set_wise_cache_constraints(int nset, Z3_ast& cnstr)
{
	if(!cache_set_log.count(nset)) {
		log_t tlog;
		tlog.push_back(cnstr);
		cache_set_log[nset] = tlog;
	} else
		cache_set_log[nset].push_back(cnstr);
}

/* (for sound over-approximation) log cache-set-wise symbolic delay */
void log_set_wise_symbolic_delay(int nset, Z3_ast& s_delay)
{
	if(!cache_set_log.count(nset)) {
		log_t tlog;
		tlog.push_back(s_delay);
		symbolic_delay_per_set_log[nset] = tlog;
	} else
		symbolic_delay_per_set_log[nset].push_back(s_delay);
}


