#include <stdio.h>
#include <stdlib.h>
#include "constraints_all.h"

int main(int argc, char* argv[])
{
	/* sanity check */
	if (argc < 3) {
		fprintf(stdout, "Format: <timing> <LRUC/FIFOC/FIFOB/PRIORITYB> <local analysis dump> [Threshhold] [cache]\n");
		fprintf(stdout, "Exiting the process.....\n");
		exit(-1);
	}


	/* prepares constrants for a simple example and run in Z3 solver */
	running_example();

	/* build the constraint system <policy, local analysis file> */
	if (argc < 4)
		global_constraint_system(argv[1], argv[2], NULL, NULL);
	else if (argc < 5)
		global_constraint_system(argv[1], argv[2], argv[3], NULL);
	else
		global_constraint_system(argv[1], argv[2], argv[3], argv[4]);	
}
