#include "constraints_sound_approx.h"

/* set-wise constraint log and symbolic delay */
extern set_log_t cache_set_log;
extern set_log_t symbolic_delay_per_set_log;
/* for filename and policy and threshold delay */
extern char* local_analysis_file;

/* set-wise performance objective function */
std::map<int, Z3_ast> performance_obj;

/* first compute a set-wise partition of constraints, note that 
 * different cache sets are independent and their constraints 
 * can be solved independently. However, the solutions of 
 * different cache sets, if put together, might be infeasible, 
 * given the possibility of false positives */
void shared_cache_sound_approx(Z3_context& ctx, all_access_t& all_access, ast_list_t& order_var_list, cache& shared_cache)
{
	int nsets = shared_cache.get_nsets();

	/* generate program order constraints sliced out per cache set */
	generate_program_order_constraints_per_set(ctx, all_access, order_var_list, shared_cache);

	/* generate performance bug objective (sliced out per cache set and provided as an 
	 * objective function to be used by UFO/SYMBA) */
	generate_performance_bug_objective(ctx, shared_cache.get_nsets());

	/* generate an SMT2 format file for each cache set to be solved independently */
	print_constraint_file_list_for_sound_approx(ctx, nsets);

}

/* generate program order constraints per set (required only for the approximate solution) */
static void generate_program_order_constraints_per_set(Z3_context& ctx, all_access_t&  all_access, 
	ast_list_t& order_var_list, cache& shared_cache)
{
	for (unsigned tid = 0; tid < NUM_OF_THREADS; tid++) {
		std::map<int, Z3_ast> prev_access;
		for (unsigned seq = 0; seq < all_access[tid].size(); seq++) {
			access_t acc = all_access[tid][seq];
			long seqI = acc.first;
			long memI = acc.second; 
			/* get cache set */
			int nset = shared_cache.get_mapped_set(memI);
			Z3_ast prog_order;
			/* detected previous access to this cache set */
			if(prev_access.count(nset)) {
				prog_order = Z3_mk_lt(ctx, prev_access[nset], order_var_list[tid][seq]);
				/* record set wise cache constraints */
				log_set_wise_cache_constraints(nset, prog_order);
			}
			prev_access[nset] = order_var_list[tid][seq];
		}	
	}
}

/* generate performance bug objective --- sliced out per cache set */
static void generate_performance_bug_objective(Z3_context& ctx, int nsets)
{
	for (int SI = 0; SI < nsets; SI++) {
		/* prepare objective variable */
		std::string wc_delay("wct_k1");
		wc_delay += "_";
		i2string(wc_delay, SI);
		Z3_ast wc_delay_z3 = mk_int_var(ctx, wc_delay.c_str());
		
		if (!symbolic_delay_per_set_log.count(SI)) {
			std::cerr << "\n\n.....cache set " << SI << " does not required to be modeled.....ignoring.....\n\n";
			continue;
		}
#ifdef _DEBUG
		std::cerr << "Number of symbolic delay variables = " << symbolic_delay_per_set_log[SI].size() << "\n";
#endif
		std::vector<Z3_ast>::iterator II = symbolic_delay_per_set_log[SI].begin();
		/* handle special case: when there is only one delay (symba crashes with marshalling error, otherwise) */
		if (symbolic_delay_per_set_log[SI].size() == 1) {
			/* prepare set-wise onbjective function */
			performance_obj[SI] = Z3_mk_lt(ctx, *II, wc_delay_z3);
		} else { /* multiple delay variables */
			Z3_ast* args = new Z3_ast[symbolic_delay_per_set_log[SI].size()];
			unsigned ti = 0;
			for (; II != symbolic_delay_per_set_log[SI].end(); II++) {
				args[ti] = *II;
				ti++;
			}
			Z3_ast all_delay_per_set = Z3_mk_add(ctx, symbolic_delay_per_set_log[SI].size(), args);
			delete args;	
			/* prepare set-wise onbjective function */
			performance_obj[SI] = Z3_mk_lt(ctx, all_delay_per_set, wc_delay_z3);
			/* end */
		}
	}
}

/* print one constraint file for each cache set. Each such file are independent and can be solved in parallel */
static void print_constraint_file_list_for_sound_approx(Z3_context& ctx, int nsets)
{
	int max_set_constraints = 0;

	for (int SI = 0; SI < nsets; SI++) {
		/* no constraints for this cache set...ignoring */
		if (!cache_set_log.count(SI)) continue;
		/* no need to model delay for this cache set, as all accesses 
		 * might be cache misses even in the absence of inter-core 
		 * cache conflicts (they might not get filtered in the previous 
		 * step due to the presence of program order constraints) */
		if (!performance_obj.count(SI)) continue;

		std::cerr << "\n\n*****Number of constraints in set " << SI << " = " << cache_set_log[SI].size() << "\n\n";
		/* log maximum number of constraints in set log for reporting */
		max_set_constraints = (cache_set_log[SI].size() > max_set_constraints) ? cache_set_log[SI].size() : max_set_constraints;
		Z3_ast* args = new Z3_ast[cache_set_log[SI].size()];
		for (unsigned sizeI = 0; sizeI < cache_set_log[SI].size(); sizeI++)
			args[sizeI] = cache_set_log[SI][sizeI];
		Z3_ast all_si_constraints = Z3_mk_and(ctx, cache_set_log[SI].size(), args);
		delete args;
		/* now prepare UFO/SYMBA compliant performance objective function together with constraints */
		/* antecedent = all_si_constraints && consequent = performance_obj[SI] */
		Z3_ast wc_delay_equivalence = Z3_mk_implies(ctx, all_si_constraints, performance_obj[SI]);
    	Z3_set_ast_print_mode(ctx, Z3_PRINT_SMTLIB2_COMPLIANT);
		/* prepare constraint name */
		std::string cnstr_name("cache_constraints");
		cnstr_name += "__";
		i2string(cnstr_name, SI);
		/* generate SMT2 compliant string */
    	char const* smt_constraints = Z3_benchmark_to_smtlib_string(ctx, cnstr_name.c_str(), 0, 0, 
			0, 0, 0, wc_delay_equivalence);
    	std::ofstream smtfile;
		/* generate the name of the file in format <prefix>.thread.smt2.<cache_set> */
    	std::string filename(local_analysis_file);
    	filename += ".smt2";
		filename += ".";
		i2string(filename, SI);
		/* end generating file name */
    	smtfile.open(filename.c_str(), std::ofstream::out);
    	//std::cerr << smt_constraints << "\n";
    	smtfile << smt_constraints << "\n";
    	smtfile.close();
    	/* we are done printing and logging for cache set SI */
	}	
	std::cerr << "\n\nMaximum number of set-level constraints = " << max_set_constraints << "\n\n";
	/* We are done printing all files for all cache sets */
}


