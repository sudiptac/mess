#ifndef __CONSTRAINTS_ALL_H__
#define __CONSTRAINTS_ALL_H__
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <tuple>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <z3.h>
#define NUM_OF_THREADS 2 /* Maximum number of threads running in parallel */
#define HIT_LATENCY 1 /* cache hit latency */
#define MISS_LATENCY 100 /* cache miss latency */
#define THRESHOLD 1200 /* cache miss latency */
#define CONFLICT_CONSTRAINTS_THRESHOLD 50000 /* threshold on the number of conflict constraints */

/* sudiptac: type definition */
typedef std::pair<int,long> loc_t; /* <thread id, program location> pair */
typedef std::pair<long,long> access_t; /* <sequence,memory block> pair */
typedef std::vector<access_t> access_list_t; /* for one thread */
typedef std::map<int, access_list_t> all_access_t; /* for all threads */
typedef std::map<int, std::vector<Z3_ast> > ast_list_t;
typedef std::vector<Z3_ast> log_t; /* for logging all constraints */
typedef std::map<int, log_t> set_log_t; /* for logging set-wise constraints */
typedef std::tuple<int, long, long> ctriple_t;
/* sudiptac: utility functions */
void i2string(std::string& str, int id);
void print_constraint_log(Z3_context& ctx, log_t& logv);
void print_constraint_log_for_optimization(Z3_context& ctx, std::vector<Z3_ast>& all_delay_vars, log_t& logv);

/* sudiptac: APIs meant for performance debugging */
void running_example();
void example_cache_conflict_thread(Z3_ast& order_t1_cx, Z3_ast& order_t2_cy, Z3_ast& psi_t1cx_t2cy);
void example_inequality_constraints(Z3_ast& order_t1_c2, Z3_ast& order_t1_c4, Z3_ast& order_t2_c3, Z3_ast& order_t2_c4);
void example_program_order_constraints(Z3_ast& order_t1_c2, Z3_ast& order_t1_c4, Z3_ast& order_t2_c3, Z3_ast& order_t2_c4);
void example_memory_latency_constraints(Z3_ast& psi_t1cx_t2cy, Z3_ast& psi_t1cx_t2cz, Z3_ast& delay);
void example_encode_performance_bug_predicate(Z3_ast& delay1, Z3_ast& delay2, Z3_ast& delay3, Z3_ast& delay4);
void print_example_constraint_log();
void log_set_wise_cache_constraints(int nset, Z3_ast& cnstr);
void log_set_wise_symbolic_delay(int nset, Z3_ast& s_delay);

void global_constraint_system(char* policy, char* filename, char* threshold, char* cache);
void constraint_system_shared_cache_lru();
void constraint_system_shared_arbitration_priority();
void constraint_system_shared_cache_fifo();
void constraint_system_shared_arbitration_fifo();
void fetch_all_accesses(unsigned tid, access_list_t& access_list, std::vector<int>& res_age_list);
void generate_performance_bug_predicate(Z3_context& ctx, std::vector<Z3_ast>& all_delay_vars, log_t& logv);
void log_set_wise_cache_constraints(int nset, Z3_ast& cnstr);
void log_set_wise_cache_constraints(int nset, Z3_ast& cnstr);

/* constraint generation common to all instantiations */
void generate_program_order_constraints(Z3_context& ctx, ast_list_t& var_list, log_t& logv);
void generate_order_inequality_constraints(Z3_context& ctx, ast_list_t& var_list, log_t& logv);

/* standard APIs */
void exitf(const char* message);
void unreachable();
void error_handler(Z3_context c, Z3_error_code e);
void throw_z3_error(Z3_context c, Z3_error_code e);
Z3_context mk_context_custom(Z3_config cfg, Z3_error_handler err);
Z3_context mk_context();
Z3_context mk_proof_context();
Z3_ast mk_var(Z3_context ctx, const char * name, Z3_sort ty);
Z3_ast mk_bool_var(Z3_context ctx, const char * name);
Z3_ast mk_int_var(Z3_context ctx, const char * name);
Z3_ast mk_int(Z3_context ctx, int v);
Z3_ast mk_real_var(Z3_context ctx, const char * name);
Z3_ast mk_unary_app(Z3_context ctx, Z3_func_decl f, Z3_ast x);
Z3_ast mk_binary_app(Z3_context ctx, Z3_func_decl f, Z3_ast x, Z3_ast y);
void check(Z3_context ctx, Z3_lbool expected_result);
void prove(Z3_context ctx, Z3_ast f, Z3_bool is_valid);
void assert_inj_axiom(Z3_context ctx, Z3_func_decl f, unsigned i);
void assert_comm_axiom(Z3_context ctx, Z3_func_decl f);
Z3_ast mk_tuple_update(Z3_context c, Z3_ast t, unsigned i, Z3_ast new_val);
void display_symbol(Z3_context c, FILE * out, Z3_symbol s);
void display_sort(Z3_context c, FILE * out, Z3_sort ty);
void display_ast(Z3_context c, FILE * out, Z3_ast v);
void display_function_interpretations(Z3_context c, FILE * out, Z3_model m);
void display_model(Z3_context c, FILE * out, Z3_model m);
void check2(Z3_context ctx, Z3_lbool expected_result);
void display_version();
void demorgan();
#endif
