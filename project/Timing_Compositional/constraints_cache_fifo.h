#ifndef __CONSTRAINTS_CACHE_FIFO__
#define __CONSTRAINTS_CACHE_FIFO__

#include "constraints_all.h"
#include "cache.h"

static void generate_symbolic_cache_conflict_variables(Z3_context& ctx, ast_list_t& order_var_list, ast_list_t& delay_var_list, cache& fifo_cache);
static void generate_fifo_cache_conflict_constraints(Z3_context& ctx, ast_list_t& order_var_list, ast_list_t& delay_var_list, cache& fifo_cache, log_t& logv);
static void fifo_cache_conflict_constraints_per_block(Z3_context& ctx, Z3_ast& order_entry, loc_t& tloc, long& mem, long& cmem, cache& fifo_cache, log_t& logv);
static void generate_memory_latency_constraints(Z3_context& ctx, ast_list_t& delay_var_list, cache& fifo_cache, log_t& logv);

#endif
