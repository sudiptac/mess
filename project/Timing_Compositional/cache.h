#ifndef __CACHE_H__
#define __CACHE_H__
	
typedef enum {LRU_POLICY, FIFO_POLICY} repl_t;

class cache {
	private:
	int nsets; /* number of sets */
	int nassoc; /* associativity */
	int nwords; /* line size (in bytes) */
	repl_t repl; /* replacement policy */

	public:
	cache(repl_t policy) { /* default setting is 4-way associative, 1KB cache */
		nsets = 16;
		nassoc = 4;
		nwords = 32;
		repl = policy;
	}
	cache(int sets, int assoc, int word, repl_t policy) {
		nsets = sets;
		nassoc = assoc;
		nwords = word;
		repl = policy;
	}
	int get_nsets() {
		return nsets;
	}
	int get_nassoc() {
		return nassoc;
	}
	int get_nwords() {
		return nwords;
	}
	repl_t get_repl() {
		return repl;
	}
	bool is_conflicting(long memI, long memJ) {
		
		return ((get_mapped_set(memI) == get_mapped_set(memJ)) && (memI != memJ));	
	}
	int get_mapped_set(long mem) {
		long block = mem/nwords;
		int set = block % nsets;		
		
		return set;
	}
};

#endif
