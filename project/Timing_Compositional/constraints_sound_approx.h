#ifndef __CONSTRAINTS__SOUND__APPROX__H__
#define __CONSTRAINTS__SOUND__APPROX__H__

#include "constraints_all.h"
#include "cache.h"

void shared_cache_sound_approx(Z3_context& ctx, all_access_t& all_access, ast_list_t& order_var_list, cache& shared_cache);
static void generate_program_order_constraints_per_set(Z3_context& ctx, all_access_t&  all_access, ast_list_t& order_var_list, cache& shared_cache);
static void generate_performance_bug_objective(Z3_context& ctx, int nsets);
static void print_constraint_file_list_for_sound_approx(Z3_context& ctx, int nsets);


#endif
