; cache_constraints
(set-info :status unknown)
(declare-fun delay_1_4 () Int)
(declare-fun delay_1_3 () Int)
(declare-fun delay_0_4 () Int)
(declare-fun delay_0_2 () Int)
(declare-fun psi_1_4_4096 () Int)
(declare-fun psi_1_3_768 () Int)
(declare-fun psi_0_4_2304 () Int)
(declare-fun psi_0_2_1536 () Int)
(declare-fun order_1_4 () Int)
(declare-fun order_0_2 () Int)
(declare-fun order_1_3 () Int)
(declare-fun order_0_4 () Int)
(assert
(let ((?x173 (+ delay_0_2 delay_0_4 delay_1_3 delay_1_4)))
(let (($x178 (>= ?x173 104)))
(let (($x165 (= delay_1_4 1)))
(let (($x162 (> psi_1_4_4096 1)))
(let (($x167 (or $x162 $x165)))
(let (($x164 (= delay_1_4 100)))
(let (($x163 (not $x162)))
(let (($x166 (or $x163 $x164)))
(let (($x154 (= delay_1_3 1)))
(let (($x151 (> psi_1_3_768 1)))
(let (($x156 (or $x151 $x154)))
(let (($x153 (= delay_1_3 100)))
(let (($x152 (not $x151)))
(let (($x155 (or $x152 $x153)))
(let (($x143 (= delay_0_4 1)))
(let (($x140 (> psi_0_4_2304 1)))
(let (($x145 (or $x140 $x143)))
(let (($x142 (= delay_0_4 100)))
(let (($x141 (not $x140)))
(let (($x144 (or $x141 $x142)))
(let (($x132 (= delay_0_2 1)))
(let (($x129 (> psi_0_2_1536 0)))
(let (($x134 (or $x129 $x132)))
(let (($x131 (= delay_0_2 100)))
(let (($x130 (not $x129)))
(let (($x133 (or $x130 $x131)))
(let (($x123 (= psi_1_4_4096 0)))
(let (($x107 (< order_0_2 order_1_4)))
(let (($x108 (< order_1_4 order_1_4)))
(let (($x109 (and $x108 $x107)))
(let (($x110 (not $x109)))
(let (($x111 (and $x110)))
(let (($x112 (and $x107 $x111)))
(let (($x122 (or $x112)))
(let (($x124 (or $x122 $x123)))
(let (($x114 (= psi_1_4_4096 1)))
(let (($x113 (not $x112)))
(let (($x115 (or $x113 $x114)))
(let (($x103 (= psi_1_3_768 0)))
(let (($x87 (< order_0_4 order_1_3)))
(let (($x88 (< order_1_3 order_1_3)))
(let (($x89 (and $x88 $x87)))
(let (($x90 (not $x89)))
(let (($x91 (and $x90)))
(let (($x92 (and $x87 $x91)))
(let (($x102 (or $x92)))
(let (($x104 (or $x102 $x103)))
(let (($x94 (= psi_1_3_768 1)))
(let (($x93 (not $x92)))
(let (($x95 (or $x93 $x94)))
(let (($x83 (= psi_0_4_2304 0)))
(let (($x65 (< order_1_3 order_0_4)))
(let (($x66 (< order_0_4 order_0_4)))
(let (($x67 (and $x66 $x65)))
(let (($x68 (not $x67)))
(let (($x69 (and $x68)))
(let (($x70 (and $x65 $x69)))
(let (($x82 (or $x70)))
(let (($x84 (or $x82 $x83)))
(let (($x72 (= psi_0_4_2304 1)))
(let (($x71 (not $x70)))
(let (($x73 (or $x71 $x72)))
(let (($x61 (= psi_0_2_1536 0)))
(let (($x43 (< order_1_4 order_0_2)))
(let (($x44 (< order_0_2 order_0_2)))
(let (($x45 (and $x44 $x43)))
(let (($x46 (not $x45)))
(let (($x47 (and $x46)))
(let (($x48 (and $x43 $x47)))
(let (($x60 (or $x48)))
(let (($x62 (or $x60 $x61)))
(let (($x51 (= psi_0_2_1536 1)))
(let (($x49 (not $x48)))
(let (($x52 (or $x49 $x51)))
(let (($x34 (< order_1_3 order_1_4)))
(let (($x9 (< order_0_2 order_0_4)))
(and $x9 $x34 $x52 $x62 $x73 $x84 $x95 $x104 $x115 $x124 $x133 $x134 $x144 $x145 $x155 $x156 $x166 $x167 $x178))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
(check-sat)

